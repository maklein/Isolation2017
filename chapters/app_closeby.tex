\section{Additional information concerning the close-by correction}

\label{app_closeby}

\subsection{Models in case no topocluster information is available}

\textbf{The \emph{basic} model.} This model (\texttt{topoetconeModel == 0}) calculates a 
basic correction and is applied by default if the topocluster information is not 
available and if no models were specified. The core energy 
$E_{\text{T}}^{\text{core}}\left(\lambda\right)$ of the close-by objects is added to the 
correction if ${\Delta{}R}_{\lambda, \lambda^{\prime}}\leq{\Delta{}R}_{\text{calo}XX}$. 
Using this model would result in an over-estimation of the correction to apply.

\textbf{The \emph{minimal} model.} This model (\texttt{topoetconeModel == 1}) calculates 
a minimal correction. The core energy of the close-by objects is 
added to the correction if 
${\Delta{}R}_{\text{core}}\leq{\Delta{}R}_{\lambda, \lambda^{\prime}}
\leq{\Delta{}R}_{\text{calo}XX} - {\Delta{}R}_{\text{core}}$. 
The lower limit ensures no topoclusters used to calculate 
$E_{\text{T}}^{\text{core}}\left(\lambda\right)$ and 
$E_{\text{T}}^{\text{core}}\left(\lambda^{\prime}\right)$ are shared, which would lead 
to double-counting. The upper limit ensures that no topoclusters which are outside the 
${\Delta{}R}_{\text{calo}XX}$ cone with respect to $\lambda$ (but close enough to 
$\lambda^{\prime}$) are counted. Using this model would result in minimising the 
correction to be applied. However, double-counting could still happen if topoclusters are 
shared between close-by objects $\lambda^{\prime}$, whose ${\Delta{}R}\leq{\Delta{}R}_{\lambda, 
\lambda^{\prime}}$ are within the limits, in the calculation of their core energies. 

\textbf{The \emph{complex} model.} This model (\texttt{topoetconeModel == 2}) calculates 
a correction which should be the closest to the reality. In this model, a fraction $f$ 
of the core energy of the close-by objects $\lambda^{\prime}$ is added to the correction 
depending on ${\Delta{}R}_{\lambda, \lambda^{\prime}}$. This model is not as stringent as 
the \emph{minimal} model, but suffers the same limits: there may be over-counting in 
case some topoclusters are shared among close-by objects. Noting 
${\Delta{}R}_{1} = \text{min}\left(2{\Delta{}R}_{\text{core}}, 
{\Delta{}R}_{\text{iso}} - {\Delta{}R}_{\text{core}}\right)$, 
${\Delta{}R}_{2} = \text{max}\left(2{\Delta{}R}_{\text{core}}, 
{\Delta{}R}_{\text{iso}} - {\Delta{}R}_{\text{core}}\right)$ 
and ${\Delta{}R}_{3} = {\Delta{}R}_{\text{iso}} + {\Delta{}R}_{\text{core}}$, the 
fraction $f$ of $E_{\text{T}}^{\text{core}}\left(\lambda^{\prime}\right)$ to be removed 
is given by:
		
\begin{itemize}
		
	\item ${\Delta{}R}_{\text{core}}\geq{\Delta{}R}_{\text{iso}} \Rightarrow 
	\forall{}\Delta{}R, f = 0$.
			 
	\item ${\Delta{}R}_{\text{core}}<{\Delta{}R}_{\text{iso}} \Rightarrow$
	\begin{displaymath}
		\left\{ \begin{array}{l}
		0 \leq \Delta{}R \leq {\Delta{}R}_{1} \Rightarrow f = 
		\text{min}\left(\frac{\Delta{}R}{{\Delta{}R}_{\text{core}}}, 1\right). \\
		{\Delta{}R}_{1} \leq \Delta{}R \leq {\Delta{}R}_{2} \Rightarrow f = 
		\text{min}\left(\frac{{\Delta{}R}_{1}}{{\Delta{}R}_{\text{core}}}, 1\right). \\
		{\Delta{}R}_{2} \leq \Delta{}R \leq {\Delta{}R}_{3} \Rightarrow f = 
		\text{min}\left(\frac{{\Delta{}R}_{1}}{{\Delta{}R}_{\text{core}}}, 
		1\right)\times\frac{{\Delta{}R}_{3} - \Delta{}R}{{\Delta{}R}_{3} - 
		{\Delta{}R}_{1}}. \\
		\Delta{}R \geq {\Delta{}R}_{3} \Rightarrow f = 0.
		\end{array} \right.
	\end{displaymath}
	
\end{itemize}

Note that the first item applies to all the models.

\subsection{Software implementation}

Having introduced the problem and the way to handle it properly, this subsection focuses 
on the software implementation of the correction for close-by objects. The algorithm 
performing the correction was developed as part of the \texttt{IsolationTool} package (\texttt{IsolationCloseByCorrectionTol})
 and allows the user to calculate the correction 
to apply to the isolation variables, given a certain set of close-by objects. It is 
important to note that the user should be responsible for choosing which objects should 
have their contributions removed, as this is very analysis-dependent. The tool will then 
take care of checking whether the selected objects pass the extra criteria (which are 
part of the definition of the isolation variables only and are therefore 
analysis-independent). 

There are two functionalities available in both Athena and Rootcore analysis releases. 
The first one takes all the parameters needed and returns the value of the correction to 
be applied to the isolation variables. This method is for experts only and will not be further described here. 
The second method uses the \texttt{IsolationSelectionTool} to check whether an 
object passes or fails the isolation working points after applying the correction for 
close-by objects.

More information can be found at \href{https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/IsolationOverlapRemoval}{https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/IsolationOverlapRemoval}.

The tool operates on objects which are particles (\texttt{const xAOD::IParticle}): muons, 
electrons and photons.

%\subsubsection{Calculating the correction}
%
%In order to calculate the correction to be removed from the isolation variables, a 
%function can be called by the tool:
%
%\begin{lstlisting}
%public:
%bool removeOverlap(std::vector<float>& overlapCorrections, const xAOD::IParticle& par, const std::vector<xAOD::Iso::IsolationType>& types, const std::vector<const xAOD::IParticle*>& closePar, float trackRadiusMin = 0.0, float caloRadiusMin = 0.0, int topoetconeModel = -1, const xAOD::Vertex* vertex = 0, const xAOD::CaloClusterContainer* topoClusters = 0) const;
%private:
%bool calculateRemovalTrackIso(float& removal, const xAOD::IParticle& par, xAOD::Iso::IsolationType type, const std::vector<const xAOD::IParticle*>& closePar, const xAOD::Vertex* vertex, float coneSize, float trackRadiusMin = 0.0) const;
%bool calculateRemovalTopoetcone(float& removal, const xAOD::IParticle& par, xAOD::Iso::IsolationType type, const std::vector<const xAOD::IParticle*>& closePar, float coneSize, float caloRadiusMin = 0.0, int modelOption = -1, const xAOD::CaloClusterContainer* topoClusters = 0) const;
%\end{lstlisting}
%
%The user has to provide the following inputs:
%
%\begin{itemize}
%	\item the particle \texttt{par} which isolation needs to be corrected ($\lambda$ in 
%	the previous formulae), 
%	\item a vector \texttt{closePar} containing the particles to be considered for 
%	removal (set of $\lambda^{\prime}$ in the previous formulae), 
%	\item a vector \texttt{types} containing the isolation types to be considered,
%	\item an empty vector \texttt{overlapCorrections} in which the corrections 
%	corresponding to each type of isolation of the vector \texttt{types} will be stored.
%\end{itemize}
%
%Several models can be used to approximate the topocore energy in case the topoclusters 
%are not available. The different models supported by the tool are described next. In case 
%the information about the topoclusters is missing and no models are specified, the tool 
%performs a \emph{basic} model.
%
%A minimal radius for track- and calorimeter-based isolation variables can be 
%optionally set: objects $\lambda^{\prime}$ too close to the particle $\lambda$ are not 
%considered for correction. This option can be useful for specific analyses.
%
%Note that the parameters \texttt{vertex} and \texttt{topoclusters} are optional. They 
%should be used if the user wants to impose the primary vertex to use for the track 
%selection and the topocluster container for the calorimeter isolation correction.
%
%The function \texttt{removeOverlap} calls the two private functions 
%\texttt{calculateRemovalTrackIso} and \texttt{calcu- lateRemovalTopoetcone} depending on 
%the type of the isolation, which correction should be calculated.
%
%This short example illustrates how to use this particular function:
%
%\begin{lstlisting}
%// Stores the muons in a vector.
%const xAOD::MuonContainer* muons = 0;
%CHECK(event.retrieve(muons, "Muons"));  
%vector<const xAOD::IParticle*> muonsVec; 
%for(auto muon: *muons) { 
%    muonsVec.push_back((const xAOD::IParticle*) muon);
%}
%
%// Stores the isolation types and their names in a vector.
%vector<xAOD::Iso::IsolationType> types; 
%types.push_back(xAOD::Iso::IsolationType::ptvarcone30); 
%types.push_back(xAOD::Iso::IsolationType::topoetcone20);
%vector<const char*> names; 
%names.push_back("ptvarcone30"); 
%names.push_back("topoetcone20");
%
%// Loops over muons.
%for(auto muon: *muons){
%    // Performs overlap removal. Removals is the vector which will contain the corrections.
%    vector<Float_t> removals; 
%    m_isoHelper->removeOverlap(removals, *muon, types, muonsVec); 
%    for (unsigned int j = 0; j < types.size(); j++) {
%    	if(j < removals.size()) {
%     		float value; muon->isolation(value, types.at(j));
%     		Info(APP_NAME, "Muon Iso, value, correction: %s, %f, %f", names.at(j), value, removals.at(j));
% 		}
%	}
%}
%\end{lstlisting}
%
%\subsubsection{Applying the correction to the isolation selection}

An instance of the \texttt{IsolationSelectionTool} is associated to the 
\texttt{IsolationHelper}. The properties of this instance have to be set by the user before the 
initialisation of the \texttt{IsolationCloseByCorrectionTool}.

Having initialised the tool, the following function can be used to check the isolation working 
points after applying the corrections to the objects:

\begin{lstlisting}
public :
virtual Root::TAccept& acceptCorrected(const xAOD::IParticle& x, const std::vector<const xAOD::IParticle*>& closePar, int topoetconeModel = -1) = 0;
\end{lstlisting}

\begin{itemize}
	\item \texttt{x} is the particle ($\lambda$) which isolation is corrected. This is 
	the object on which the isolation working points are tested.
	\item \texttt{closePar} are the close-by objects to be subtracted (set of $\lambda^{\prime}$).
	\item The optional parameters have the same use.
\end{itemize}

For examples, please refer to the Twiki page.

\subsubsection{Adding the necessary variables to the DxAOD}

In order for the tool to properly calculate and apply the correction, the following 
information are needed in the n-tuples:
\begin{itemize}
	\item The uncalibrated energy $E_{\text{T}}^{c}$ of the topoclusters, as well 
	as their coordinates $\eta^{c}$ and $\phi^{c}$.
	\item The extrapolated variables $\eta$ and $\phi$ of the muons (these information 
	are contained in the \emph{cluster} of the muon, which gathers the information on 
	the hits of the muon in each layer of the calorimeters).
	\item The core energy for the track-based isolation $p_{\text{T}}^{\text{core}}$.
\end{itemize}

To add these variables to the derivation files, please have a look at the example on the Twiki page.
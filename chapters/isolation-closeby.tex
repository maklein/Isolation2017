% This is the part about the correction for close-by objects.
% Responsible: Arthur Lesage (arthur.lesage@cern.ch)

The isolation variables are calculated to distinguish signal objects (which are 
well-isolated) from background objects (not isolated). As described in the previous 
sections (Sec.~\ref{sec:muon_iso} and~\ref{sec:ele_iso}), isolation sums up the 
contribution of the objects (leptons or jets), and their energies, which are close to the 
particle studied.

However, certain specific physics processes may require to select objects which are close.
An example is the $Z\rightarrow{}\mu\mu$ process. At high-$\pt$, the muons tend to be 
closer, but should still be selected for the analysis. In these situations, a signal 
object may be marked as non-isolated because of the contribution of another signal object 
close-by.

Therefore, a tool was developed to correct for these extra contributions, improving the 
isolation of the signal objects and increasing the signal significance (background 
objects are corrected at a lesser extent and should not pass the isolation criteria after 
correction).

This section describes how the correction is technically performed by the tool with a 
brief presentation of the performance results.

\subsection{Correcting for the contributions of close-by objects}

The contribution of the close-by objects is removed from the isolation variables. 
Therefore, having preselected which objects should be considered for the correction, the 
subtraction of the contribution has to be done in the opposite way as compared to how the 
isolation variable was first calculated. The contribution of the close-by objects is 
equal to the core energies of these objects, as defined in the previous 
sections~\ref{sec:muon_iso} and~\ref{sec:ele_iso}.

Three isolation variables are supported: the topoetcone$XX$, ptcone$XX$ and ptvarcone$XX$ 
variables, where $XX\in\left\{20, 30, 40\right\}$. The treatment of these variables is 
separated into two categories linked to the nature of the isolation. These two categories 
are detailed in the next subsections.

\subsubsection{The calorimetric isolation}

The topoetcone$XX$ variables are calculated as follows:

\begin{equation}
	E_{\text{T}}^{\text{isol}} = E_{\text{T, raw}}^{\text{isol}} - E_{\text{T, core}} - 
	E_{\text{T, leakage}}\left(\pt\right) - E_{\text{T, pile-up}}\left(\eta\right).
\end{equation}

This equation can be reduced to:

\begin{equation}
	E_{\text{T}}^{\text{isol}} = E_{\text{T, raw}}^{\text{isol}} - E_{\text{T, core}} - 
	E_{\text{T}}^{\text{corr}}\left(\pt, \eta\right).
\end{equation}

The two first components in the equation are calculated by summing up over the 
topoclusters which are close enough to the object $\lambda$. Since these two components 
are the dominating contributions to the isolation, they determine the correction to be 
removed for close-by objects $\lambda^{\prime}$. 

The core energy accounts for the energy deposited by the object itself. It corresponds to 
the sum of the transverse energy $E_{\text{T}}^{c}$ of the topoclusters $c$ which are 
close to the object $\lambda$ in $\eta$ and $\phi$ (${\Delta{}R}_{\lambda,c} 
\leq {\Delta{}R}_{\text{core}} = 0.1$ or $0.05$ for the variables after Moriond 2017). 
Note that the extrapolated variables are used in this case.

By analogy, the contribution of the close-by objects $\lambda^{\prime}$ to be removed is 
set to the sum of $E_{\text{T}}^{c}$ of the topoclusters which are used for the 
calculation of both $E_{\text{T, raw}}^{\text{isol}}\left(\lambda\right)$ and 
$E_{\text{T, core}}\left(\lambda^{\prime}\right)$. The $E_{\text{T}}^{c}$ of such 
topoclusters should be removed only once even if the topoclusters contribute to the core 
energy of several close-by objects. The topolcusters have to be selected the same way as 
for the calculation of $E_{\text{T}}^{\text{isol}}$.

The correction can be summarised through this equation:

\begin{equation}
	E_{\text{T}}^{\text{close-by corr}} = \sum_{\substack{c, \\ 
	{\Delta{}R}_{c, \lambda^{\prime}}\leq{\Delta{}R}_{\text{core}}, \\
	{\Delta{}R}_{\text{core}}\leq{\Delta{}R}_{c, \lambda}\leq{\Delta{}R}_{\text{calo}XX}}}
	{E_{\text{T}}^{c}},
\end{equation}
where ${\Delta{}R}_{\text{calo}XX} = XX/100$ is the isolation cone for the variable 
topoetcone$XX$, with $XX\in\left\{20, 30, 40\right\}$.

In order to properly perform the correction, the information about the topoclusters must 
be available. The selection of the topoclusters is done the same way as for the 
calculation of the topoetcone$XX$ variables. Note that it is assumed that the pile-up and 
leakage corrections have only a very minor effect on the contribution of the close-by 
objects.

\subsubsection{The track-based isolation} 

The procedure is similar to that of the calorimetric isolation, but this time, the core 
energy cannot be split into clusters. Once again, the core energy 
$p_{\text{T}}^{\text{core}}$, which is subtracted in the calculation of the variables 
ptcone$XX$ and ptvarcone$XX$, is taken for the contribution of the close-by objects. The 
track-based isolation of an object $\lambda$ is calculated using

\begin{equation}
	p_{\text{T}}^{\text{isol}} = \sum_{\substack{t,\,t\neq\lambda\\ {\Delta{}R}_{t,
	\lambda}\leq{\Delta{}R}_{\text{track}XX}}}{p_{\text{T}}\left(t\right)} - p_{\text{T}}^{
	text{core}}\left(\lambda\right), 
	\label{eq:track_def}
\end{equation}
where $t$ is the set of tracks which pass the selection requirements (as described in 
Sec.~\ref{subsec:muon_trackiso}) and ${\Delta{}R}_{\text{track}XX}$ is the size of the isolation 
cone ($XX/100$ for the ptcone$XX$ variables, and 
$\text{min}\left(\frac{k_{T}}{p_{\text{T}}^{\lambda}}, \frac{XX}{100}\right)$ for the 
ptvarcone$XX$ variables).

Using Eq.~\ref{eq:track_def}, the correction to be applied can easily be inferred:

\begin{equation}
	p_{\text{T}}^{\text{close-by corr}} = \sum_{\substack{\lambda^{\prime}, \\ 
	{\Delta{}R}_{\lambda,
	t_{\lambda^{\prime}}}\leq{\Delta{}R}_{\text{track}XX}}}{p_{\text{T}}^{\text{core}}
	\left(t_{\lambda^{\prime}}\right)},
\end{equation}
where $\lambda^{\prime}$ are the selected objects, which contributions have to be 
removed. Note that the track associated to these objects should also pass the 
requirements as described in Sec.~\ref{subsec:muon_trackiso}.

\subsubsection{The different models}

For the calorimeter-based isolation, the information of the topoclusters are needed. 
If this is missing, models assuming a uniform density of the transverse energy of the 
topoclusters can be used, which do not require any additional information. Calculating 
the corrections using the topocluster information is precise, whereas the models 
calculate estimates of the correction to apply. They are briefly described in 
App.~\ref{app_closeby}.

\subsection{Performance}

The validation was performed for muons and electrons using $WH$ samples:

\texttt{mc15\_{}13TeV.341964.Pythia8EvtGen\_{}A14NNPDF23LO\_{}WH125\_{}ZZ4l}. 
$80000$ events were processed. The choice of this set of samples was motivated by the 
abundance of muons and electrons close to each other.

Muons (electrons) having a truthType of $6$ ($2$), corresponding to well-isolated objects, are selected. 
These objects both serve for the studied particles, as well as for the particles which contributions are subtracted.

The following figures validate the functionalities of the tool performing the correction 
for close-by objects. For each particle (muon and electron), the efficiency of the 
\emph{Loose} isolation working point (using cuts on both calorimeter- and track-based isolation) 
is presented before and after applying the correction. Then the distributions of the 
isolation variables before and after correction are compared (both in the case where the 
object has a non-zero correction and for all objects without any distinction). An 
additional plot presents the comparison of the uncorrected isolation for objects which 
should not require any correction (${\Delta{}R}$ with the closest object greater than the 
isolation cone or object alone in the event) with the corrected isolation for objects 
which should require correction. Ideally these two distributions should be quite close.

Finally this part finishes with a presentation of the number of objects shared among 
topoclusters. This last study aims at testing whether the information concerning 
topoclusters is really necessary or if one of the proposed models can actually be used 
as a good approximation.

\FloatBarrier

\subsubsection{The \emph{Loose} isolation working point}

To briefly validate the functionality of the tool which tells whether an object passes 
the cuts of an isolation working point after correction, the efficiencies of the \emph{Loose} 
isolation working point are calculated before and after correction for electrons and 
muons. The results are shown in Fig.~\ref{fig:looseEff_electron_muon}.

The corrected efficiency is higher, especially for muons. The correction affects 
more the muons than the electrons because they are intrinsically more isolated and the 
low efficiencies are due to the contribution of close-by objects. For electrons on the 
other hand the contribution of close-by objects does not play a major role in the 
activity around the electron.

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/electron_Loose_EffComp.pdf}
\includegraphics[scale = 0.4]{figures/close_by/muon_Loose_EffComp.pdf}
\caption{The efficiencies of the \emph{Loose} isolation working point for (left) electrons 
and (right) muons, before and after correction for close-by objects, as a function of 
the transverse momentum. As expected the efficiencies increase after correction, 
especially for muons where the contribution of close-by objects must be more 
important as compared to the total activity surrounding the muon.}
\label{fig:looseEff_electron_muon}
\end{figure}

\FloatBarrier

\subsubsection{The electron variables}

First, the validation is done on the electrons. The following variables are chosen for 
study, representative to the ones used for the isolation working points: 
$p_{\text{T}}^{\text{cone}20}$, $p_{\text{T}}^{\text{varcone}20}$ and 
$E_{\text{T}}^{\text{topoetcone}20}$.

Figures \ref{fig:electron_ptcone20_CompBeforeAfterCorr}, 
\ref{fig:electron_ptvarcone20_CompBeforeAfterCorr} and 
\ref{fig:electron_topoetcone20_CompBeforeAfterCorr} show the distribution before and 
after applying the correction, if the correction is not nil. A zoom is proposed close to 
$0$ to check the behaviour of the corrected variable.

Figures \ref{fig:electron_ptcone20_2DPlot_and_WithWithoutCorrection}, 
\ref{fig:electron_ptvarcone20_2DPlot_and_WithWithoutCorrection} and 
\ref{fig:electron_topoetcone20_2DPlot_and_WithWithoutCorrection} show the on the left 
the comparison of the variable after correction ($y$ axis) and before ($x$ axis). All 
objects are included in this plot. On the right, the Fig. shows the distribution of the 
corrected variable for objects which should require correction (${\Delta{}R}$ with 
close-by objects less than the isolation cone size) against the uncorrected variable 
for objects which should not require correction (no close-by object or ${\Delta{}R}$ 
with close-by objects greater than the isolation cone size). Ideally these two last 
distributions should be close to each other.

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/WH_electron_ptcone20_CompBeforeAfterCorr.pdf}
\includegraphics[scale = 0.4]{figures/close_by/WH_electron_ptcone20_CompBeforeAfterCorr_Zoomed.pdf}
\caption{The distributions of $p_{\text{T}}^{\text{cone}20}$ of the electrons before 
(blue) and after (red) correction for close-by objects. Only electrons for which the 
correction was not nil and passing the cuts are selected. The right plot shows a view 
close to $0$ of the distributions presented in the left plot. The corrected isolation 
has its distribution shifted to lower values and should not have negative values.}
\label{fig:electron_ptcone20_CompBeforeAfterCorr}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/WH_(electron_ptcone20)_Plot2D.pdf}
\includegraphics[scale = 0.4]{figures/close_by/WH_electron_ptcone20_WithWithoutCorr.pdf}
\caption{The left plot shows the repartition of the $p_{\text{T}}^{\text{cone}20}$ 
before and after correction for the electrons passing the cuts. The diagonal indicates 
the electrons for which the correction was negligible. The lower points indicate 
electrons which become fully isolated after correction. The right plot shows the 
distributions of the corrected $p_{\text{T}}^{\text{cone}20}$ of the electrons passing 
the cuts for which no correction are expected (blue) and a correction is expected 
(red). Ideally the two distributions should look similar.}
\label{fig:electron_ptcone20_2DPlot_and_WithWithoutCorrection}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/WH_electron_ptvarcone20_CompBeforeAfterCorr.pdf}
\includegraphics[scale = 0.4]{figures/close_by/WH_electron_ptvarcone20_CompBeforeAfterCorr_Zoomed.pdf}
\caption{The distributions of $p_{\text{T}}^{\text{varcone}20}$ of the electrons before 
(blue) and after (red) correction for close-by objects. Only electrons for which the 
correction was not nil and passing the cuts are selected. The right plot shows a view 
close to $0$ of the distributions presented in the left plot. The corrected isolation 
has its distribution shifted to lower values and should not have negative values.}
\label{fig:electron_ptvarcone20_CompBeforeAfterCorr}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/WH_(electron_ptvarcone20)_Plot2D.pdf}
\includegraphics[scale = 0.4]{figures/close_by/WH_electron_ptvarcone20_WithWithoutCorr.pdf}
\caption{The left plot shows the repartition of the $p_{\text{T}}^{\text{varcone}20}$ 
before and after correction for the electrons passing the cuts. The diagonal indicates 
the electrons for which the correction was negligible. The lower points indicate 
electrons which become fully isolated after correction. The right plot shows the 
distributions of the corrected $p_{\text{T}}^{\text{varcone}20}$ of the electrons passing 
the cuts for which no correction are expected (blue) and a correction is expected 
(red). Ideally the two distributions should look similar.}
\label{fig:electron_ptvarcone20_2DPlot_and_WithWithoutCorrection}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/WH_electron_topoetcone20_CompBeforeAfterCorr.pdf}
\caption{The distributions of $E_{\text{T}}^{\text{topoetcone}20}$ of the electrons 
before (blue) and after (red) correction for close-by objects. Only electrons for which 
the correction was not nil and passing the cuts are selected. The corrected isolation has 
its distribution shifted to lower values.}
\label{fig:electron_topoetcone20_CompBeforeAfterCorr}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/WH_(electron_topoetcone20)_Plot2D.pdf}
\includegraphics[scale = 0.4]{figures/close_by/WH_electron_topoetcone20_WithWithoutCorr.pdf}
\caption{The left plot shows the repartition of the $E_{\text{T}}^{\text{topoetcone}20}$ 
before and after correction for the electrons passing the cuts. The diagonal indicates 
the electrons for which the correction was negligible. The lower points indicate 
electrons which become fully isolated after correction. The right plot shows the 
distributions of the corrected $E_{\text{T}}^{\text{cone}20}$ of the electrons passing 
the cuts for which no correction are expected (blue) and a correction is expected 
(red). Ideally the two distributions should look similar.}
\label{fig:electron_topoetcone20_2DPlot_and_WithWithoutCorrection}
\end{figure}

\FloatBarrier

\subsubsection{The muon variables}

Then, the validation is done on the muons. The following variables are chosen for 
study, representative to the ones used for the isolation working points: 
$p_{\text{T}}^{\text{cone}30}$, $p_{\text{T}}^{\text{varcone}30}$ and 
$E_{\text{T}}^{\text{topoetcone}20}$.

Figures \ref{fig:muon_ptcone30_CompBeforeAfterCorr}, 
\ref{fig:muon_ptvarcone30_CompBeforeAfterCorr} and 
\ref{fig:muon_topoetcone20_CompBeforeAfterCorr} show the distribution before and 
after applying the correction, if the correction is not nil. A zoom is proposed close to 
$0$ to check the behaviour of the corrected variable.

Figures \ref{fig:muon_ptcone30_2DPlot_and_WithWithoutCorrection}, 
\ref{fig:muon_ptvarcone30_2DPlot_and_WithWithoutCorrection} and 
\ref{fig:muon_topoetcone20_2DPlot_and_WithWithoutCorrection} show the on the left 
the comparison of the variable after correction ($y$ axis) and before ($x$ axis). All 
objects are included in this plot. On the right, the Fig. shows the distribution of the 
corrected variable for objects which should require correction (${\Delta{}R}$ with 
close-by objects less than the isolation cone size) against the uncorrected variable 
for objects which should not require correction (no close-by object or ${\Delta{}R}$ 
with close-by objects greater than the isolation cone size). Ideally these two last 
distributions should be close to each other.

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/WH_muon_ptcone30_CompBeforeAfterCorr.pdf}
\includegraphics[scale = 0.4]{figures/close_by/WH_muon_ptcone30_CompBeforeAfterCorr_Zoomed.pdf}
\caption{The distributions of $p_{\text{T}}^{\text{cone}30}$ of the muons before 
(blue) and after (red) correction for close-by objects. Only muons for which the 
correction was not nil and passing the cuts are selected. The right plot shows a view 
close to $0$ of the distributions presented in the left plot. The corrected isolation 
has its distribution shifted to lower values and should not have negative values.}
\label{fig:muon_ptcone30_CompBeforeAfterCorr}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/WH_(muon_ptcone30)_Plot2D.pdf}
\includegraphics[scale = 0.4]{figures/close_by/WH_muon_ptcone30_WithWithoutCorr.pdf}
\caption{The left plot shows the repartition of the $p_{\text{T}}^{\text{cone}30}$ 
before and after correction for the muons passing the cuts. The diagonal indicates 
the muons for which the correction was negligible. The lower points indicate 
muons which become fully isolated after correction. The right plot shows the 
distributions of the corrected $p_{\text{T}}^{\text{cone}30}$ of the muons passing 
the cuts for which no correction are expected (blue) and a correction is expected 
(red). Ideally the two distributions should look similar.}
\label{fig:muon_ptcone30_2DPlot_and_WithWithoutCorrection}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/WH_muon_ptvarcone30_CompBeforeAfterCorr.pdf}
\includegraphics[scale = 0.4]{figures/close_by/WH_muon_ptvarcone30_CompBeforeAfterCorr_Zoomed.pdf}
\caption{The distributions of $p_{\text{T}}^{\text{varcone}30}$ of the muons before 
(blue) and after (red) correction for close-by objects. Only muons for which the 
correction was not nil and passing the cuts are selected. The right plot shows a view 
close to $0$ of the distributions presented in the left plot. The corrected isolation 
has its distribution shifted to lower values and should not have negative values.}
\label{fig:muon_ptvarcone30_CompBeforeAfterCorr}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/WH_(muon_ptvarcone30)_Plot2D.pdf}
\includegraphics[scale = 0.4]{figures/close_by/WH_muon_ptvarcone30_WithWithoutCorr.pdf}
\caption{The left plot shows the repartition of the $p_{\text{T}}^{\text{varcone}30}$ 
before and after correction for the muons passing the cuts. The diagonal indicates 
the muons for which the correction was negligible. The lower points indicate 
muons which become fully isolated after correction. The right plot shows the 
distributions of the corrected $p_{\text{T}}^{\text{varcone}30}$ of the muons passing 
the cuts for which no correction are expected (blue) and a correction is expected 
(red). Ideally the two distributions should look similar.}
\label{fig:muon_ptvarcone30_2DPlot_and_WithWithoutCorrection}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/WH_muon_topoetcone20_CompBeforeAfterCorr.pdf}
\caption{The distributions of $E_{\text{T}}^{\text{topoetcone}20}$ of the muons 
before (blue) and after (red) correction for close-by objects. Only muons for which 
the correction was not nil and passing the cuts are selected. The corrected isolation has 
its distribution shifted to lower values.}
\label{fig:muon_topoetcone20_CompBeforeAfterCorr}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/WH_(muon_topoetcone20)_Plot2D.pdf}
\includegraphics[scale = 0.4]{figures/close_by/WH_muon_topoetcone20_WithWithoutCorr.pdf}
\caption{The left plot shows the repartition of the $E_{\text{T}}^{\text{topoetcone}20}$ 
before and after correction for the muons passing the cuts. The diagonal indicates 
the muons for which the correction was negligible. The lower points indicate 
muons which become fully isolated after correction. The right plot shows the 
distributions of the corrected $E_{\text{T}}^{\text{cone}20}$ of the muons passing 
the cuts for which no correction are expected (blue) and a correction is expected 
(red). Ideally the two distributions should look similar.}
\label{fig:muon_topoetcone20_2DPlot_and_WithWithoutCorrection}
\end{figure}

\FloatBarrier

\subsubsection{Conclusions}

As a conclusion, the behaviour of the corrected variables is as expected for both
electrons and muons. The corrected distributions look very close to the uncorrected 
distributions in the case no correction should be needed, especially for muons.

\subsubsection{The topocluster information}

This last study concerns the information of the topoclusters. Indeed, theis information 
are needed to subtract the contribution of the close-by objects for the 
$E_{\text{T}}^{\text{topoetcone}XX}$ variables. In case this information is not 
available, models are available to approximate the correction to apply. However, these 
models may overcorrect the isolation variables in case some topoclusters are shared among 
various objects (there may be over-counting of the energy to be removed). 

Therefore, the number of objects (electrons or muons) which share the same topocluster is 
calculated. Figure \ref{fig:topocluster_sharedObjects} presents the distributions of the 
number of objects which share a same topocluster (for each topocluster $c$, the number 
of objects $\lambda$ verifying ${\Delta{}R}_{c, \lambda}\leq{\Delta{}R}_{\text{core}}$). 
Ideally, this number should not exceed $1$, case where the topocluster is only used to 
calculate the core energy of one object.

The conclusion of this study is that topoclusters are shared among objects in less than 
one permil of the cases. Therefore the models should not suffer from this.

\begin{figure}[htbp]
\centering
\includegraphics[scale = 0.4]{figures/close_by/sharedClustersElectrons.pdf}
\includegraphics[scale = 0.4]{figures/close_by/sharedClustersMuons.pdf}
\caption{This figure shows the number of (left) electrons and (right) muons, which share 
a same topocluster in the calculation of their topocore energy. If two objects share 
the same topoclusters, the models developed to approximate the correction for close-by 
objects may overestimate the correction. For both electrons and muons, the fraction of 
events in which this happens (number greater than 1) is negligible and the models should 
not be impacted too much by this effect.}
\label{fig:topocluster_sharedObjects}
\end{figure}

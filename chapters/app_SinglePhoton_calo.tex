\section{Calorimeter Isolation Efficiency and Scale Factors in Single Photons}
\label{sec:app_SinglePhoton_calo}

In order to determine scale factors for calorimetric photon isolation, the efficiency of the relevant selection must be obtained for both Monte Carlo and for data containing only true photons. The efficiency is obtained by fitting the shape of the calorimetric isolation variable, \textit{topoETcone40} or \textit{topoETcone20}, minus the relevant $p_T$ farction ($0.022 \times p_{T}$ for $FixedCutTight$ and $FixedCutTightCaloOnly$ and $0.065 \times p_{T}$ for $FixedCutLoose$). By definition, events which fall to the left of the cut value ($2.45$ GeV for $FixedCutTight$ and $FixedCutTightCaloOnly$ and $0.0$ GeV for $FixedCutLoose$) pass the calorimetric-isolation selection. The efficiency is then the integral of the events below this value divided by the total integral. The primary complication of this process arises when determining the shape of only true photons in data, since the single photon data sample is contaminated by a significant number of fake photons passing identification requirements. Subtracting the fake photon shape involves a process of iterative fits, which is described below in detail.  

Scale factors are determined for fifteen bins in transverse momentum, corresponding with the single photon trigger thresholds: (25,30), (30,35), (35,40), (40,45), (45,50), (50,55), (55,65), (65,75), (75,85), (85,105), (105,125), (125,150), (150,175), (175,250), and (250,1500) GeV. The scale factors are binned in six bins in the absolute-value of pseudo-rapidity: (0,0.60), (0.60,0.82), (0.82,1.15), (1.15,1.37), (1.52,1.81) and (1.81,2.37). This choice of $|\eta|$ binning is the same as that used by the photon identification group, and the bins correspond to different detector regions. The scale factors are provided separately for converted or unconverted photons, as well as separately for the 2015-2016 and 2017 data-taking periods. 

Pythia8 Monte Carlo $\gamma +jet$ samples are used for the true photon template, with true photons defined as prompt photons from either the $q\bar{q}\rightarrow \gamma g $ process or the box-diagram $qg\rightarrow q \gamma$ process. The background template is obtained using the Loose' 4 ID-region, defined as photon candidates failing identification requirements for one of the following shower shape variables: $F_{side}$, $w_{s3}$, $\Delta E$ or $E_{ratio}$. Although background-enriched, the loose region also contains some amount of true photons failing the tight ID requirement, defined as ``leakage'' photons.

Unbinned fits are performed on both the data and Monte Carlo samples. The samples include respective weights, such as pileup re-weighting on Monte Carlo and trigger weights on data. The sequence of fits proceeds as follows: 

\begin{figure}
\centering
\subfloat[\emph{Loose'4 Monte Carlo}]{\includegraphics[width=0.45\textwidth]{photon_caloIso_sf/FitExamplePlots/{loosePythiaCB0}}}
\subfloat[\emph{Tight Monte Carlo}]{\includegraphics[width=0.45\textwidth]{photon_caloIso_sf/FitExamplePlots/{tightPythiaACB0}}}
\caption{The (a) Crystal Ball fit to Loose'4 and (b) Asymmetric Crystal Ball fit to Tight photons from Pythia 8 simulation.\label{fig:photoncalosf_fits_pythia}}
\end{figure}

\begin{itemize}

\item An initial fit to simulated tight photons (photons passing the ``Tight'' ID requirement) is performed using an Asymmetric Crystal Ball function. An example of this fit can be seen in Figure \ref{fig:photoncalosf_fits_pythia}a. 

\item A fit to simulated loose photons (photons passing the ``Loose'4'' ID requirement) is performed using a Crystal Ball function. An example of this fit can be seen in Figure \ref{fig:photoncalosf_fits_pythia}b. 

\item A naive two-component fit is performed to loose photons in data. An unconstrained Crystal Ball function is used to fit the fake photon shape, and the Crystal Ball fit to loose photons in Monte Carlo is used for the shape of the leakage photons. An example of this two-component fit can be seen in Figure \ref{fig:photoncalosf_fits_initialdata}a, where the leakage component is shown in blue, the fake component is shown in green, and the total fit is shown in red. %Note that this fit consistently overestimates the number of leakage photons in the data sample compared to the prediction of the Monte Carlo. 

\item A naive two-component fit is performed on the tight data sample in order to estimate $N_{True, Tight-Data}$. The fake-photon (``background'') component of the sample is fitted with the background shape from the fit to loose data (the red curve in Figure \ref{fig:photoncalosf_fits_initialdata}a), and the true photon (``signal'') component is fitted with the shape obtained from the fit to tight Monte Carlo (the curve in Figure \ref{fig:photoncalosf_fits_pythia}b). An example of this fit is shown in Figure \ref{fig:photoncalosf_fits_initialdata}b. 

\end{itemize}

\begin{figure}
\centering
\subfloat[\emph{Loose'4 Data (Initial)}]{\includegraphics[width=0.45\textwidth]{photon_caloIso_sf/FitExamplePlots/{looseDataBkgCB1SigCB1}}}
\subfloat[\emph{Tight Data (Initial)}]{\includegraphics[width=0.45\textwidth]{photon_caloIso_sf/FitExamplePlots/{tightDataBkgMIXSigACB}}}
\caption{The initial two-component fit to (a) loose data and (b) tight data. In the fit to loose data, the blue line shows the fit to the leakage (true photons failing tight ID) component, and the green line shows the fit to the the fake photon component. The red line shows the total fit. In the fit to tight data, the red line shows the fitted background shape, which is taken from the red curve in (a). The blue line shows the total fit to signal (real photons) and background.\label{fig:photoncalosf_fits_initialdata}}
\end{figure}

A more precise prediction for the number of leakage photons expected in the Loose'4 data is taken from the ratio in Monte Carlo, and it is used to better constrain the leakage shape in loose data. If the number of true photons in the tight-ID data sample is known, then the prediction for the number of leakage photons in the loose data is simply given by: $$N_{Leak, Loose-Data} = N_{True, Tight-Data} \times \frac{N_{Loose-MC}}{N_{Tight-MC}}$$ where $N_{True, Tight-Data}$ is the number of true photons in the tight data sample, $N_{Loose-MC}$ is the number of photons in the Loose'4 Monte Carlo sample and $N_{Tight-MC}$ is the number of photons in the tight Monte Carlo sample (note again that the Monte Carlo only contains true photons). However, the tight data sample is contaminated with some amount of fake photons passing the identification requirement, and so the estimation of $N_{True, Tight-Data}$ is non-trivial. 

The total number of events in the tight data sample, subtracting the integral of the background component (the red curve in Figure \ref{fig:photoncalosf_fits_initialdata}b), gives an estimate of the number of true photons in the tight data. Because the total background shape includes real photons from leakage, though, this prediction will underestimate $N_{True, Tight-Data}$.

A second estimate of $N_{True, Tight-Data}$ can be obtained by assuming that the leakage shape fitted in the loose data fit (the blue component in Figure \ref{fig:photoncalosf_fits_initialdata}a) contains entirely true photons. $N_{True, Tight-Data}$ is then estimated by subtracting only the integral of the fake part of the background shape (the green component in Figure \ref{fig:photoncalosf_fits_initialdata}a) from the total number of events. Due to the consistent overestimation of the number of leakage events by the naive loose data fit, this second prediction will overestimate the value of $N_{True, Tight-Data}$.

Since the two estimates of $N_{True, Tight-Data}$ are expected to deviate in opposite directions, an average of the two is taken to give the final prediction of $N_{True, Tight-Data}$. The differences between the final $N_{True, Tight-Data}$ value and the two predictions is taken as the error. The number of leakage events $N_{Leak, Loose-Data}$ can then be determined using the above formula. The fitting procedure with the updated leakage prediction continues as follows:

\begin{itemize}

\item The two-component fit to loose data is performed again, with the number of leakage events constrained to $N_{Leak, Loose-Data}$ with a Poisson constraint. This second fit tends to show a decrease in the fraction of fitted leakage events, as shown in the blue component of the example fit in Figure \ref{fig:photoncalosf_fits_finaldata}. The red component shows the fit to fake photon component of the loose data, while the green component shows the total fit. 

\item The final two-component fit is performed on tight data. The signal shape is again taken from the fit to tight Monte Carlo (from Figure \ref{fig:photoncalosf_fits_pythia}). The background shape is taken from the fake photon component of the second fit to loose data (the red component in Figure \ref{fig:photoncalosf_fits_finaldata}a). An example fit can be seen in Figure \ref{fig:photoncalosf_fits_finaldata}b, where the red component shows the predicted background and the blue shows the total fit to signal and background.

\item The background (red component) of the two-component fit is subtracted out of the tight data. The remainder is the best estimate for the shape of true photons in the tight data. An example of the background-subtracted data shape can be seen in Figure \ref{tightDataBkgSub}. 

\end{itemize}

The efficiency in data can finally be obtained by integrating the background-subtracted tight data up to the working point cutoff of $2.45$ GeV ($FixedCutTight$ and $FixedCutTightCaloOnly$) or $0.0$ GeV ($FixedCutLoose$). 

\begin{figure}
\centering
\subfloat[\emph{Loose'4 Data (Final)}]{\includegraphics[width=0.45\textwidth]{photon_caloIso_sf/FitExamplePlots/{looseDataBkgCB2SigCB2}}}
\subfloat[\emph{Tight Data (Final)}]{\includegraphics[width=0.45\textwidth]{photon_caloIso_sf/FitExamplePlots/{tightDataBkgCB2SigACB2}}}
\caption{The final two-component fit to (a) loose data and (b) tight data. In the fit to loose data, the blue line shows the fit to the leakage (true photons failing tight ID) component, and the red line shows the fit to the the fake photon component. The green line shows the total fit. In the fit to tight data, the red line shows the fitted background shape, which is taken from the red curve in (a). The blue line shows the total fit to signal (real photons) and background.\label{fig:photoncalosf_fits_finaldata}}
\end{figure}

\begin{figure}[h]
 \centering
   \includegraphics[width=0.65\textwidth]{photon_caloIso_sf/FitExamplePlots/{tightDataBkgSub}}
 \caption{An example of the final background-subtracted data. The signal component of the two-component fit to tight data (the blue component in Figure \ref{fig:photoncalosf_fits_finaldata}b is shown, again in blue, while the background-subtracted data is shown with the black points. The residuals are plotted below. }
 \label{tightDataBkgSub}
\end{figure}

Three sources of systematic error are considered: estimation of the number of leakage events in loose data, differences between the Loose'-ID regions, and discrepancies between the fit and data shapes. A binomial statistical error on the scale factors is also calculated and added in quadrature with the systematic components. 

To determine the error from the leakage estimate in loose data, the fits are performed with both the overestimate and underestimate of the number of leakage events, which are discussed above. The maximum discrepancy between the $N_{overestimate}$ and $N_{underestimate}$ scale factor value in each $p_{T}/\eta/$conversion bin from the nominal result is taken as the error. 

To determine the error from the choice of Loose'-ID region, the fitting process is performed using the Loose'3 and Loose'5 regions of data and Monte Carlo. The Loose'3 (Loose'5) region is defined as photon candidates failing identification requirements for one of the following shower shape variables: $F_{side}$, $w_{s3}$, or $\Delta E$ ( $F_{side}$, $w_{s3}$, $\Delta E$, $E_{ratio}$, or $w_{stot}$). The Loose'2 region was not included, as many bins in this region suffer from poor statistics which prevent the fits from converging reasonably. The maximum discrepancy between the Loose'3 and Loose'5 results from the nominal Loose'4 result is taken as the error value. This error is also calculated uniquely for each $p_{T}/\eta/$conversion bin. 

The contribution from the fit quality is included as a scaling on the statistical error, and it is obtained from the $\sqrt{\chi^2/(N_{bins}-1)}$ value of the fits. The $\chi^2$ is calculated from two histograms with 100 bins, one created from the fit to tight Monte Carlo and the other containing the tight Monte Carlo events. Only the bins which contain a significant number of entries are included in the calculation. The $\chi^2/(N_{bins}-1)$ values are calculated separately for each $p_{T}/\eta/$conversion bin and then averaged over the fourteen $p_{T}$ bins for a given $\eta/$conversion bin. The binomial statistical component of the error is then scaled by this averaged $\sqrt{\chi^2/(N_{bins}-1)}$ factor. 

To obtain the total error, the two systematic (Loose'-ID choice and leakage estimate) components are added in quadrature with the scaled statistical component. A breakdown of the systematic uncertainty components can be found in Figures \ref{fig:SystErrorsFixedCutLoose}, \ref{fig:SystErrorsFixedCutTight}, and \ref{fig:SystErrorsFixedCutTightCaloOnly} for the $FixedCutLoose$, $FixedCutTight$, and $FixedCutTightCaloOnly$ working points, respectively. 

Two scale factor bins in the $FixedCutTightCaloOnly$ working point for 2017 data with MC16d were observed to have anomalous behavior due to issues in the background fits. The data efficiency in these bins can be observed to decrease suddenly in Figure \ref{fig:Efficiencies_FixedCutTightCaloOnly} with respect to the neighboring efficiencies. The first bin was that for unconverted photons with $105<E_{T}<125$ GeV and $1.15<|\eta|<1.37$. The second was for converted photons with $150<E_{T}<175$ GeV and $1.81<|\eta|<2.37$. For these bins, the scale factor was set to the mean of the neighboring values in $E_{T}$, as the scale factors are generally smooth accross $E_{T}$. The error assigned to these bins was set such that it incorporates the full range of the error bars in either of the neighboring bins. 

The final scale factors for photon calorimetric isolation from single photons are provided in Figures \ref{fig:ScaleFactors_FixedCutLoose}, \ref{fig:ScaleFactors_FixedCutTight}, and \ref{fig:ScaleFactors_FixedCutTightCaloOnly} for the $FixedCutLoose$, $FixedCutTight$, and $FixedCutTightCaloOnly$ working points, respectively. The data and MC efficiencies are provided in Figures \ref{fig:Efficiencies_FixedCutLoose}, \ref{fig:Efficiencies_FixedCutTight}, and \ref{fig:Efficiencies_FixedCutTightCaloOnly}. 


%%% The Breakdown of Systematic Errors

\begin{figure}
\centering
\subfloat[\emph{2015+2016 Data with MC16a, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{SystErrors/allSystErrors_FixedCutLoose_topoETcone20pt_unconv_2016.eps}}} \\
\subfloat[\emph{2015+2016 Data with MC16a, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{SystErrors/allSystErrors_FixedCutLoose_topoETcone20pt_conv_2016.eps}}} \\
\subfloat[\emph{2017 Data with MC16d, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{SystErrors/allSystErrors_FixedCutLoose_topoETcone20pt_unconv_2017.eps}}} \\
\subfloat[\emph{2017 Data with MC16d, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{SystErrors/allSystErrors_FixedCutLoose_topoETcone20pt_conv_2017.eps}}} \\
\caption{The breakdown of the systematic errors for the FixedCutLoose scale factors for (a) unconverted photons in 2015+2016 Data and MC16a, (b) converted photons in 2015+1016 Data and MC16a, (c) unconverted photons in 2017 Data and MC16d, and (d) converted photons in 2017 Data and MC16d. The error values versus photon $E_{T}$ are plotted separately for each bin in $|\eta|$. The leakage error is shown in magenta, and the $Loose'$ error is shown in blue. The raw statistical error is shown in red, and the the scaled statistical error (to account for the fit error) is shown in green. The black line shows the total error.}
\label{fig:SystErrorsFixedCutLoose}
\end{figure}


\begin{figure}
\centering
\subfloat[\emph{2015+2016 Data with MC16a, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{SystErrors/allSystErrors_FixedCutTight_topoETcone40pt_unconv_2016.eps}}} \\
\subfloat[\emph{2015+2016 Data with MC16a, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{SystErrors/allSystErrors_FixedCutTight_topoETcone40pt_conv_2016.eps}}} \\
\subfloat[\emph{2017 Data with MC16d, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{SystErrors/allSystErrors_FixedCutTight_topoETcone40pt_unconv_2017.eps}}} \\
\subfloat[\emph{2017 Data with MC16d, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{SystErrors/allSystErrors_FixedCutTight_topoETcone40pt_conv_2017.eps}}} \\
\caption{The breakdown of the systematic errors for the FixedCutTight scale factors for (a) unconverted photons in 2015+2016 Data and MC16a, (b) converted photons in 2015+1016 Data and MC16a, (c) unconverted photons in 2017 Data and MC16d, and (d) converted photons in 2017 Data and MC16d. The error values versus photon $E_{T}$ are plotted separately for each bin in $|\eta|$. The leakage error is shown in magenta, and the $Loose'$ error is shown in blue. The raw statistical error is shown in red, and the the scaled statistical error (to account for the fit error) is shown in green. The black line shows the total error.}
\label{fig:SystErrorsFixedCutTight}
\end{figure}


\begin{figure}
\centering
\subfloat[\emph{2015+2016 Data with MC16a, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{SystErrors/allSystErrors_FixedCutTightCaloOnly_topoETcone40pt_unconv_2016.eps}}} \\
\subfloat[\emph{2015+2016 Data with MC16a, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{SystErrors/allSystErrors_FixedCutTightCaloOnly_topoETcone40pt_conv_2016.eps}}} \\
\subfloat[\emph{2017 Data with MC16d, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{SystErrors/allSystErrors_FixedCutTightCaloOnly_topoETcone40pt_unconv_2017.eps}}} \\
\subfloat[\emph{2017 Data with MC16d, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{SystErrors/allSystErrors_FixedCutTightCaloOnly_topoETcone40pt_conv_2017.eps}}} \\
\caption{The breakdown of the systematic errors for the FixedCutTightCaloOnly scale factors for (a) unconverted photons in 2015+2016 Data and MC16a, (b) converted photons in 2015+1016 Data and MC16a, (c) unconverted photons in 2017 Data and MC16d, and (d) converted photons in 2017 Data and MC16d. The error values versus photon $E_{T}$ are plotted separately for each bin in $|\eta|$. The leakage error is shown in magenta, and the $Loose'$ error is shown in blue. The raw statistical error is shown in red, and the the scaled statistical error (to account for the fit error) is shown in green. The black line shows the total error.}
\label{fig:SystErrorsFixedCutTightCaloOnly}
\end{figure}


%%% The final Scale Factors plots 

\begin{figure}
\centering
\subfloat[\emph{2015+2016 Data with MC16a, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{FinalSFs/ScaleFactors_FixedCutLoose_wSystErrors_2016_unconv.eps}}} \\
\subfloat[\emph{2015+2016 Data with MC16a, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{FinalSFs/ScaleFactors_FixedCutLoose_wSystErrors_2016_conv.eps}}} \\
\subfloat[\emph{2017 Data with MC16d, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{FinalSFs/ScaleFactors_FixedCutLoose_wSystErrors_2017_unconv.eps}}}\\
\subfloat[\emph{2017 Data with MC16d, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{FinalSFs/ScaleFactors_FixedCutLoose_wSystErrors_2017_conv.eps}}} \\
\caption{The FixedCutLoose scale factors for (a) unconverted photons in 2015+2016 Data and MC16a, (b) converted photons in 2015+1016 Data and MC16a, (c) unconverted photons in 2017 Data and MC16d, and (d) converted photons in 2017 Data and MC16d. The scale factor value versus photon $E_{T}$ is plotted separately for each bin in $|\eta|$. The systematic error is shown in the red portion of error bands, while the total error (systematic plus scaled statistical) is shown in blue.}
\label{fig:ScaleFactors_FixedCutLoose}
\end{figure}


\begin{figure}
\centering
\subfloat[\emph{2015+2016 Data with MC16a, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{FinalSFs/ScaleFactors_FixedCutTight_wSystErrors_2016_unconv.eps}}} \\
\subfloat[\emph{2015+2016 Data with MC16a, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{FinalSFs/ScaleFactors_FixedCutTight_wSystErrors_2016_conv.eps}}} \\
\subfloat[\emph{2017 Data with MC16d, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{FinalSFs/ScaleFactors_FixedCutTight_wSystErrors_2017_unconv.eps}}}\\
\subfloat[\emph{2017 Data with MC16d, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{FinalSFs/ScaleFactors_FixedCutTight_wSystErrors_2017_conv.eps}}} \\
\caption{The FixedCutTight scale factors for (a) unconverted photons in 2015+2016 Data and MC16a, (b) converted photons in 2015+1016 Data and MC16a, (c) unconverted photons in 2017 Data and MC16d, and (d) converted photons in 2017 Data and MC16d. The scale factor value versus photon $E_{T}$ is plotted separately for each bin in $|\eta|$. The systematic error is shown in the red portion of error bands, while the total error (systematic plus scaled statistical) is shown in blue.}
\label{fig:ScaleFactors_FixedCutTight}
\end{figure}


\begin{figure}
\centering
\subfloat[\emph{2015+2016 Data with MC16a, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{FinalSFs/ScaleFactors_FixedCutTightCaloOnly_wSystErrors_2016_unconv.eps}}} \\
\subfloat[\emph{2015+2016 Data with MC16a, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{FinalSFs/ScaleFactors_FixedCutTightCaloOnly_wSystErrors_2016_conv.eps}}} \\
\subfloat[\emph{2017 Data with MC16d, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{FinalSFs/ScaleFactors_FixedCutTightCaloOnly_wSystErrors_2017_unconv.eps}}}\\
\subfloat[\emph{2017 Data with MC16d, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/{FinalSFs/ScaleFactors_FixedCutTightCaloOnly_wSystErrors_2017_conv.eps}}} \\
\caption{The FixedCutTightCaloOnly scale factors for (a) unconverted photons in 2015+2016 Data and MC16a, (b) converted photons in 2015+1016 Data and MC16a, (c) unconverted photons in 2017 Data and MC16d, and (d) converted photons in 2017 Data and MC16d. The scale factor value versus photon $E_{T}$ is plotted separately for each bin in $|\eta|$. The systematic error is shown in the red portion of error bands, while the total error (systematic plus scaled statistical) is shown in blue.}
\label{fig:ScaleFactors_FixedCutTightCaloOnly}
\end{figure}


%%% The Efficiencies (Data and MC) 

\begin{figure}
\centering
\subfloat[\emph{2015+2016 Data with MC16a, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/Efficiencies/Efficiency_FixedCutLoose_unconv_2016.eps}} \\
\subfloat[\emph{2015+2016 Data with MC16a, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/Efficiencies/Efficiency_FixedCutLoose_conv_2016.eps}} \\
\subfloat[\emph{2017 Data with MC16d, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/Efficiencies/Efficiency_FixedCutLoose_unconv_2017.eps}}\\
\subfloat[\emph{2017 Data with MC16d, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/Efficiencies/Efficiency_FixedCutLoose_conv_2017.eps}} \\
\caption{The FixedCutLoose MC and data efficiencies for (a) unconverted photons in 2015+2016 Data and MC16a, (b) converted photons in 2015+1016 Data and MC16a, (c) unconverted photons in 2017 Data and MC16d, and (d) converted photons in 2017 Data and MC16d. The scale factor value versus photon $E_{T}$ is plotted separately for each bin in $|\eta|$. The systematic error is shown in the red portion of error bands, while the total error (systematic plus scaled statistical) is shown in blue.}
\label{fig:Efficiencies_FixedCutLoose}
\end{figure}


\begin{figure}
\centering
\subfloat[\emph{2015+2016 Data with MC16a, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/Efficiencies/Efficiency_FixedCutTight_unconv_2016.eps}} \\
\subfloat[\emph{2015+2016 Data with MC16a, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/Efficiencies/Efficiency_FixedCutTight_conv_2016.eps}} \\
\subfloat[\emph{2017 Data with MC16d, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/Efficiencies/Efficiency_FixedCutTight_unconv_2017.eps}}\\
\subfloat[\emph{2017 Data with MC16d, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/Efficiencies/Efficiency_FixedCutTight_conv_2017.eps}} \\
\caption{The FixedCutTight MC and data efficiencies for (a) unconverted photons in 2015+2016 Data and MC16a, (b) converted photons in 2015+1016 Data and MC16a, (c) unconverted photons in 2017 Data and MC16d, and (d) converted photons in 2017 Data and MC16d. The scale factor value versus photon $E_{T}$ is plotted separately for each bin in $|\eta|$. The systematic error is shown in the red portion of error bands, while the total error (systematic plus scaled statistical) is shown in blue.}
\label{fig:Efficiencies_FixedCutTight}
\end{figure}


\begin{figure}
\centering
\subfloat[\emph{2015+2016 Data with MC16a, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/Efficiencies/Efficiency_FixedCutTightCaloOnly_unconv_2016.eps}} \\
\subfloat[\emph{2015+2016 Data with MC16a, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/Efficiencies/Efficiency_FixedCutTightCaloOnly_conv_2016.eps}} \\
\subfloat[\emph{2017 Data with MC16d, Unconverted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/Efficiencies/Efficiency_FixedCutTightCaloOnly_unconv_2017.eps}}\\
\subfloat[\emph{2017 Data with MC16d, Converted}]{\includegraphics[width=0.60\textwidth]{photon_caloIso_sf/Efficiencies/Efficiency_FixedCutTightCaloOnly_conv_2017.eps}} \\
\caption{The FixedCutTightCaloOnly MC and data efficiencies for (a) unconverted photons in 2015+2016 Data and MC16a, (b) converted photons in 2015+1016 Data and MC16a, (c) unconverted photons in 2017 Data and MC16d, and (d) converted photons in 2017 Data and MC16d. The scale factor value versus photon $E_{T}$ is plotted separately for each bin in $|\eta|$. The systematic error is shown in the red portion of error bands, while the total error (systematic plus scaled statistical) is shown in blue.}
\label{fig:Efficiencies_FixedCutTightCaloOnly}
\end{figure}

\clearpage
\subsection{Large Scale Factors in FixedCutTightCaloOnly Working Point}

The scale factors for high-$E_T$ photons in the \textit{FixedCutTightCaloOnly} working point were observed to be as large as about $1.1$, such as for the $0.0<|\eta|<0.6$ pseudo-rapidity bin for unconverted photons in the 2017 data-taking period. The efficiencies and scale factors are shown together in Figure \ref{fig:FixedCutTightCaloOnly_LargeSF}. The data efficiency appears to increase with $E_T$ at a faster rate than the MC efficiency, which causes the scale factor value to increase. 

The quality of the final fit to tight data was investigated for the highest $E_T$ bin, since poor background subtraction could lead to an overestimation of the data efficiency. This fit is shown in Figure \ref{fig:FixedCutTightCaloOnly_LargeSF_SBfit}. There does not appear to be significant background contamination in this fit. In addition, the fraction of fake photon events in the high-$E_T$ region is typically low, as can be seen in the fit. The signal shape does appear to show some mis-modeling; however, the data efficiency is calculated from the actual data distribution (minus the background shape) and not from the signal component of the fit. Therefore, this mis-modeling is unlikely to explain the large scale factor in this bin. 

\begin{figure}
\centering
\includegraphics[width=0.55\textwidth]{photon_caloIso_sf/extraFixedCutTightCaloOnly/SFFixedCutTightCaloOnly_2017_unconv.pdf}
\caption{The efficiencies for unconverted photons in the rapidity range $0.0<|\eta|<0.6$ for the \textit{FixedCutTightCaloOnly} working point. The Data efficiency is shown in black, and the Monte Carlo efficiency is shown in red. The bottom panel contains the scale factor values ($\epsilon_{Data}/\epsilon_{MC}$). Both statistical and systematic uncertainties (fit error, leakage uncertainty, and Loose' uncertainty) are shown in the error bars.}
\label{fig:FixedCutTightCaloOnly_LargeSF}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.55\textwidth,angle=270]{photon_caloIso_sf/extraFixedCutTightCaloOnly/tightDataBkgCB2SigACB2_topoETcone40pt_2017_unconv_pT_105_125_absEta_115_137_Loose4_NominalLeakage.pdf}
\caption{The final signal and background fit to tight data in for unconverted photons in the $0.0<|\eta|<0.6$ region in 2017 data. The background (fake photon) shape is shown in red, while the total signal plus background shape is shown in blue.}
\label{fig:FixedCutTightCaloOnly_LargeSF_SBfit}
\end{figure}
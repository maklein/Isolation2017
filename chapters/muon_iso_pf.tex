The particle flow algorithm~\cite{ParticleFlow} aims at optimally combining the tracker and calorimeter measurements to reconstruct particles in an event. It takes advantage of the better energy and angular resolution and better pile-up resilience of the tracker over the calorimeter at low \pt, while using the ability for the calorimeter to reconstruct neutral particles. To achieve this, a boolean decision is taken wether to use the tracker or calorimeter measurement for each particle, and in case the tracker is used, one should subtract the corresponding energy of the particle from the calorimeter to avoid any double counting. Details on the particle-flow algorithm can be found in ref.~\cite{ParticleFlow}, and only a very short summary of the algorithm is given here.


First, one selects good-quality tracks with $0.5<\pt (\gev)< 40$ (above $40\gev$, it is more beneficial to use the calorimeter measurement). Since the particle algorithm is optimized for pions, electrons (with $\pt>10\gev$, and passing a medium identification criterium) and muons (with $\pt>5\gev$ and $\chi^2/ndof>10$) are removed from the track selection. Then, one reconstructs topological clusters: it is worth noting that these clusters are not meant to separate energy deposits from different particles, but rather to separate energy showers of different nature (electromagnetic or hadronic) and to suppress noise. Therefore, most of the time, a particle will yield several topological clusters, and one cluster can have contributions from several particles. These topological clusters are then calibrated for pions response. 

The tracks are then matched to clusters by extrapolating each track into the calorimeter. The distance $\Delta R' $ between the extrapolated track position and the cluster barycenter ($ \Delta \eta$ and $\Delta \phi$), normalized by the expected standard deviation of the cells in a pion cluster $\sigma_\eta$ and $\sigma_{\phi}$ is computed as: 
\begin{equation}
\Delta R' = \sqrt{\left( \Delta \eta / \sigma_{\eta}\right)^2+\left(\Delta \phi / \sigma_{\phi}\right)^2},
\end{equation}
and the closest cluster with $E_\textrm{cluster}/p_\textrm{track}>0.1$ in $\Delta R' $ is considered for subtraction. If the energy of the matched cluster is less than the expected $E_\textrm{cluster}/p_\textrm{track}$ (determined in single particle samples, and binned as a function of $\eta$, energy of the incoming particle, and where the particle showers in the calorimeter) by more than the width of $E_\textrm{cluster}/p_\textrm{track}$, then it is assumed that the particle has deposited its energy in several topological clusters. In this case, all clusters within a cone of $0.2$ around the extrapolated track are considered for subtraction. Finally, if the closest cluster is farther away than $1.64$, then no subtraction is performed. 

The next step is the energy subtraction itself: if the energy of the set of matched clusters is less than the expected $E_\textrm{cluster}/p_\textrm{track}$, then the full set is removed. Otherwise, the subtraction is done cell by cell, amongst selected rings of cells in each layer by scaling down some of the ring cells energy in order to reach the expected energy that should be removed.
The remaining cells are either removed as well if they are consistent within $1.5$ width of the expected $E_\textrm{cluster}/p_\textrm{track}$, or kept as a remaining cluster otherwise. 

At the end of the algorithm, the selected tracks (charged particle flow objects) and remaining clusters (neutral particle flow objects) should thus represent the reconstructed event without any double counting of energy. 

The particle flow isolation consists in a charged part, that is equivalent to the track isolation described in section~\ref{subsec:muon_trackiso} (so it is not built, one can simply use the standard track isolation), and a neutral part called \textbf{neflowisol}, built in a similar way then the previously described calorimeter isolation in section~\ref{subsec:muon_caloiso} but using the neutral energy flow objects instead of the raw topological clusters as input. 

Dedicated corrections are applied to the neutral particle flow isolation:
\begin{itemize}
\item the core subtraction can only be done using the \textbf{coreCone} technique described in section~\ref{subsec:muon_caloiso}, because the topological clusters used in the particle flow are calibrated for pions, and therefore it does not make sense to subtract cells at the electromagnetic level as done in \textbf{core57cells}. 
\item the leakage correction is done as for \textbf{topoetcone} using single particle Monte Carlo events, where the lepton/photon is reconstructed with the particle flow algorithm. 
\item the pile-up correction is done using a \rhomedian\ specifically computed using jets built on neutral energy flow objects.
\end{itemize}

\textbf{Neutral pflow vs. topocluster isolation}
\label{subsec:pfcorrmuon}

The performance of neutral energy flow based isolation variables and calorimeter and track based isolation variables is compared. Studies are performed using $t\bar{t}$ Monte Carlo sample: \\ $\mathtt{mc15\_13TeV.410000.PowhegPythiaEvtGen\_P2012\_ttbar\_hdamp172p5\_nonallhad}$.

Correlation plots for $\mathtt{neflowisol, topoetcone}$ and $\mathtt{neflowisol, ptvarcone}$ separately for signal and background muons are shown in Figure~\ref{fig:muon_iso_correlation}. As expected, $\mathtt{neflowisol}$ and $\mathtt{topoetcone}$ are highly correlated, and, since $\mathtt{topoetcone}$ includes also information about charged particles, it has larger values, in particular for background muons, which provides better discrimination between signal and background. No correlation is observed between $\mathtt{neflowisol}$ and $\mathtt{ptvarcone}$ variables.

\begin{figure}[!htb]
\centering
\includegraphics[width=0.45\textwidth]{figures/neflowisol_muon/c_corr_sig_neflowisol_topoetcone_30_zoom}
\includegraphics[width=0.45\textwidth]{figures/neflowisol_muon/c_corr_bkg_neflowisol_topoetcone_30_zoom} \\
\includegraphics[width=0.45\textwidth]{figures/neflowisol_muon/c_corr_sig_neflowisol_ptvarcone_30_zoom}
\includegraphics[width=0.45\textwidth]{figures/neflowisol_muon/c_corr_bkg_neflowisol_ptvarcone_30_zoom}
\caption[Correlation plots]{
  Correlation plots for $\mathtt{neflowisol30, topoetcone30}$ (top) and $\mathtt{neflowisol30, ptvarcone30}$ (bottom) for signal (left) and background muons (right).
  }
\label{fig:muon_iso_correlation}
\end{figure}

\textbf{PFlow WP design}
\label{subsec:pffullperfmuon}

Unlike the non-pflow-WPs, much better performance is observed by adding the track and neutral pflow isolation in some form, rather than applying a cut on each variable separately. .

For the sake of comparison, a simple MVA is used as a baseline, combining the neutral pflow isolation and track isolation with a kNN-100 algorithm, in ttbar simulation.
An example ROC curve comparison is included in fig.~\ref{fig:pflowroc}

I need to fill this in.

\begin{figure}[!htb]
\centering
\includegraphics[width=0.45\textwidth]{pflow_roc/roc_20}
\includegraphics[width=0.45\textwidth]{pflow_roc/roc_30}\\
\includegraphics[width=0.45\textwidth]{pflow_roc/roc_40}
\includegraphics[width=0.45\textwidth]{pflow_roc/roc_50}
\caption[Correlation plots]{
   PFlow performance
  }
\label{fig:pflowroc}
\end{figure}


This section is devoted to a prospective study on photon calorimetric isolation and its performance in high pileup envirnments. Calorimetric photon isolation energy is computed as follows:

\begin{equation}
\label{eq:topoetcone40_iso}
E_{\textrm{T, corr}}^{\textrm{isol}} ( R )= \sum_{r_{i}<R} E_{\textrm{T, raw}}^{\textrm{isol}} - E_{\textrm{T, core}}-E_{\textrm{T,leakage}}(\pt)-E_{\textrm{T,pile-up}}(\eta).
\end{equation}

where:

\begin{itemize}
  \item $\sum  E_{\textrm{T, raw}}^{\textrm{isol}}$ is the sum of raw energy of each positive energy topocluster whose barycentre $r_{i}$ falls within a cone of radius R. This computation includes photon and pileup topoclusters.
  \item $- E_{\textrm{T, core}}$ and $-E_{\textrm{T,leakage}}(\pt)$ are corrections accounting for the photon topocluster. The first term is denominated core subtraction and subtracts the energy of a fixed window of 5x7 cells around the photon direction. The second term accounts for the energy leaking outside the window. 
  \item $-E_{\textrm{T,pile-up}}(\eta) $ is the pileup correction, computed using the ambient energy density $\rho_{median}$ of each event. Two ambient energy densities are used per event: one for the barrel and one for the endcaps.  
 \end{itemize}


%From this expression, it can be seen that all positive energy topological clusters whose barycentre falls within a cone centered around the photon candidate are included in the computation.
The main drawback of considering all topological clusters in the previous computation is that pileup clusters are systematically included. Even though a correction is applied afterwards, this worses the resolution of $E_{\textrm{T, corr}}^{\textrm{isol}}$ with increasing pileup. 


Therefore, two studies are shown in this section. First, a study on the shape of the distribution of $E_{\textrm{T, corr}}^{\textrm{isol}}$. Then, in the second part, a prospective study on the information available in the topological clusters that could make photon calorimetric isolation performances more robust against pileup.


\subsection{Photon calorimetric isolation shape and pileup}
\label{subsec:calo_iso_pu_shape}
In contrast to photon track isolation, which is robust against pileup, calorimetric photon isolation is highly dependent on pileup clusters which fall close to the photon candidate. In this section the effects of pileup in the isolation variable \textbf{topoetcone40} shape are shown. 

\subsubsection{Samples}
 \label{subsubsec:calo_iso_pu_samples} 
In this study the official SinglePhoton ntuples for both data and MC are used\footnote{https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HgammaInclusivePhotonSamples}:

 \begin{itemize}
        \item Data 2015+2016 (data15+16), \quad\quad$ data15\_data16\_merge\_v17.root$ 
        \item MC16a, \quad\quad $ PyPt17\_inf\_mc16a\_v17$ mcid = 423104
        \item MC16d, \quad\quad$ PyPt17\_inf\_mc16d\_v17$ mcid = 423104
         \item Data 2017 (data17), \quad\quad$data17\_328263\_336548\_GRL\_v17.root$
    \end{itemize}

where MC16a has the data 2015+2016 $<\mu>$ profile ( Figure \ref{fig:mu_hlt}). Only photons with $145<p_{T}<205$ GeV are selected due to the fact that photons passing the $HLT\_g140\_loose$ trigger are not prescaled, and hence more data are available.

\begin{figure}[h]
 \centering
  \includegraphics[width=0.45\textwidth]{isolation_pileup/mu_histo.pdf}
 	\includegraphics[width=0.45\textwidth]{isolation_pileup/HLT140.pdf}\\
\caption{ Left: Pileup profile for 2015+2016 data , 2017 data and MC16a and MC16d. Right: Photon spectra showing the available data for photons with $p_{T}>145$ GeV compared to lower $p_{T}$ photons }\label{fig:mu_hlt}
\end{figure}

\subsubsection{Signal and background subsamples}
\label{subsubsec:calo_iso_pu_sig}
Signal photon candidates are required to pass tight ID and  photon track isolation $ptcone20/p_{T}<0.05$. This last requirement decreases the background level and improves the robustness of the subtraction method.

Background template is obtained using loose ID photon candidates that pass photon track isolation $ptcone20/p_{T}<0.05$ but fails tight ID in one of the so called strip shower shape variables, which are asumed to be poorly correlated with photon isolation energy. This subsample is enriched in fakes, although it contains also true photons failing tight ID, defined as leakage photons. 

\subsubsection{Methodology}
\label{subsubsec:calo_iso_pu_method}
Binned fits of $E_{\textrm{T, corr}}^{\textrm{isol}}$ bkg-subtracted distributions are perfomed on both data and MC for different $\eta$ and conversion status categories:

\begin{itemize}
    \item $\eta$: [0,0.6], [0.6,0.8], [0.8,1.15], [1.15,1.37], [1.52,1.81], [1.81,2.01], [2.01,2.37]
    \item Split unconverted and converted photons
		\item $\left<\mu\right>$: from 20 to 60 divided in 12 bins.
    \end{itemize}

In order to obtain the bkg-subtracted distribution, the background distribution is normalized using a region far from the signal distribution. This region must be sufficiently far from the signal, which peaks around $E_{\textrm{T, corr}}^{\textrm{isol}}=0$, but close enough to consider valid the hypothesis of no-correlation between the calorimetric isolation energy and the strip shower shape variables. In this study, the normalizetion region is chosen to be $20<E_{\textrm{T, corr}}^{\textrm{isol}}<30$ GeV. The normalization factor is computed as follows:

\begin{equation}
	N_{factor} = \frac{N_{signal}(20-30 GeV)}{N_{bkg}(20-30 GeV)} .
\end{equation}

Once the normalized background distribution is subtracted from the signal distribution, a fit is perfomed using a Crystal Ball (CB). This fit is perfomed between $-10<E_{\textrm{T, corr}}^{\textrm{isol}}<20$, in order to avoid fitting the normalization region (Figure \ref{fig:iso_fit}). 


\begin{figure}[h]
 \centering
   \includegraphics[width=0.75\textwidth]{isolation_pileup/{topoetcone40_data17_eta_08__115__status_unconv_mu_40_45}}
 \caption{ Left: both distributions for tight photon candidates (signal) and loose'4 photon candidates (bkg); Right: the final bkg-subtracted distribution and fit to a CB. 
 \label{fig:iso_fit} }
\end{figure}


Nevertheless, the fact that it is a simple bkg-subtraction method implies some caveats:

\begin{itemize}
	\item This method does not consider true photons leakage in the Loose'4 ID region.
	% but as it will be shown later, we will look at the dependence of the CB parameters with pileup, in particular $\mu_{CB}$ and $\sigma_{CB}$, which are not considerably afected by a decrease in the peak's height. 
	\item Efficiency and purity measurements are biased due to negative bin contents in the normalization region and due to the leakage photons. 
\end{itemize}

In the following ( Figure \ref{fig:muCB_figures} ) the mean of the CB, $\mu_{CB}$; and the width of the CB, $\sigma_{CB}$; as a function of $\left<\mu\right>$ are shown.

\begin{figure}[!htbp]
  \centering
\includegraphics[width=0.240\linewidth]{isolation_pileup/mean_rms/mean_eta_0__60_status_unconv.pdf}
\includegraphics[width=0.24\linewidth]{isolation_pileup/mean_rms/mean_eta_60__80_status_unconv.pdf}
\includegraphics[width=0.24\linewidth]{isolation_pileup/mean_rms/mean_eta_80__115_status_unconv.pdf}
\includegraphics[width=0.24\linewidth]{isolation_pileup/mean_rms/mean_eta_115__137_status_unconv.pdf}\\
\centering
\includegraphics[width=0.24\linewidth]{isolation_pileup/mean_rms/mean_eta_152__181_status_unconv.pdf}
\includegraphics[width=0.24\linewidth]{isolation_pileup/mean_rms/mean_eta_181__201_status_unconv.pdf}
\includegraphics[width=0.24\linewidth]{isolation_pileup/mean_rms/mean_eta_201__237_status_unconv.pdf}\\
\centering
\includegraphics[width=0.240\linewidth]{isolation_pileup/mean_rms/rms_eta_0__60_status_unconv.pdf}
\includegraphics[width=0.24\linewidth]{isolation_pileup/mean_rms/rms_eta_60__80_status_unconv.pdf}
\includegraphics[width=0.24\linewidth]{isolation_pileup/mean_rms/rms_eta_80__115_status_unconv.pdf}
\includegraphics[width=0.24\linewidth]{isolation_pileup/mean_rms/rms_eta_115__137_status_unconv.pdf}\\
\centering
\includegraphics[width=0.24\linewidth]{isolation_pileup/mean_rms/rms_eta_152__181_status_unconv.pdf}
\includegraphics[width=0.24\linewidth]{isolation_pileup/mean_rms/rms_eta_181__201_status_unconv.pdf}
\includegraphics[width=0.24\linewidth]{isolation_pileup/mean_rms/rms_eta_201__237_status_unconv.pdf}\\
  \caption{There are 4 rows of plots showing $\mu_{CB}$ (first 2 rows) and $\sigma_{CB}$(last 2 rows) as a function of $\left<\mu\right>$ for unconverted photons, in seven different pseudorapidity regions.}
  \label{fig:muCB_figures}
\end{figure}


$\mu_{CB}$ shows some $\left<\mu\right>$-dependent trends, probably due to imperfect pileup corrections. These  are overestimated in the $|\eta|$ bins close to the crack in the barrel and underestimated in the $|\eta|$ bins close to the crack in the endcap. In general, MC16 follows the same trends as data besides some few hundred MeV data/MC shifts in some $\eta$ bins in the endcaps. $\mu_{CB}$ is expected to be robust against pileup as there is a correction acounting for it. 

$\sigma_{CB}$ increases roughly linearly with $\left<\mu\right>$ in all $|\eta|$ bins, which has a negative effect for both purity and efficiency, as the overlap between signal and background shapes is larger. Slopes are larger in the barrel than in the endcaps due to a larger pileup correction in the barrel than in the endcaps, and hence a larger fluctuation. 

\subsubsection{Conclusion on calorimetric isolation performances and pileup}
\label{subsubsec:calo_iso_pu_conclusion}

In order to maintain the current efficiency and purity of photon calorimetric isolation performances with higher pileup, modifications to the algorithm are required to improve its robustness against pileup.

\subsection{Pileup mitigation using cluster-level variables}
\label{subsec:calo_iso_pu_mitigation}

Several topoclusters can be found in the cone of radius XX around the photon candidate. Firstly, the photon candidate topocluster. This topocluster is accounted for in the nominal computation (Equation \ref{eq:topoetcone40_iso})in the first term ($E_{\textrm{T, raw}}^{\textrm{isol}}$) and corrected with the following two terms ($- E_{\textrm{T, core}}-E_{\textrm{T,leakage}}(\pt)$), as some energy of the photon candidate could leak outside the core ( see section~\ref{subsec:corecorr} for details on how the core correction is computed). The rest of the clusters can come either from pileup interactions or, in the case of fakes, from other particles in the jet. 

In this second part a cluster selection criteria  with the purpose of mitigating pileup is explained. In the two first  subsections(\ref{subsubsec:calo_iso_pu_samples} and ~\ref{subsubsec:calo_iso_pu_variables}) a short introduction on the samples and available cluster-level information is given followed by the definition of the pileup clusters and clusters from fakes enriched subsamples in subsection ~\ref{subsubsec:calo_iso_pu_charac}. A likelihood ratio is defined to clasify clusters labelling them as clusters from fakes  or as pileup clusters. Afterwards, details on how the pileup correction behaves using cluster level-information are given. Finally, new isolation variables are presented which present significant pileup mitigation with respect to the nominal isolation variable \textbf{topoetcone40}.

\subsubsection{Samples and enriched subsamples}
\label{subsubsec:calo_iso_pu_samples}

In order to produce the samples for this study, the SinglePhoton package \footnote{https://gitlab.cern.ch/ATLAS-EGamma/Software/PhotonID/HGamSinglePhotons} is modified to include cluster-level information. Data from runs 328263 to 336548 and MC16d are used. Two cluster subsamples are created:

\begin{itemize}
	\item Cluster around isolated photons (clusters around signal candidates): clusters in a $\Delta R=0.4$ cone around tight isolated photon candidates.
	\item Clusters around fakes (clusters around background candidates): clusters in a $\Delta R=0.4$ cone around loose'4 photon candidates. 
\end{itemize}

The first subsample is enriched in clusters coming from pileup while in the second subsample both pileup and clusters from fakes are present. 

\subsubsection{Topocluster variables}
\label{subsubsec:calo_iso_pu_variables}

In this subsection several cluster-level variables are defined and compared between the two subsamples defined above. 

\paragraph{Time}

Each topocluster is given a time \textit{t}, using the optimal filtering method, measured with respect to the time a neutrino would take to travel from the interaction point to the energy deposit in the detector. The difference between the photon time and each topocluster time is computed and shown in figure \ref{fig:time_dist}. Some features of this plot are: a Dirac delta at 0, a narrow peak at positive around 0 value, asymetric tails up to several ns, wide bumps at $\pm$ 25 ns and a few narrow peaks far from $\Delta t=0$.

Clusters with larger $|\Delta t|$ are more likely to originate from pileup objects, as they may come from a different interaction vertex in the same bunch crossing (in-time pileup) or from the previous/next bunch crossing (out-of-time pileup). The structures far from 0 are not yet well understood.

\begin{figure}[h]
 \centering
   \includegraphics[width=0.53\textwidth]{isolation_pileup/{timeiso_eta_0__06_status_unconv_mu_20_25}}
   \includegraphics[width=0.40\textwidth]{isolation_pileup/{meanr_eta_000__060_status_unconv_mu_20_25}}
 \caption{Left: $\Delta t$ distribution for clusters around tight isolated photon candidates and around fakes. There are not significant differences between both samples. Right: $\left<\Delta R\right>$ distribution for isolated photon candidates and fakes.  
\label{fig:time_dist}}
\end{figure}


\paragraph{$\left<\Delta R\right>$}

$\left<\Delta R\right>$ is the $p_{T}$ weighted mean distance of clusters around photon candidates:

\begin{equation} 
\left<\Delta R\right> = \frac{\sum^{n}_{i}\Delta R_{i}p_{T,i}}{\sum^{n}_{i}p_{T,i}}
\end{equation}

Figure \ref{fig:time_dist} shows that clusters around isolated photon candidates are further from the photon candidate while clusters around fakes are located closer to the photon candidate. Clusters from pileup are uniformingly distributed and hence more clusters are found far from it but clusters from fakes are more colimated at a smaller $\left<\Delta R\right>$. A contribution from clusters from pileup is appreciable in the clusters from fakes subsample. 

%\begin{figure}[h]
% \centering
%   \includegraphics[width=0.65\textwidth]{isolation_pileup/{meanr_eta_000__060_status_unconv_mu_20_25}}
% \caption{ $\Delta R$ distribution for photons: (MC) \textcolor{blue}{prompt} and \textcolor{red}{brem} truthmatched photons; and (data) \textcolor{green}{tight isolated} , \textcolor{yellow}{tight non isolated} and loose'4 photon candidates. This variable has some discriminating power between prompts and fakes \textcolor{red}{change colors plot!}.\label{fig:deltar_dist} }
%\end{figure}

%This is expected, as the probability for a cluster to be inside a cone at a certain radius R, increases with R. On the other hand, clusters from fakes are more energetic than pileup clusters 

\paragraph{Longitudinal deposits}

As mentioned before ( section ~\ref{subsec:caloisolcomputation} ), topoclusters are 3-dimensional cell clusters and hence they, in general, spread through several layers of both LAr and Tile calorimeters. In order to characterize and clasify clusters, the ratio between the energy deposited in the LAr calorimeter and the total energy of the cluster is computed ( Figure \ref{fig:emhadratio} ). Using this ratio, 4 categories are defined:

\begin{itemize}
	\item pure EM clusters: category which englobes all clusters with deposits only in the LAr calorimeter. About $80\%$ of the clusters fall in this category. 
	\item pure hadronic clusters: category which englobes all clusters with deposits only in the Tile calorimeter. 
	\item mainlyEM/mainly hadronic clusters: categories with clusters that deposit at least $50\%$ of the energy in the LAr/Tile calorimeter. 
\end{itemize} 


\begin{figure}[h]
 \centering
   \includegraphics[width=0.6\textwidth]{isolation_pileup/{emhadrat.pdf}}
 \caption{Ratio between the energy deposited in the LAr calorimeter and the total energy deposited in all calorimeteres by each topocluster. The region in pink is composed by mainly hadronic clusters while the region in light green is composed by mainly EM clusters. The $\delta =1$ is composed by pure EM clusters while the $\delta =0$ is composed by pure hadronic clusters. 
 \label{fig:emhadratio} }
\end{figure}


\subsubsection{Pileup clusters and clusters from fakes subsamples characterization}
\label{subsubsec:calo_iso_pu_charac}

Three subsamples are defined with the purpose of characterizing pileup clusters and clusters from fakes, where there is one pileup enriched subsample and two clusters from fakes subsamples.  

\begin{itemize}
	\item Pileup enriched cluster subsample:
		\begin{itemize}
			\item Tight isolated photon candidates to minimize fakes. 
			\item 0.3 < $\left<\Delta R\right>$ < 0.4 to minimize the impact of leakge or clusters from jets. 
			\item High pileup environments ($\left<\mu\right>$ > 40 )
		\end{itemize}
	\item Clusters from fakes subsamples:
		\begin{itemize}
			\item Loose'4 photon candidates or tight non isolated photon candidates to enrich the sample with clusters coming from fakes.
			\item 0.1 < $\left<\Delta R\right>$ < 0.3 to minimize the impact of pileup. ( Figure \ref{fig:time_dist} )
			\item Low pileup environments ($\left<\mu\right>$ < 10 )
		\end{itemize}
\end{itemize}

Although in all subsamples clusters from pileup are present, it is significantly reduced in the clusters from fakes subsamples. 

For each longitudinal deposit category and subsample, 2D-plots showing $\Delta t$ vs $E_{T}$  are made ( Figures \ref{fig:iso_jets_pile_cat} ). Also, a distintion between barrel and endcap is made ( finner $\eta$ bins are made but no significant differences are found ).  


\begin{figure}[h]
 \centering
  \subfloat[\emph{Pure EM clusters in the barrel}]{ \includegraphics[width=0.45\textwidth]{isolation_pileup/plots_jets_pile/{empttimepileupbarrel.png}}
   \includegraphics[width=0.45\textwidth]{isolation_pileup/plots_jets_pile/{empttimejetbarrel.png}}}\\
 \subfloat[\emph{Mainly EM  clusters in the barrel}] { \includegraphics[width=0.45\textwidth]{isolation_pileup/plots_jets_pile/{mempttimepileupbarrel.png}}
   \includegraphics[width=0.45\textwidth]{isolation_pileup/plots_jets_pile/{mempttimejetbarrel.png}}}\\
 \caption{Plots showing pileup (left) and clusters from fakes enriched (right) subsamples for pure (upper row)  and mainly (lower row) EM  categories in the barrel. Pure EM clusters category is mainly pileup in both fakes enriched and pileup enriched samples. Mainly EM clusters category show significantly higher $E_{T}$ in the clusters from fakes subsample.  
 \label{fig:iso_jets_pile_cat} }
\end{figure}


Clusters from the pileup enriched subsample are softer and are often close in time with the photon candidate. Nevertheless, some clusters are more widely spread in time around the photon candidate, mainly out-of-time pileup clusters. Clusters from fakes are harder and, besides pileup contamination, slightly spreaded in time. 

In order to characterize both types of clusters (from a fake or not from a fake) a simultaneous fit is performed to the three subsamples ( one sample is pileup enriched and two samples that are enriched in clusters from fakes ). The simultaneous fit considers 4 different cluster categories (pure EM, pure hadronic , mainly EM/hadronic), regarding their longitudinal deposits in the calorimeter; and 3 different species:

\begin{itemize}
	\item Red species: soft and in time pileup.
	\item Blue species: harder and wider pileup.
	\item Pink species: harder clusters and slightly shifted in time, mainly clusters from fakes. 
\end{itemize}

The functions used to fit these 2D distributions are bifurcated bigaussians, one for each species. The simultaneous fit uses the same shapes to fit the three subsamples but with different yields for each of them. 


\begin{figure}[h]
 \centering
   \includegraphics[width=0.60\textwidth]{isolation_pileup/{mixed.pdf}}
 \caption{Plot showing the different fitted regions. Clusters from fakes are weightened in order to be distinguished in the same plot from pileup clusters. 
 \label{fig:reg_plot} }
\end{figure}

The outputs of all these fits are Probability Density Functions for each category, part of the detector and region ( Figure \ref{fig:pdf_and_fits} ).  First, a pileup PDF is defined as:
\begin{equation}
	Prob_{pileup} = fProb_{soft} + (1-f)Prob_{hard} ; \hspace{3mm} f = \frac{N_{soft}}{N_{soft}+N_{hard}}
\end{equation}

\begin{figure}[h]
 \centering
   \includegraphics[width=0.60\textwidth]{isolation_pileup/{doublefit}}
   \includegraphics[width=0.30\textwidth]{isolation_pileup/{contfits}}
 \caption{Left: one-dimensional projections of the fits performed to the distrobutions of mainly hadronic clusters in the endcaps are shown in the first two plots. Solid pink line shows the total distribution, dashed blue is the clusters from fakes distribution and dashed red are both soft and hard pileup distributions. Right: plot showing soft pileup, hard pileup and clusters from fakes 2D PDFs.   
 \label{fig:pdf_and_fits} }
\end{figure}



Using $Prob_{pileup}$ and $Prob_{fake}$ a weight showing the likelihood for a cluster to come from a fake is built  as:

\begin{equation}
	weight = \frac{ Prob_{fake}}{Prob_{fake}+Prob_{pileup}}
\end{equation}

This weight is larger if the cluster is likely coming from a fake and smaller otherwise. This likelihood ratio has, by construction, some discrimination power between pileup clusters and clusters from fakes(Figure \ref{fig:clus_weights}). Even if the fit is not perfect, this just implies that the weight is suboptimal but it is not expected to give an improper performance.  

\begin{figure}[h]
 \centering
   \includegraphics[width=0.45\textwidth]{isolation_pileup/{weights}}
   \includegraphics[width=0.45\textwidth]{isolation_pileup/{likely.pdf}}
 \caption{Left: distribution of weights for an arbitrary cluster sample. In this plot, the weight for all positive energy clusters in the detector are shown. Right: distribution of weights for mainly EM clusters in the barrel. It is coherent with the PDFs shown in Figure \ref{fig:pdf_and_fits}
 \label{fig:clus_weights} }
\end{figure}

\subsubsection{Pileup correction computation}
\label{subsubsec:calo_iso_pu_correc}
\textcolor{red}{TO BE MODIFIED FROM HERE!}
The aim of this subsection is to show how using the weights obtained in the previous section pileup can be efficiently mitigated. In order to show that, several cluster selection criteria are studied:

\begin{itemize}
	\item Nominal cluster selection: only positive energy clusters in the acceptance of the calorimeters.
	\item Weighted cluster selection: only positive energy clusters in the acceptance of the calorimeters but pt-weighted ( $p_{T}$*weight)
	\item Type01 cluster selection: only positive energy clusters in the acceptance of the calorimeters with weight>0.01
	\item Type1 cluster selection: only positive energy clusters in the acceptance of the calorimeters with weight>0.1
\end{itemize}

Using these inputs to the FastJet package algorithm, the obtained ambient energy density is shown in (\textcolor{red}{figures tal}) as well as their trends with pielup. The slope of the nominal cluster selection is reduced by a factor of 4 with respect to the  weighted cluster selection. 

There is a possible issue regarding 0 ambient energy density for a large number of events in the endcaps which needs to be investigated further.

\subsubsection{Isolation working points with pileup mitigation techniques} 
\label{subsubsec:calo_iso_pu_prime}

With the cluster selection working points and the ambient energy density computed in the previous sections, new isolation working points are studied in order to test the changes in performances and its robustness against pileup. 

TO BE CONTINUED


%If the photon candidate is a prompt photon, we do not expect it to create more than a few topoclusters, depending on whether it is an unconverted or a converted photon. On the other hand, fakes (aka mainly energetic neutral pions inside a jet) create more topoclusters around them due to other particles in the jet. 










% ---
% Definition (or re-definition) of the new pileup-robust WPs
% ---

The implementation of isolation criteria is specific to the physics analysis
needs, be it to identify prompt electrons, isolated or produced in a
busy environment, or to reject electrons from heavy flavor decays or light hadrons misidentified as electrons. The
different electron isolation working points are presented in the Table~\ref{tab:ele_wps}.

The working points can be defined in two different ways, targeting a fixed
value of efficiency or with fixed cuts on the isolation variables. The \texttt{Gradient}
working point was designed to have an efficiency of 90\% at $\pt = 25\ \gev$ and
$99\%$ at $\pt=60\ \gev$, uniform in $\eta$. The cut maps for this working point
were derived from $Z\to ee$ and $J/\Psi \to ee$ Monte Carlo samples produced
with a pile-up profile used for pre-Run II studies. Despite the fact that it was
not optimized for a high pile-up environment it still provides a very good
performance for 2017 and 2018 data.
The three other working points, \texttt{FCHighPtCaloOnly}, \texttt{FCLoose} and \texttt{FCTight}, have a
fixed requirement on the calorimetric and/or the track isolation variables.

\begin{table}[!ht]
  \centering
  \resizebox{\textwidth}{!}{
  \begin{tabular}{|l|c|c|}
    \hline
    \textbf{Working point} & \textbf{Calorimetric Isolation}  & \textbf{Track Isolation}    \\
    \hline \hline
    \texttt{Gradient}         & $\epsilon=0.1143\times \pt+92.14\%$ (with \etcs)       & $\epsilon=0.1143\times \pt+92.14\%$ (with \ptvcs) \\ \hline
    \texttt{FCHighPtCaloOnly} & $\etcs < \text{max}(0.015\times \pt, 3.5~ \text{GeV})$ & -                                                            \\
    \texttt{FCLoose}          & $\etcs/\pt < 0.20$                                     & $\ptvcsttva /\pt < 0.15$                \\
    \texttt{FCTight}          & $\etcs/\pt < 0.06$                                     & $\ptvcsttva /\pt < 0.06$                \\
    \hline
  \end{tabular}
  }

  \caption{Definition of the electron isolation working points and isolation efficiency $\epsilon$.
    In the Gradient working point definition, the unit of \pt\ is \gev; it was designed to have a combined
    efficiency of $\epsilon$(25 \gev) = 90\%, $\epsilon$(60 \gev) = 99\%, estimated with $Z, J/\Psi \to ee$
    MC samples, and Tight identification requirements. All working points use a cone size
    of $\Delta R = 0.2$ for calorimeter isolation and $R_{\mathrm{max}} = 0.2$ for track isolation.}
  \label{tab:ele_wps}

\end{table}


% FCHighPtCaloOnly
The \texttt{FCHighPtCaloOnly} working point was introduced to reduce the amount of fake leptons from multi-jet processes applying a cut
on the calorimetric isolation variable and without any requirement on the track variable.
This working point was redefined from the previous version \texttt{FixedCutHighPtCaloOnly}~\cite{PERF-2017-01}
as $\etcs < \max(0.015\,\pt, 3.5\ \gev)$ instead of $\etcs < 3.5\ \gev$ obtaining higher prompt efficiency
in the high \pt\ region, for a similar background rejection. The comparison between the efficiency for the two working points is shown
in Figure~\ref{fig:eleiso_cmp_FixedCutHighPtCaloOnly_FCHighPtCaloOnly}.

\begin{figure}[!h]
  \centering

  \includegraphics[width=0.50\textwidth]{figures/ele_iso/eff_vs_logpt_data17_MediumLH_FixedCutHighPtCaloOnly_FCHighPtCaloOnly.pdf}

  \caption{Comparison between the efficiencies of the Rel20.7 working point \texttt{FixedCutHighPtCaloOnly} (dark blue) and the optimized \texttt{FCHighPtCaloOnly} (red) as a function of the
    electron {\pt}. The efficiencies have been measured using data recorded by the ATLAS experiment in 2017 at a centre-of-mass energy of $\sqrt{s} = 13$ \tev.
    Both statistical and systematic uncertainties are included.}
  \label{fig:eleiso_cmp_FixedCutHighPtCaloOnly_FCHighPtCaloOnly}

\end{figure}


% FCLoose/FCTight
The \texttt{FCLoose} and \texttt{FCTight} working points with a fixed cut on both calorimetric and track isolation
variables were re-optimized from the previous versions \cite{PERF-2017-01} to be more stable
under pile-up. In the actual definition they use the \texttt{ptvarcone20}\_\texttt{TightTTVA}\_\texttt{pt1000} track isolation
variable (defined in Section~\ref{sec:ele_trackiso}) instead of \texttt{ptvarcone20}. In Figure
\ref{fig:eleiso_cmp_FixedCut_FC_WPs} the comparison between the previous and optimized working points
is shown as a function of the number of interactions per bunch crossing, $\mu$, using 2017 data.

\begin{figure}[!h]
  \centering

  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_vs_mu_data17_MediumLH_FixedCutLoose_FCLoose.pdf}
  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_vs_mu_data17_MediumLH_FixedCutTight_FCTight.pdf}

  \caption{Comparison between the efficiencies of the Rel20.7 working points and the optimized working points as a function of the
    actual number of interactions per bunch crossing : \texttt{FixedCutLoose} and \texttt{FCLoose} (left), \texttt{FixedCutTight} and \texttt{FCTight} (right).
    The efficiencies have been measured using data recorded by the ATLAS experiment in 2017 at a centre-of-mass energy of $\sqrt{s} = 13$ \tev.
    Both statistical and systematic uncertainties are included.}
  \label{fig:eleiso_cmp_FixedCut_FC_WPs}

\end{figure}

The track isolation, called \textbf{ptcone}, is computed by summing the transverse momentum of some selected tracks within a cone centred around the lepton track or photon direction. The selection of the tracks is detailed in the table~\ref{tab:trksel}. The first five lines correspond to the \textbf{Loose} definition of track selection defined by the tracking working group. The last two lines are dedicated cuts optimised in the context of track isolation to maximise the fake lepton background in \ttbar\ events (\pt\ cut), and minimize the pile-up dependence of the track isolation ($|z_{0}\sin\theta|$ cut). 

\begin{table}[htbp]
\centering
\begin{tabular}{|c|c|}
\hline
Criterium & Cut value \\
\hline
\rs{|$\eta$| }& \rs{$<2.5$ }\\
\rs{$N_\textrm{Si}$ }& \rs{$\geq 7$} \\
\rs{$N_\textrm{mod}^\textrm{sh}$ }& \rs{$\leq 1$ }\\
\rs{$N_\textrm{Si}^\textrm{hole}$} & \rs{$\leq 2$} \\
\rs{$N_\textrm{Pix}^\textrm{hole}$} & \rs{$\leq 1$} \\ 
\rs{\pt} & \rs{$> 1 \gev$} \\
\hline
\end{tabular}
\caption{Track selection for the track isolation. $N_\textrm{Si}$  is the number of silicon (pixel + SCT) hits (including dead sensors); $N_\textrm{mod}^\textrm{sh}$ is the number of shared models~\ie~$(N_\textrm{Pix}^\textrm{sh} + N_\textrm{SCT}^\textrm{sh}/2)$ where $N_\textrm{Pix}^\textrm{sh}$ and $N_\textrm{SCT}^\textrm{sh}$ are the number of shared hits, \ie\ assigned to several tracks, in the pixel and SCT detectors;  $N_\textrm{Si}^\textrm{hole}$ is the number of silicon holes, \ie\ missing hits in the pixel and SCT detectors; $N_\textrm{Pix}^\textrm{hole}$ is the number of pixel holes; $z_{0}$ is the difference of longitudinal impact parameter of the track, and the position of a given vertex (by default the hardest vertex of the event)}
\label{tab:trksel}
\end{table}

The selection of tracks has been optimised using candidate muons from \ttbar\ MC events (using an earlier
version of the reconstruction software than the one used for all other studies detailed in this note). A
fixed cone size of $R = 0.3$ was used. Fig.~\ref{fig:trkOpt} shows the evolution of the background muon
(typically from heavy flavour hadron decay) rejection as a function of the efficiency for the signal muons
for various track selections :
\begin{itemize}
\item \textbf{Loose} vs \textbf{TightPrimary};
\item $p_{\mathrm{T}} > 400, 600, 800, 1000, 1200 \mev$;
\item $|z_{0}\sin\theta| < 15, 30, 50\,mm$ or using a track-to-vertex association tool provided by the tracking working group.
\end{itemize}
The \textbf{Loose} criteria give the best sensitivity and variations on the $p_{\mathrm{T}}$ or $|z_{0}\sin\theta|$ thresholds
have a small impact.
\begin{figure}[htbp]
\centering
\includegraphics[width=6cm]{figures/trkSelOptim}
\caption{Evolution of the background muon rejection as a function of the efficiency for the signal muons
for various track selections.}
\label{fig:trkOpt}
\end{figure}




Oppositely to the calorimeter isolation\footnote{The lepton / photon cluster has a size of $3\times 7$ or $5\times 5$ in middle cell calorimeter unit of $0.025$, which corresponds roughly to a cone of radius $0.1$} where a cone with a radius smaller than 0.2 cannot be used, the tiny tracker "cell size" allows to use smaller cone size when needed. For example, in boosted signatures or very busy environnements other objects can end up very close to the lepton / photon direction. For such cases, a variable-cone size track isolation~\cite{Rehermann:2010vq}, called \textbf{ptvarcone}, can be used. For this variable, the cone size gets smaller for larger transverse momentum of the lepton / photon:

\begin{equation}
\Delta R = \textrm{min} \left(\frac{k_T}{\pt},R\right),
\end{equation}

where $k_T$ is a constant fixed to $10\gev$ and $R$ is the maximum cone size ($0.2$ to $0.4$). In run 1, $k_T=10\gev$ was optimised on \ttbar\ Monte Carlo events to maximise the background rejection coming from fake leptons. 

The sketch in Fig.~\ref{fig:schema_track_isolation} shows how the variable is calculated, which summarises this section.


\begin{figure}
\centering
\includegraphics[width = 8cm]{figures/schema_track_isolation}
\caption{Schema of the $p_{\text{T}}^{\text{varcone}}$ variable. All the good tracks (brown) located in a cone (blue) around the object, are selected. 
The dotted lines show tracks which are too far from the object, and are therefore not selected. The $p_{\text{T}}$ of these tracks are summed to calculate the isolation variable, and 
the core energy is subtracted (for muons, this corresponds to the transverse momentum of the object's track, in green). The size of the cone depends on the $p_{\text{T}}$ of the object.}
\label{fig:schema_track_isolation}
\end{figure}

\textbf{Vertex association}

The old isolation working points (those using the variables $pt[var]coneX0$) use as the track-vertex association $\Delta z_{0}sin(\theta)<3$.

A set of new track isolation variables were defined to mitigate the effects of pileup for 2017 and 2018 pileup conditions.
These working points are labeled as $pt[var]coneX0\_TightTTVA\_ptX$. 
For the vertex association, they require tracks to satisfy one of the following:

\begin{description}
  \item $trk\rightarrow vertex()==PV$
  \item $trk\rightarrow vertex()==NULL$ and $\Delta z_{0}sin(\theta)<3$
\end{description}

These and a couple other options are compared in fig.~\ref{fig:mcisottva}.

\begin{figure}
\centering
\subfloat[\emph{Prompt, 10-20 GeV}]{\includegraphics[width=0.40\textwidth]{isottva/ttbarmc_prompt_10.pdf}}
\subfloat[\emph{Prompt, 30-40 GeV}]{\includegraphics[width=0.40\textwidth]{isottva/ttbarmc_prompt_30.pdf}} \\
\subfloat[\emph{HF, 10-20 GeV}]{\includegraphics[width=0.40\textwidth]{isottva/ttbarmc_hf_10.pdf}}
\subfloat[\emph{HF, 30-40 GeV}]{\includegraphics[width=0.40\textwidth]{isottva/ttbarmc_hf_30.pdf}}
\caption{Isolation efficiency for muons, for $ptvarcone30<0.06\times p_{T}$}
\label{fig:mcisottva}
\end{figure}

Tight was chosen, since it removed most of the pileup dependence without significantly sacrificing heavy-flavor rejection.
This choice has the added benefit of improving the agreement of data and simulation, due to the mismodeling of pileup and the beamspot in simulation. 
Fig.~\ref{fig:mcdatattva} compares data and simulation.

\begin{figure}
\centering
\subfloat[\emph{Old TVA}]{\includegraphics[width=0.40\textwidth]{isottva/datamc_old_30.pdf}}
\subfloat[\emph{New TVA}]{\includegraphics[width=0.40\textwidth]{isottva/datamc_new_30.pdf}}
\caption{Isolation efficiency for muons, for $ptvarcone30<0.06\times p_{T}$}
\label{fig:mcdatattva}
\end{figure}



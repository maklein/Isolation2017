\subsubsection{Photons isolation efficiency measurements}

For \ET from 10 \gev\ up to 100 \gev, it is possible to measure the efficiencies with radiative Z decays. The low \ET region (from 10 to 20 \gev) is contaminated with background from Z+jets events which needs to be subtracted. A template fit to the three body invariant mass ($m_{\ell\ell\gamma}$) distribution is performed, where the templates are extracted from signal ($Z\to\ell\ell\gamma$) and background ($Z$+jets) simulated events. The Monte-Carlo samples include the following weights: pileup reweighting, generator weight, trigger weights, cross-section weight and filter efficiency, photon ID and lepton scale factors, normalization to data luminosity and to total number of weighted MC events. The purity obtained in the data sample allows to calculate the number of background events to be subtracted from the number of probes to get the estimate of the number of true photons. The isolation efficiency in the $10<\ET<20$ \gev\ region is corrected as follows: 
\begin{equation}
  \varepsilon_{iso} = \frac{N_{probes, iso} - N_{B,iso}}{N_{probes} - N_{B}} ,
\end{equation}
which is equivalent to:
\begin{equation}
  \varepsilon_{iso} = \frac{N_{S,iso}}{N_{S}} .
\end{equation} 

Figures~\ref{fig:photon_radZ_wploose},~\ref{fig:photon_radZ_wptight} and ~\ref{fig:photon_radZ_wpcaloonly} shows the photon efficiency and scale factors for the \texttt{FixedCutLoose}, \texttt{FixedCutTight} and \texttt{FixedCutTightCaloOnly} isolation WP for unconverted and converted photons as a function of \ET, for $|\eta|<0.6$. Background subtraction is provided via template fit method for low \ET region up to 20 \gev\ in case of data events. The uncertainties include both statistical and systematic ones.

\begin{figure}[htbp]
\centering
  \includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{eff_data17_mc_corr_unc_wploose_iddep_eta0_06}}
  \includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{eff_data17_mc_corr_conv_wploose_iddep_eta0_06}}
  \caption{Comparison of the photon isolation efficiency in data and simulation, as a function of \ET for $|\eta|<0.6$, for the \texttt{FixedCutLoose} isolation WP, measured using $Z\to\ell\ell\gamma$, for unconverted (left) and converted (right) photons. Only statistical uncertainties are shown.}
\label{fig:photon_radZ_wploose}
\end{figure}

\begin{figure}[htbp]
\centering
  \includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{eff_data17_mc_corr_unc_wptight_iddep_eta0_06}}
  \includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{eff_data17_mc_corr_conv_wptight_iddep_eta0_06}}
  \caption{Comparison of the photon isolation efficiency in data and simulation, as a function of \ET for $|\eta|<0.6$, for the \texttt{FixedCutTight} isolation WP, measured using $Z\to\ell\ell\gamma$, for unconverted (left) and converted (right) photons. Only statistical uncertainties are shown.}
\label{fig:photon_radZ_wptight}
\end{figure}

\begin{figure}[htbp]
\centering
  \includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{eff_data17_mc_corr_unc_wptightcaloonly_iddep_eta0_06}}
  \includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{eff_data17_mc_corr_conv_wptightcaloonly_iddep_eta0_06}}
  \caption{Comparison of the photon isolation efficiency in data and simulation, as a function of \ET for $|\eta|<0.6$, for the \texttt{FixedCutTightCaloOnly} isolation WP, measured using $Z\to\ell\ell\gamma$, for unconverted (left) and converted (right) photons. Only statistical uncertainties are shown.}
\label{fig:photon_radZ_wpcaloonly}
\end{figure}

\subsubsection{Pile-up dependence}

The pile-up dependence of the photon isolation efficiency is evaluated, as a function of $\mu$. According to pile-up recommendations for Run2, MC samples should be reweighted to match the actual $\mu$ distribution in data. However, there might be residual differences between data and MC efficiencies due to incorrect simulation of the detector response to pile-up. \\
In Figures~\ref{fig:eff_pileup_radZfct} and~\ref{fig:eff_pileup_radZoth}, the pile-up dependence of the efficiency obtained in data with radiative $Z\to \ell\ell\gamma$ decays is compared to the same obtained in simulated $Z\to \ell\ell$ MC samples. Only photons with $20 < \ET < 40$ \gev\ are used to avoid background contamination in the data sample.  The pile-up dependence is presented for unconverted and converted photons for \texttt{FixedCutTight}, and only for unconverted photons for other WPs, in the $|\eta|<0.6$ region. The efficiencies show similar dependence for both data and MC for \texttt{FixedCutLoose} WP, some major differences are observed for unconverted photons in the barrel region of the detector for \texttt{FixedCutTight} WP, and in case of \texttt{FixedCutTightCaloOnly} in the most of the regions of $\mu$.

\begin{figure}[!htbp]
  \centering
  {\includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{eff_data17_mc_mu_unc_wptight_eta0}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{eff_data17_mc_mu_conv_wptight_eta0}}}
  \caption{Photon isolation efficiencies for \texttt{FixedCutTight} isolation WP versus $\mu$ for reconstructed unconverted photons (left) and converted photons (right), for $|\eta|<0.6$. Solid black dots represent data, hollow red dots - simulation. Only statistical uncertainties are shown.}
  \label{fig:eff_pileup_radZfct}
\end{figure}

\begin{figure}[!htbp]
  \centering
  {\includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{eff_data17_mc_mu_unc_wploose_eta0}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{eff_data17_mc_mu_unc_wpcaloonly_eta0}}}
  \caption{Photon isolation efficiencies for \texttt{FixedCutLoose} (left) and for \texttt{FixedCutTightCaloOnly} (right) isolation WPs versus $\mu$ for reconstructed unconverted photons, for $|\eta|<0.6$. Solid black dots represent data, hollow red dots - simulation. Only statistical uncertainties are shown.}
  \label{fig:eff_pileup_radZoth}
\end{figure}

\subsubsection{Systematic Uncertainties}
\label{sec:syst_radZ}

Several sources of systematic uncertainties are contributing to the total systematic uncertainty on the measurement of photon isolation efficiency. The dominant ones are coming from the template fit method. 

\textbf{Closure test}\\ 
The photon isolation efficiency estimated from photon candidates matched to true photons are compared to the results of the template fit method. Simulated background was added to the signal from truth photons coming from radiative Z decays.
%Results coming from template fit method also present sum of background and signal events.
The closure test was provided only for $\ET$ region [10, 20] \gev\ due to lack of statistics at higher \ET. The maximum difference between the two estimations is less than 0.1\% for all isolation working points.

\textbf{Background uncertainty}\\
The template fit is performed with different fit ranges - instead of the nominal fit range [65, 105] \gev, an extended range [45, 120] \gev\ is chosen; the signal region is also changed and varied to [45, 95] \gev\ and to [80, 120] \gev\ to cover mismodeling effects on the low mass range. The maximum value is 0.4\% for converted and and 0.2\% for unconverted photons.

\textbf{MC-related uncertainty}\\
An alternative MC event generator sample was chosen, where the uncertainty comes from the deviation in the MC predictions of the background. Subtraction of background events (for \ET in [10, 20] \gev) with the template fit method was performed using templates extracted from the alternative generator \textsc{Powheg}+\textsc{Pythia} instead of the nominal one (\textsc{Sherpa}). The final uncertainty is evaluated from the difference between the nominal scale factors and then scale factors where the numerator is the data efficiency which was corrected with the varied templates and the denominator is the efficiency from the nominal MC sample. The maximum value is 0.4\% for both unconverted and converted photons.

\textbf{Alternative background estimation method}\\
An alternative background estimation method based on two different photon identification requirements was developed. It gives results consistent with the template fit method and is mainly used as a cross-check.\\
The method is defined as a 2D-sideband method and it is based on the identification (ID) of the photon candidate and the two-body invariant mass $m_{\ell\ell}$ requirement. One region is defined as signal region (A) with $Z \to \ell\ell\gamma$ events and the other three regions are enriched with $Z$+jet events. \\
The tight ID requirement is used in signal region A since it provides good background rejection, while retaining a photon efficiency above 95\% for photons with transverse momenta above 20 \gev ~\cite{Aaboud:2643391}. Regions C and D use looser or "not-tight" photon identification requirements (called "Loose'4"), obtained from the nominal tight ID criteria by reverting the requirements on at least one of the four shower shape variables $\Delta E$, $w_{s3}$, $E_{ratio}$ and $F_{side}$. \\
The two-body invariant mass requirement is good to be used on both selection of $Z$ boson and background sample: by reversing the $m_{\ell\ell}$ from 40~\gev < $m_{\ell\ell}$ < 83~\gev\ to $m_{\ell\ell}$ > 85~\gev\ almost all photon candidates should be jets since the minimal photon energy is \ET > 10~\gev. 

The four method regions are defined as follows:
\begin{itemize}
  \item Tight and direct mass cut region (A): the photon candidates are required to pass the tight ID criteria and $40<m_{\ell\ell} <83$~GeV;
  \item Tight, reversed mass cut region (B): the photon candidates are required to have two-body invariant mass $m_{\ell\ell}$ > 85~GeV but pass tight ID criteria;
  \item Not-tight, direct mass cut region (C): the photon candidates have mass requirement $40<m_{\ell\ell} <83$~GeV, fail the tight ID critera but pass "loose prime4" ID criteria;
  \item  Not-tight, reversed mass cut region (D): the photon candidates are required to have two-body invariant mass $m_{\ell\ell}$ > 85~GeV, fail the tight ID critera but pass "loose prime4" ID criteria.
\end{itemize}

The 2 GeV-wide two-body invariant mass gap between the signal and background regions helps reducing the signal leakage into the background region.

The difference between the alternative background subtraction method and the template fit method is measured; the maximum difference  is 2\% for unconverted photons and 4\% for converted photons.

\textbf{Results}\\
Total systematic uncertainties include uncertainties from the non-closure in simulation, the template fit method, the difference in data efficiencies coming from the template fit method and the data-driven method and the alternative MC event generator. Individual and total uncertainties are shown in Figure~\ref{fig:tot_unc} for both unconverted and converted photons, for $|\eta|<0.6$.

\begin{figure}[!htbp]
  \centering
  {\includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{uncert_data17_unc_wploose_eta0_06}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{uncert_data17_conv_wploose_eta0_06}}}
  \caption{Systematic uncertainties for the \texttt{FixedCutLoose} isolation WP, measured using $Z\to\ell\ell\gamma$ from data and MC simulated samples,
  for $|\eta| < 0.6$ and unconverted (left) and converted (right) photons.}
  \label{fig:tot_unc}
\end{figure}


\subsubsection{Comparison of 2017 and 2018 data}

Cross-checks of photon isolation were performed using 2017 and 2018 Data-taking periods. Track based isolation \ptcs/\pt and calorimeter based isolation $\etcb - 0.022 \times \pt$ distributions were checked with 2017 and 2018 data and were compared to the signal Montle-Carlo in case of unconverted and converted photons (fig.~\ref{fig:ph_radZ_dist_17_18}). The ratio of two Data-taking periods to Monte-Carlo is close to each other and agrees within the uncertainties.

\begin{figure}[htbp]
\centering
  \includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{ptcone20_pt_Zll_data17-18_mc_unc_iddep}}
  \includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{ptcone20_pt_Zll_data17-18_mc_conv_iddep}}
  \includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{topoetcone40_pt_Zll_data17-18_mc_unc_nolog_iddep}}
  \includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{topoetcone40_pt_Zll_data17-18_mc_conv_nolog_iddep}}
  \caption{$\ptcs/\pT$ distribution (top) and $\etcb - 0.022 \times \pt$ distribution (bottom) for 2017 data (dots), for 2018 data (hollow dots) and for signal MC Sherpa (red line) for unconverted (left) and converted (right) photons.}
\label{fig:ph_radZ_dist_17_18}
\end{figure}

The efficiencies of the \texttt{FixedCutTight} isolation WP were compared for each of the Data-taking period to Montle-Carlo, and their scale factors were obtained with dependence on the pile-up, as a function of $\mu$.  Fig.~\ref{fig:ph_radZ_dist_17_18} shows comparison of two sets of scale-factors, obtained with 2017 data and 2018 data, for unconverted and converted photons, for $|\eta|<0.6$. Only photons with $20 < \ET < 40$ \gev\ are used to avoid background contamination in the data sample. \\
Two sets of scale factors show similar dependence on the pile-up, with the scale factor values agreeing within the uncertainties. Therefore it is possible to use the set of scale factors obtained for 2017 data for 2018 data.

\begin{figure}[htbp]
\centering
  \includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{SF_wptight_data17_18_unc_eta0}}
  \includegraphics[width=0.48\textwidth,angle=0]{ph_iso-radZ/{SF_wptight_data17_18_conv_eta0}}
  \caption{Scale factors for the \texttt{FixedCutTight} isolation WP for 2017 data (dots) and 2018 data (hollow dots)}
\label{fig:ph_radZ_dist_17_18}
\end{figure}
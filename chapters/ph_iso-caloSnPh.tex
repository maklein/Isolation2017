Photon calorimetric isolation (calo-only)  efficiency is obtained by fitting the shape of the calorimetric isolation variable, \etcb or \etcs, minus the relevant \ET fraction ($0.022 \times \ET$ for \texttt{FixedCutTight(CaloOnly)} and $0.065 \times \pt$ for \texttt{FixedCutLoose}). By definition, events which fall to the left of the cut value ($2.45$ GeV for \texttt{FixedCutTight} and \texttt{FixedCutTightCaloOnly} and $0.0$ GeV for \texttt{FixedCutLoose}) pass the calorimetric-isolation selection. For each measurement, the cut on the track isolation variable (see table~\ref{tab:phWPs}) is applied at pre-selection level. The primary complication of the efficiency calculation is in determining the shape of only true photons in Data, since the single photon Data sample is contaminated by a significant number of fake photons passing identification requirements. Subtracting the fake photon shape involves a process of iterative fits, which is described below in detail.  

Scale factors are determined for fifteen bins in transverse momentum, corresponding to the single photon trigger (see section~\ref{sec:ph_calotrackiso_sgPhotons})  thresholds: (25,30), (30,35), (35,40), (40,45), (45,50), (50,55), (55,65), (65,75), (75,85), (85,105), (105,125), (125,150), (150,175), (175,250), and (250,1500) GeV. The scale factors are binned in six bins in the absolute-value of pseudo-rapidity: (0,0.60), (0.60,0.82), (0.82,1.15), (1.15,1.37), (1.52,1.81) and (1.81,2.37). This choice of $|\eta|$ binning is the same as that used for the photon identification efficiency measurements, and the bins correspond to different detector regions. The scale factors are provided separately for converted or unconverted photons, as well as separately for the 2015-2016 and 2017 Data-taking periods. 

Pythia8 Monte Carlo $\gamma +jet$ samples are used for the true photon template, with true photons defined as prompt photons from the $q\bar{q}\rightarrow \gamma g$ and $qg\rightarrow q \gamma$ processes. The background template is obtained using the Loose' 4 ID-region, defined as photon candidates failing identification requirements for one of the following shower shape variables: $F_{side}$, $w_{s3}$, $\Delta E$ or $E_{ratio}$. Although background-enriched, the Loose'4 ID region also contains some amount of true photons failing the Tight ID requirement, defined as ``leakage'' photons.

Unbinned fits are performed on both the Data and Monte Carlo samples. The samples include several weights: pileup re-weighting on Monte Carlo, trigger weights on Data, generator weight, photon ID scale factors, cross-section weight, generator efficiency and k-factors, normalization to data luminosity and to total number of weighted MC events. Because the \ET bins chosen for the measurement correspond to the trigger thresholds (when pre-scaled triggers), no trigger prescale weights are used. The sequence of fits proceeds as follows: 

\begin{figure}
\centering
\subfloat[\emph{Loose'4 photons in Monte Carlo}]{\includegraphics[width=0.45\textwidth]{ph_iso-caloSnPh/FitExamplePlots/{loosePythiaCB0}}}
\subfloat[\emph{Tight photons in Monte Carlo}]{\includegraphics[width=0.45\textwidth]{ph_iso-caloSnPh/FitExamplePlots/{tightPythiaACB0}}}
\caption{Measurement using single photons, calo-only: The (a) Crystal Ball fit to Loose'4 photons and (b) Asymmetric Crystal Ball fit to Tight photons from Pythia 8 MC simulations.}
\label{fig:photoncalosf_fits_pythia}
\end{figure}

\begin{itemize}

\item An initial fit to Tight photons (photons passing the ``Tight'' ID requirement) in Monte Carlo Simulation is performed using an Asymmetric Crystal Ball function. An example of this fit can be seen in Figure \ref{fig:photoncalosf_fits_pythia}b). 

\item A fit to photons from Monte Carlo Simulation passing the ``Loose'4'' ID requirement is performed using a Crystal Ball function. An example of this fit can be seen in Figure \ref{fig:photoncalosf_fits_pythia}a). 

\item A naive two-component fit is performed to photons passing the ``Loose'4'' ID requirement in data. An unconstrained Crystal Ball function is used to fit the fake photon shape, and the Crystal Ball fit to Loose'4 photons in Monte Carlo is used for the shape of the leakage photons. The Crystal Ball function (as opposed to Double Sided Crystal Ball) is used in order to simplify the fit and decrease the likelihood of the fit failing to converge. In addition, a wide Crystal Ball shape has generally been observed to decently model the shape of fake photons. An example of this two-component fit can be seen in Figure \ref{fig:photoncalosf_fits_initialData}a), where the leakage component is shown in blue, the fake component is shown in green, and the total fit is shown in red.

\item A naive two-component fit is performed on the Tight Data photon sample in order to estimate $N_{True, Tight-Data}$. The fake-photon (``background'') component of the sample is fitted with the background shape from the fit to Loose'4 photons in Data (the red curve in Figure~\ref{fig:photoncalosf_fits_initialData}a), and the true photon (``signal'') component is fitted with the shape obtained from the fit to Tight Monte Carlo photons (the curve in Figure~\ref{fig:photoncalosf_fits_pythia}b). An example of this fit is shown in Figure~\ref{fig:photoncalosf_fits_initialData}b). 

\end{itemize}

\begin{figure}
\centering
\subfloat[\emph{Loose'4 photons in Data (Initial)}]{\includegraphics[width=0.45\textwidth]{ph_iso-caloSnPh/FitExamplePlots/{looseDataBkgCB1SigCB1}}}
\subfloat[\emph{Tight photons in Data (Initial)}]{\includegraphics[width=0.45\textwidth]{ph_iso-caloSnPh/FitExamplePlots/{tightDataBkgMIXSigACB}}}
\caption{Measurement using single photons, calo-only: The initial two-component fit to (a) Loose'4 photons in Data and (b) Tight photons in Data. In the fit to Loose'4 photons in Data (a), the blue line shows the fit to the leakage (true photons failing Tight ID) component, and the green line shows the fit to the the fake photon component. The red line shows the total fit. In the fit to Tight Data photons (b), the blue line shows the total fit to signal (real photons) and background (fake photons), while the red line shows the fitted background shape. The background shape is taken from the red curve in (a).}
\label{fig:photoncalosf_fits_initialData}
\end{figure}

The relative fractions of real and fake photons in the naive two-component fits to Tight and Loose'4 photons in data are not derived from Monte Carlo. They are determined based on the best fit of the real (signal or leakage) and fake photon shapes to the data in each region. However, a more precise prediction for the number of leakage photons expected in the Loose'4 Data can be obtained from the ratio of Tight to Loose'4 photons in Monte Carlo. This prediction is then used to better constrain the magnitude of the leakage photon shape in Loose'4 Data. If the number of true photons in the Tight-ID Data sample is known, then the prediction for the number of leakage photons in the Loose'4 Data is simply given by: $$N_{Leak, Loose'4-Data} = N_{True, Tight-Data} \times \frac{N_{Loose'4-MC}}{N_{Tight-MC}}$$ where $N_{True, Tight-Data}$ is the number of true photons in the Tight Data sample, $N_{Loose'4-MC}$ is the number of photons in the Loose'4 Monte Carlo sample and $N_{Tight-MC}$ is the number of photons in the Tight Monte Carlo sample. 

The total number of events in the Tight Data photon sample minus the integral of the background component (the red curve in Figure \ref{fig:photoncalosf_fits_initialData}b), gives an estimate of the number of true photons in the Tight Data. Because the total background shape includes real photons from leakage, though, this prediction will underestimate $N_{True, Tight-Data}$.

A second estimate of $N_{True, Tight-Data}$ can be obtained by assuming that the leakage shape fitted to photons in the Loose'4 Data fit (the blue component in Figure \ref{fig:photoncalosf_fits_initialData}a) contains entirely true photons. $N_{True, Tight-Data}$ is then estimated by subtracting only the integral of the fake part of the background shape (the green component in Figure \ref{fig:photoncalosf_fits_initialData}a) from the total number of events. Due to the consistent overestimation of the number of leakage events by the naive Loose'4 Data photons fit, this second prediction will overestimate the value of $N_{True, Tight-Data}$.

Since the two estimates of $N_{True, Tight-Data}$ are expected to deviate in opposite directions, an average of the two is taken to give the final prediction of $N_{True, Tight-Data}$. The differences between the final $N_{True, Tight-Data}$ value and the two predictions is taken as the uncertainty. Therefore the final error is: $\sqrt{\mathrm{Average(over,under) + Difference(over,under)^2 }}$. The number of leakage events $N_{Leak, Loose-Data}$ can then be determined using the above formula. The fitting procedure with the updated leakage prediction continues as follows:

\begin{itemize}

\item The two-component fit to photons in Loose'4 Data is performed again, with the number of leakage events constrained to $N_{Leak, Loose'4-Data}$ with a Poisson constraint. This second fit tends to show a decrease in the fraction of fitted leakage events, as shown in the blue component of the example fit in Figure \ref{fig:photoncalosf_fits_finalData}a. The red component shows the fit to fake photon component of the Loose'4 Data, while the green component shows the total fit. 

\item The final two-component fit is performed on Tight Data photons. The signal shape is again taken from the fit to Tight Monte Carlo photons (from Figure~\ref{fig:photoncalosf_fits_pythia}b). The background shape is taken from the fake photon component of the second fit to Loose'4 Data (the red component in Figure~\ref{fig:photoncalosf_fits_finalData}a). An example fit can be seen in Figure~\ref{fig:photoncalosf_fits_finalData}b, where the red component shows the predicted background and the blue shows the total fit to signal and background.

\item The background (red component) of the two-component fit is subtracted out of the Tight Data photon shape. The remainder is the best estimate for the shape of true photons in the Tight Data. 

\end{itemize}

The calo-only isolation efficiency in Data can finally be obtained by integrating the background-subtracted Tight Data photon shape up to the working point cutoff of $2.45$ GeV (\texttt{FixedCutTight} and \texttt{FixedCutTightCaloOnly}) or $0.0$ GeV (\texttt{FixedCutLoose}). 

\begin{figure}
\centering
\subfloat[\emph{Loose'4 photons in Data (Final)}]{\includegraphics[width=0.45\textwidth]{ph_iso-caloSnPh/FitExamplePlots/{looseDataBkgCB2SigCB2}}}
\subfloat[\emph{Tight photons in Data (Final)}]{\includegraphics[width=0.45\textwidth]{ph_iso-caloSnPh/FitExamplePlots/{tightDataBkgCB2SigACB2}}}
\caption{Measurement using single photons, calo-only: The final two-component fit to (a) Loose'4 photons in Data and (b) Tight photons in Data. In the fit to Loose'4 Data photons (a), the blue line shows the fit to the leakage (true photons failing Tight ID) component, and the red line shows the fit to the the fake photon component. The green line shows the total fit. In the fit to Tight Data photons (b), the blue line shows the total fit to signal (real photons) and background (fake photons), and the red line shows the fitted background shape, which is taken from the red curve in (a).}
\label{fig:photoncalosf_fits_finalData}
\end{figure}

Three sources of systematic uncertainty are considered: estimation of the number of leakage photons in Loose'4 Data, differences between the Loose'-ID regions (Loose'3 and Loose'5), and discrepancies between the fit and Data photon shapes. A binomial statistical error on the scale factors is also calculated and added in quadrature with the systematic components. 

To determine the uncertainty from the leakage estimate in Loose'4 Data photons, the fits are performed with both the overestimate and underestimate of the number of leakage events, which are discussed above. The maximum deviation between the $N_{overestimate}$ and $N_{underestimate}$ scale factor values in each $\ET/\eta/$conversion bin from the nominal result is taken as the error. 

To determine the uncertainty from the choice of Loose'-ID region, the fitting process is performed using photons in the Loose'3 and Loose'5 regions of Data and Monte Carlo. The Loose'3 (Loose'5) region is defined as photon candidates failing identification requirements for one of the following shower shape variables: $F_{side}$, $w_{s3}$, or $\Delta E$ ( $F_{side}$, $w_{s3}$, $\Delta E$, $E_{ratio}$, or $w_{stot}$).
%The Loose'2 region was not included, as many bins in this region suffer from poor statistics which prevent the fits from converging reasonably. 
The maximum discrepancy between the Loose'3 and Loose'5 results from the nominal Loose'4 result is taken as the error value. This error is also calculated uniquely for each $\ET/\eta/$conversion bin. 

The contribution from the fit quality is included as a scaling on the statistical error, and it is obtained from the $\sqrt{\chi^2/(N_{bins}-1)}$ value of the fits. The $\chi^2$ is calculated from two histograms with 100 bins, one created from the fit to Tight Monte Carlo photons and the other containing the actual Tight Monte Carlo photons. Only the bins which contain a significant number of entries are included in the calculation. The $\chi^2/(N_{bins}-1)$ values are calculated for each $\ET/\eta/$conversion bin. 
%The binomial statistical component of the error is then scaled by this $\sqrt{\chi^2/(N_{bins}-1)}$ factor.

To obtain the total uncertainty, the two systematic (Loose'-ID choice and leakage estimate) components are added in quadrature with the scaled statistical component. A breakdown of the systematic uncertainty components can be found in Figures \ref{fig:SystErrorsFixedCutLoose} for \texttt{FixedCutLoose} working point.

Examples of the final calo-only efficiencies and scale factors are provided for unconverted photons in the most central $|\eta|$ bin ($|\eta|<0.6$) for all three working points in Figure \ref{fig:unconv_sf_all_wps_SgPhCalo}. For comparison, efficiencies and scale factors for the \texttt{FixedCutTight} working point are shown for converted photons in the most central $|\eta|$ bin and unconverted photons in the endcap $|\eta|$ bin ($1.81<|\eta|<2.37$) in Figure~\ref{fig:sf_comparison_wps_SgPhCalo}. 


\begin{figure}
\centering
\subfloat[\emph{2017 Data with MC16d, Unconverted}]{\includegraphics[width=0.85\textwidth]{ph_iso-caloSnPh/SystErrors/{allSystErrors_FixedCutLoose_topoETcone20pt_unconv_2017}}}\\
\subfloat[\emph{2017 Data with MC16d, Converted}]{\includegraphics[width=0.85\textwidth]{ph_iso-caloSnPh/SystErrors/{allSystErrors_FixedCutLoose_topoETcone20pt_conv_2017}}}
\caption{Measurement using single photons, calo-only: The breakdown of the systematic errors for the \texttt{FixedCutLoose} scale factors for (a) unconverted photons in 2017 Data and MC16d, and (b) converted photons in 2017 Data and MC16d simulations. The error values versus photon $E_{T}$ are plotted separately for each bin in $|\eta|$. The leakage error is shown in magenta, and the $Loose'$ error is shown in blue. The raw statistical error is shown in red, and the the scaled statistical error (to account for the fit error) is shown in green. The black line shows the total error.}
\label{fig:SystErrorsFixedCutLoose}
\end{figure}




\begin{figure}
\centering
\subfloat[\texttt{FixedCutTight}]{\includegraphics[width=0.31\textwidth]{ph_iso-caloSnPh/ScaleFactorPlots/{SFFixedCutTight_2017_unconv}}}
\subfloat[\texttt{FixedCutTightCaloOnly}]{\includegraphics[width=0.31\textwidth]{ph_iso-caloSnPh/ScaleFactorPlots/{SFFixedCutTightCaloOnly_2017_unconv}}}
\subfloat[\texttt{FixedCutLoose}]{\includegraphics[width=0.31\textwidth]{ph_iso-caloSnPh/ScaleFactorPlots/{SFFixedCutLoose_2017_unconv}}}
\caption{Measurement using single photons, calo-only: The efficiencies as a function of \ET\ for unconverted photons in the rapidity range $|\eta|<0.6$ for the (a) \texttt{FixedCutTight}, (b) \texttt{FixedCutTightCaloOnly}, and (c) \texttt{FixedCutLoose} working points. The Data efficiency is shown in black, and the Monte Carlo efficiency is shown in red. The bottom panel contains the scale factor values ($\epsilon_{Data}/\epsilon_{MC}$). Both statistical and systematic uncertainties (fit error, leakage uncertainty, and Loose' uncertainty) are shown in the error bars.}
\label{fig:unconv_sf_all_wps_SgPhCalo}
\end{figure}

\begin{figure}
\centering
\hspace{-1cm}
\subfloat[\emph{central converted photons}]{\includegraphics[width=0.31\textwidth]{ph_iso-caloSnPh/ScaleFactorPlots/{SFFixedCutTight_2017_conv}}}
\hspace{1cm}
\subfloat[\emph{endcap unconverted photons}]{\includegraphics[width=0.31\textwidth]{ph_iso-caloSnPh/ScaleFactorPlots/{SFFixedCutTight_endcap_2017_unconv}}} \\
\caption{Measurement using single photons, calo-only: The \texttt{FixedCutTight} working point efficiencies as a function of \ET\ for (a) converted photons in the rapidity range $|\eta|<0.6$, and for (b) unconverted photons in the endcap rapidity range $1.81<|\eta|<2.37$. The Data efficiency is shown in black, and the Monte Carlo efficiency is shown in red. The bottom panel contains the scale factor values ($\epsilon_{Data}/\epsilon_{MC}$). Both statistical and systematic uncertainties (fit error, leakage uncertainty, and Loose' uncertainty) are shown in the error bars.}
\label{fig:sf_comparison_wps_SgPhCalo}
\end{figure}



\textbf{Large scale factors in FixedCutTightCaloOnly Working Point}\\
The scale factors for high-$E_T$ photons in the \texttt{FixedCutTightCaloOnly} working point were observed to be as large as about $1.1$, such as for the $0.0<|\eta|<0.6$ pseudo-rapidity bin for unconverted photons in the 2017 data-taking period. The efficiencies and scale factors are shown together in Figure \ref{fig:unconv_sf_all_wps_SgPhCalo}b. The data efficiency appears to increase with $E_T$ at a faster rate than the MC efficiency, which causes the scale factor value to increase. 

The quality of the final fit to tight data was investigated for the highest $E_T$ bin, since poor background subtraction could lead to an overestimation of the data efficiency. This fit is shown in Figure \ref{fig:FixedCutTightCaloOnly_LargeSF_SBfit}. There does not appear to be significant background contamination in this fit. In addition, the fraction of fake photon events in the high-$E_T$ region is typically low, as can be seen in the fit. The signal shape does appear to show some mis-modeling; however, the data efficiency is calculated from the actual data distribution (minus the background shape) and not from the signal component of the fit. Therefore, this mis-modeling is unlikely to explain the large scale factor in this bin. 

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth,angle=270]{ph_iso-caloSnPh/extraFixedCutTightCaloOnly/tightDataBkgCB2SigACB2_topoETcone40pt_2017_unconv_pT_105_125_absEta_115_137_Loose4_NominalLeakage.pdf}
\caption{Measurement using single photons, calo-only: The final signal and background fit to tight data in for unconverted photons in the $0.0<|\eta|<0.6$ region in 2017 data. The background (fake photon) shape is shown in red, while the total signal plus background shape is shown in blue.}
\label{fig:FixedCutTightCaloOnly_LargeSF_SBfit}
\end{figure}

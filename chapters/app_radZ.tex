\section{Isolation photon efficiency in radiative Z decays}
\label{sec:radZ_app}

\newcommand{\effiso}{\hbox{$\varepsilon_{\textrm{iso}}$}}

\subsection{Template fit method}
\label{sec:TemplateFit_radZ}

A template fit to the radiative Z boson $m_{\ell\ell\gamma}$ invariant mass distribution is used to evaluate the signal purities ($P$) and yields ($S$) before and after the photon isolation WP selection. To perform the fit the $m_{\ell\ell\gamma}$ requirement is removed from the selection and the probability density functions (PDFs) of $m_{\ell\ell\gamma}$ are extracted for signal ($Z\to\ell\ell\gamma$) and background ($Z\to\ell\ell$ + jets) Monte Carlo. The sum of the signal and background PDFs, with floating normalizations, is fit to the data distribution.\\
Two fits are performed: one for all the events after the nominal selection, shown in fig~\ref{fig:PDF_before_tight}, and one for the events in which the photon candidate also passes the isolation requirements, shown in Fig~\ref{fig:PDF_after_tight}. The fitted shape is always in good agreement with the data distribution. The fit range is chosen as [65, 105] GeV. From the fit the signal purity is determined in the signal region $80$~GeV$<m_{\ell\ell\gamma}<100$~GeV: purity for the region 10<$\pT$<20 GeV before the isolation requirements is 94.5\% for unconverted photons and 93.9\% for converted photons. After applying the isolation criteria (WP FixedCutLoose), the photon purity is 95.9\% for unconverted photons and 95.4\% for converted photons.\\

\begin{figure}[!htbp]
\centering
  \includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{Zll_data17_mc_3mass_rf_unc}}
  \includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{Zll_data17_mc_3mass_rf_conv}}
  \caption{Invariant mass ($m_{\ell\ell\gamma}$) distribution of events selected in data after applying all the $Z\to\ell\ell\gamma$ selection criteria except that on $m_{\ell\ell\gamma}$ (black dots). The solid black line represents the result of the fit of the data distribution with the sum of the signal (red dashed line) and background (blue dotted line) invariant mass distributions obtained from MC. Left: $l=e$. Right: $l=\mu$. Top: photons are reconstructed as unconverted. Bottom: photons are reconstructed as converted.}
  \label{fig:PDF_before_tight}
\end{figure}

\begin{figure}[!htbp]
  \centering
  \includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{Zll_data17_mc_3mass_rf_tight_unc}}
  \includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{Zll_data17_mc_3mass_rf_tight_conv}}
  \caption{Invariant mass ($m_{\ell\ell\gamma}$) distribution of events selected in data after applying all the $Z\to\ell\ell\gamma$ selection criteria except that on $m_{\ell\ell\gamma}$ (black dots). In addition, the photon candidates are required to pass isolation FixedCutTight WP. The solid black line represents the result of the fit of the data distribution with the sum of the signal (red dashed line) and background (blue dotted line) invariant mass distributions obtained from MC. Top: photons are reconstructed as unconverted. Bottom: photons are reconstructed as converted.}
  \label{fig:PDF_after_tight}
\end{figure}

The average purity over 20-100 GeV is essentially dominated by the purity in the 20-25 GeV bin. This is shown in table~\ref{tab:purity_vs_et}, where the fitted purity in the 20-25 GeV bin is compared to the purity in the $>20$~GeV bin. 

\begin{table}[!htbp]
  \begin{center}
    \begin{tabular}{lll}
    \hline
    \hline
    Conversion type   & Purity [\%]     & Purity [\%]      \\
    & $20<\ET<25$ GeV & $\ET>20$~GeV     \\
    \hline
    Unconverted $\gamma$     & $99.8 \pm 0.2$  & $99.9 \pm 0.1$   \\
    Converted $\gamma$ & $99.8 \pm 0.3$  & $99.9 \pm 0.2$   \\
    \hline
    \hline
    \end{tabular}
    \caption{Fitted photon purity of converted and unconverted photons, before isolation requirement, for region $20<E_T<25$ GeV and for $E_T>20$ GeV. The uncertainty are only statistical.}
    \label{tab:purity_vs_et}   
  \end{center}
\end{table}

Obtained numbers allow to calculate number of background events and subtract them from the sample. The isolation efficiency in the $10<\ET<20$ GeV region is corrected as follows: 
\begin{equation}
  \effiso = \frac{N_{\textrm{probes, iso}} - N_{bkg,iso}}{N_{\textrm{probes}} - N_{bkg}} ,
\end{equation}
which is equivalent to:
\begin{equation}
  \effiso = \frac{N_{sig,iso}}{N_{sig}} .
\end{equation}

\subsection{Systematic Uncertainties}
\label{sec:syst_radZ}

Several sources of systematic uncertainty are contributing to the total systematic uncertainty on the measurement of photon isolation efficiency. The most part of them are coming from template fit method itself. One more general source which is not related to the method arises from pile-up dependence (sec.~\ref{sec:pileup_radZ}). 

\subsubsection{Closure test} 

The results on photon isolation efficiency coming from true photons are compared to the results of template fit method. Simulated background was added as a sum to the signal truth photons, coming from radiative Z decays. Results coming from template fit method also present sum of background and signal events. Closure test was provided only for $\ET$ region [10, 20] GeV due to lack of statistics in higher regions. The maximum difference between 2 cases is less than 0.1\% for all isolation working points.

\subsubsection{MC-related uncertainty}

Alternative MC event generator sample was chosen, where uncertainty comes from deviation in the MC predictions of the background. Subtraction of background events (for \ET in [10, 20] GeV) with templated fit method was obtained with use of templates, extracted from alternative MC event generator \textsc{Powheg}+\textsc{Pythia} instead of nominal one (\textsc{Sherpa}). The final uncertainty is evaluated from difference between nominal scale factors and scale factors, where numerator is data efficiency which was corrected with use of templates extracted from alternative MC event generator, and denominator is efficiency from nominal MC sample. The maximum value is 0.4\% for both unconverted and converted photons.

\FloatBarrier

\subsubsection{Pile-up dependence}
\label{sec:pileup_radZ}

In this section the (in-time) pile-up dependence of the photon isolation efficiency is evaluated, as a function of $\mu$. The photon probes are divided to smaller subsets according to $\mu$ to which the photon belongs to. \\
According to pile-up recommendations for Run2, MC samples should be reweighted to match actual $\mu$ distribution in data. However, there might be residual differences between data and MC efficiencies due to incorrect simulation of the detector response to pile-up. \\
In fig.~\ref{fig:eff_pileup_radZ} the in-time pile-up dependence of efficiency obtained with radiative $Z\to \ell\ell\gamma$ decays in data sample is compared to the same quantities obtained in simulated $Z\to \ell\ell$ samples. Only photons with $20 < \ET < 40$ GeV are used to avoid background contamination in the data sample.  The pile-up dependence is presented for unconverted and converted photons, in four $\eta$ regions. The efficiencies show similar dependence for both data and MC for FixedCutLoose WP, some major differences are observed for barrel in case of converted photons, for FixedCutTight WP, and in case of FixedCutTightCaloOnly differences between data and MC efficiencies are sen mostly in medium and high regions of $\mu$.

\begin{figure}[!htbp]
  \centering
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_unc_wploose_eta0}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_unc_wploose_eta1}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_unc_wploose_eta2}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_unc_wploose_eta3}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_conv_wploose_eta0}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_conv_wploose_eta1}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_conv_wploose_eta2}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_conv_wploose_eta3}}}
  \caption{Photon isolation efficiencies for \texttt{FixedCutLoose} isolation WP versus $\mu$ for reconstructed unconverted photons (left) and converted photons (right), in four different pseudorapidity regions. Solid black dots represent data, hollow red dots - MC sample.}
  \label{fig:eff_pileup_radZ}
\end{figure}

\begin{figure}[!htbp]
  \centering
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_unc_wptight_eta0}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_unc_wptight_eta1}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_unc_wptight_eta2}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_unc_wptight_eta3}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_conv_wptight_eta0}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_conv_wptight_eta1}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_conv_wptight_eta2}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_conv_wptight_eta3}}}
  \caption{Photon isolation efficiencies for \texttt{FixedCutTight} isolation WP versus $\mu$ for reconstructed unconverted photons (left) and converted photons (right), in four different pseudorapidity regions. Solid black dots represent data, hollow red dots - MC sample.}
  \label{fig:eff_pileup_radZ}
\end{figure}

\begin{figure}[!htbp]
  \centering
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_unc_wpcaloonly_eta0}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_unc_wpcaloonly_eta1}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_unc_wpcaloonly_eta2}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_unc_wpcaloonly_eta3}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_conv_wpcaloonly_eta0}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_conv_wpcaloonly_eta1}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_conv_wpcaloonly_eta2}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_mu_conv_wpcaloonly_eta3}}}
  \caption{Photon isolation efficiencies for \texttt{FixedCutTightCaloOnly} isolation WP versus $\mu$ for reconstructed unconverted photons (left) and converted photons (right), in four different pseudorapidity regions. Solid black dots represent data, hollow red dots - MC sample.}
  \label{fig:eff_pileup_radZ}
\end{figure}

The dependence of the scale factors on $\mu$ provides a good estimate of the pile-up dependence. The slope of data/MC ratio is expected to be zeror. The ratios between data and simulated efficiencies as a function of $\mu$ are fitted with a linear distribution. The slopes are listed in tables~\ref{tab:pileup_slopes_radZ_loose},~\ref{tab:pileup_slopes_radZ_tight},~\ref{tab:pileup_slopes_radZ_caloonly}. Those slopes are less than 1$\%$ within uncertainties. The pile-up uncertainty is not taken into account.

\begin{table}[!htbp]
  \begin{center}
    \begin{tabular}{llllll}
    \hline
    \hline
    &$|\eta|$    &$0.00<|\eta|<0.60$ &$0.60<|\eta|<1.37$   &$1.52<|\eta|<1.81$   &$1.81<|\eta|<2.37$  \\
    \hline
    &unconverted & 0.0001 $\pm$ 0.0004 & 0.0003 $\pm$ 0.0008 & 0.002 $\pm$ 0.0002 & -0.0013 $\pm$ 0.0006  \\
    \hline
    &converted & 0.0003 $\pm$ 0.0001 & 0.0006 $\pm$ 0.0014 & 0.0049 $\pm$ 0.0003 & -0.0012 $\pm$ 0.0009 \\
    \hline
    \hline
    \end{tabular}
    \caption{Slope of the fit of the scale factors as a function of $\mu$ with a linear distribution, in  case of \texttt{FixedCutLoose} isolation WP}  
    \label{tab:pileup_slopes_radZ_loose}
  \end{center}
\end{table}
\begin{table}[!htbp]
  \begin{center}
    \begin{tabular}{llllll}
    \hline
    \hline
    &$|\eta|$    &$0.00<|\eta|<0.60$ &$0.60<|\eta|<1.37$   &$1.52<|\eta|<1.81$   &$1.81<|\eta|<2.37$  \\
    \hline
    &unconverted & 0.0013 $\pm$ 0.0004 & 0.0005 $\pm$ 0.0003 & 0.0015 $\pm$ 0.0008 & -0.0007 $\pm$ 0.0007  \\
    \hline
    &converted & 0.0011 $\pm$ 0.0008 & 0.0019 $\pm$ 0.0005 & 0.0007 $\pm$ 0.0011 & 0.0002 $\pm$ 0.0009 \\
    \hline
    \hline
    \end{tabular}
    \caption{Slope of the fit of the scale factors as a function of $\mu$ with a linear distribution, in  case of \texttt{FixedCutTight} isolation WP}  
    \label{tab:pileup_slopes_radZ_tight}
  \end{center}
\end{table}
\begin{table}[!htbp]
  \begin{center}
    \begin{tabular}{llllll}
    \hline
    \hline
    &$|\eta|$    &$0.00<|\eta|<0.60$ &$0.60<|\eta|<1.37$   &$1.52<|\eta|<1.81$   &$1.81<|\eta|<2.37$  \\
    \hline
    &unconverted & 0.0014 $\pm$ 0.0003 & 0.0009 $\pm$ 0.0002  & 0.0028 $\pm$ 0.0007 & 0.0003 $\pm$ 0.0005   \\
    \hline
    &converted & 0.0019 $\pm$ 0.0007 & 0.0019 $\pm$ 0.0004 & 0.0024 $\pm$ 0.0008 & 0.0013 $\pm$ 0.0007  \\
    \hline
    \hline
    \end{tabular}
    \caption{Slope of the fit of the scale factors as a function of $\mu$ with a linear distribution, in  case of \texttt{FixedCutTightCaloOnly} isolation WP}  
    \label{tab:pileup_slopes_radZ_caloonly}
  \end{center}
\end{table}




In this study the alternative background estimation method based on the photon identification requirements is presented. It gives results consistent with the template fit method and therefore used only as a cross-check.\\
The method is defined as a 2D-sideband method and it is based on the identification (ID) of the photon candidate and the two-body invariant mass $m_{\ell\ell}$ requirement. One region is defined as signal region (A) with Z+$\gamma$ events and other three regions are enriched with Z+jet events. \\
The tight ID requirement is used in signal A region since it provides good background rejection, while retaining a photon efficiency above 98$\%$ for photons with transverse momenta above 20 GeV. Regions C and D use looser or "not-tight" photon identification requirement (called "loose prime4"), obtained from the nominal tight ID criteria by removing the requirements on the four shower shape variables $\Delta$E, $w_{s3}$, $E_{ratio}$ and $F_{side}$. \\
The two-body invariant mass requirement is good to be used on both selection of Z boson and background sample: by reversing the $m_{\ell\ell}$ from 40~GeV < $m_{\ell\ell}$ < 83~GeV to $m_{\ell\ell}$ > 85~GeV almost all photon candidates should be jets since the minimal photon energy is \ET > 10~GeV (fig.~\ref{fig:truth_mass_radZ}). 

\begin{figure}[!htbp]
  \centering
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{Zee_mc_2mass_truth_unc}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{Zee_mc_2mass_truth_conv}}}
  \caption{The two-body invariant mass $m_{ee}$ distributions for unconverted (left) and converted (right) photons, for background-enriched MC sample and from truth jets}
  \label{fig:truth_mass_radZ}
\end{figure}


The four method regions are defined as follows:
\begin{itemize}
  \item Tight and direct mass cut region (A): the photon candidates are required to pass the tight ID criteria and $40<m_{\ell\ell} <83$~GeV;
  \item Tight, reversed mass cut region (B): the photon candidates are required to have two-body invariant mass $m_{\ell\ell}$ > 85~GeV but pass tight ID criteria;
  \item Not-tight, direct mass cut region (C): the photon candidates have mass requirement $40<m_{\ell\ell} <83$~GeV, fail the tight ID critera but pass "loose prime4" ID criteria;
  \item  Not-tight, reversed mass cut region (D): the photon candidates are required to have two-body invariant mass $m_{\ell\ell}$ > 85~GeV, fail the tight ID critera but pass "loose prime4" ID criteria.
\end{itemize}

The 2 GeV-wide two-body invariant mass gap between the signal and background regions helps reducing the signal leakage into the background region.

Fig.~\ref{fig:eff_2methods_radZ} shows photon isolation efficiencies (WP FixedCutTight), obtained for unconverted and converted photons, in case where background subtraction of data was provided with template fit method, and in case where it was provided with 2-D sideband method. The maximum difference between two method is 2$\%$ for unconverted photons and 4$\%$ for converted photons.

\begin{figure}[!htbp]
  \centering
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_corr_unc_wploose_idincl_eta0_06}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{eff_data17_mc_corr_conv_wploose_idincl_eta0_06}}}
  \caption{Comparison of photon efficiency results for \texttt{FixedCutLoose} isolation WP measured using $Z\to\ell\ell\gamma$ from data and MC simulated samples for unconverted (left) and converted (right) photons. Solid black dots represent data, corrected with template fit method, hollow gray dots represent data, corrected with data-driven method, hollow red dots - MC sample.}
  \label{fig:eff_2methods_radZ}
\end{figure}

\subsubsection{Results}

Total systematic uncertainties include uncertainties such as uncertainty coming from the found non-closure in simulation, template fit method, difference in data efficiencies coming from template fit method and data-driven method and alternative MC event generator. The effect coming from pile-up dependence is compatible with zero and is not taken into account. Individual and total uncertainties are shown in Fig.~\ref{fig:tot_unc} for both unconverted and converted photons, in four $|\eta|$ bins. 

\begin{figure}[!htbp]
  \centering
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{uncert_data17_unc_wploose_eta0_06}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{uncert_data17_conv_wploose_eta0_06}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{uncert_data17_unc_wploose_eta06_137}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{uncert_data17_conv_wploose_eta06_137}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{uncert_data17_unc_wploose_eta152_181}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{uncert_data17_conv_wploose_eta152_181}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{uncert_data17_unc_wploose_eta181_237}}}
  {\includegraphics[width=0.48\textwidth,angle=0]{photon_radZ/{uncert_data17_conv_wploose_eta181_237}}}
  \caption{Systematic uncertainties for \texttt{FixedCutLoose} isolation WP, measured using $Z\to\ell\ell\gamma$ from data and MC simulated samples in four different pseudorapidity regions. }
  \label{fig:tot_unc}
\end{figure}


\section{Low-pt ID dedicated calibration}
\label{sec:applowptiso}

\subsection{Introduction}
Nominal isolation scale factors and systematic uncertainties are measured for all the Muon ID working points inclusively, 
as it is estimated that the muon quality does not influence the isolation performance.
Medium ID muon is used to provide central value of the SFs, and Loose/Tight ID muons are used to assign systematics.
At very low-pt, especially at $\pt <4$ GeV, the dominate systematic uncertainty source is the muon ID variation.
Limited statics is also problematic in this region, as Medium ID efficiency is very low at low-$\pt$.
The aim of providing LowPtID dedicated SFs is to avoid these large uncertainties.

The same samples as described in Sec.~\ref{ssec:isolation-workingpointsmuons} were used for this calibration. 
Some technical details are described in ~\ref{ssec:applowptiso-technical}.
Tag-and-Probe selections are slightly different from the nominal isolation efficiency measurement, as described in~\ref{ssec:applowptiso-tpselection}.
Efficiency measurement and background subtraction method are different as well, which is described in detail in~\ref{ssec:applowptiso-bkgsubtraction}.
Efficiencies, scale factors, and systematics breakdown for all the official working points are shown in~\ref{ssec:applowptiso-results}

\subsection{Technical Details}
\label{ssec:applowptiso-technical}
MUONTP ntuple version: v054a\\
MuonTPPostprocessing branch: dedicated \apkg{https://gitlab.cern.ch/atlas-mcp/MuonTPPostProcessing/tree/shun-plt}{branch} for this study

\subsection{Tag-and-Probe selection}
\label{ssec:applowptiso-tpselection}

\begin{table}[htbp]
  \centering
  \begin{tabular}{|c|c|}
    \hline
    Variable & Criteria \\
    \hline
    Tag Isolation                         & FCLoose          \\
    Tag $|z_0\sin\theta| $                & $< 0.5$ mm       \\
    Tag $\left|d_0/\sigma(d_0)\right| $   & $< 3$            \\
    Probe Identification                  & LowPtID          \\
    Probe $|z_0\sin\theta| $              & $< 0.5$ mm       \\
    Probe $\left|d_0/\sigma(d_0)\right| $ & $< 3$            \\
    $m_{\mathrm{ll}}$                & $\subset [86, 96]$ GeV \\      
    d$R({\mathrm{tag, probe}})$      & $> 0.4$                \\
    d$R({\mathrm{probe, jet}})$      & $> 0.4$                \\    
    \hline
  \end{tabular}
  \caption{Additional event selection applied in MuonTPPostProcessing. Note that more basic cuts are already applied when creating MUONTP ntuples.}
  \label{tab:lowptiso-tpselection}
\end{table}


\subsection{Efficiency calculation and background subtraction}
\label{ssec:applowptiso-bkgsubtraction}

Isolation efficiency is defined in a simple form,
\newcommand{\myNumber}[3]{{#1}_{\mathrm{#2}}^{\mathrm{#3}}}
  \begin{equation}
  \label{eq:lowptiso-eff}
  \epsilon = \myNumber{N}{matched}{sub} / \myNumber{N}{probe}{sub},
\end{equation}
where the number of events that passed tag-and-probe selection comes to the denominator, 
and the number of events that also passed the isolation comes to the numerator.
Here, $\myNumber{N}{matched}{sub}$ and $\myNumber{N}{probe}{sub}$ is the number of events after background subtraction. 

Background subtraction is performed separately for probe and matched events, in a same procedure.
The procedure is different for data and MC, shown in Eq.~\ref{eq:lowptiso-data} and Eq.~\ref{eq:lowptiso-zmumu}.
\begin{equation}
  \label{eq:lowptiso-data}
  \myNumber{N}{data}{sub} = \myNumber{N}{data}{OC} - \myNumber{T}{data}{OC/SC}\cdot \myNumber{N}{data}{SC} - \myNumber{T}{Irr}{scale}\cdot \myNumber{N}{Irr}{sub},
\end{equation}

\begin{equation}
  \label{eq:lowptiso-zmumu}
  \myNumber{N}{MC}{sub} = \myNumber{N}{Zmumu}{OC} -  \myNumber{T}{MC}{OC/SC}\cdot \myNumber{N}{Zmumu}{SC}.
\end{equation}

$\myNumber{N}{data}{OC}$, $\myNumber{N}{data}{SC}$, $\myNumber{N}{Zmumu}{OC}$, $\myNumber{N}{Zmumu}{SC}$ is the number of data-OC, data-SC, Zmumu-OC, Zmumu-SC events, respectively.
$\myNumber{T}{data}{OC/SC}$ is a transfer factor to compensate the OC/SC asymmetry in data.
$\myNumber{T}{MC}{OC/SC}$ is a transfer factor for MC OC/SC ratio, $\myNumber{T}{Irr}{scale}$ is a scale factor for MC irreducible background normalization.

$\myNumber{N}{Irr}{sub}$ is defined as 
\begin{equation}
  \label{eq:lowptiso-irr}
  \myNumber{N}{Irr}{sub} = \myNumber{N}{Irr}{OC} - \myNumber{T}{MC}{OC/SC}\cdot \myNumber{N}{Irr}{SC},
\end{equation}
where $\myNumber{N}{Irr}{OC}$ and $\myNumber{N}{Irr}{SC}$ is again the the number of irreducible background OC and SC events.

These subtraction procedures are based on a several assumptions:
\begin{itemize}
  \item Irreducible background (events with two prompt OC muons) are well modeled by MC
  \item Reducible background (events with at least one non-prompt muon) are not well modeled by MC
  \item SC event is dominated by events that contain non-prompt muons
  \item OC/SC ratio for irreducible background is not always 1, and are not well modeled by MC
\end{itemize}

$\myNumber{T}{data}{OC/SC}$ is a data-driven transfer factor, calculated at high-mass tail of the $\mll$ distribution, $\mll \subset [111, 121]$~GeV.
The events in this region are enriched by irreducible background. Based on the MC study, real contamination at this region is estimated to be below $1\%$.
The $\mll$ distributions in various probe muon $\pt$ regions are shown in Fig.~\ref{fig:lowptiso-mll-probe} and Fig.~\ref{fig:lowptiso-mll-match-fctighttrackonly}.
This transfer factor was varied from $\myNumber{T}{data}{OC/SC} = 1.0$ to $\myNumber{T}{data}{OC/SC} = 1.0 + 2\times|(\mathrm{measured~ratio})-1|$, to assign systematics.

\begin{figure}
	\centering
	{\includegraphics[scale = 0.25]{figures/lowptiso/Data-Probe-bin1}}
	{\includegraphics[scale = 0.25]{figures/lowptiso/Data-Probe-bin2}}
	{\includegraphics[scale = 0.25]{figures/lowptiso/Data-Probe-bin3}}\\
	{\includegraphics[scale = 0.25]{figures/lowptiso/Data-Probe-bin4}}
	{\includegraphics[scale = 0.25]{figures/lowptiso/Data-Probe-bin5}}
	{\includegraphics[scale = 0.25]{figures/lowptiso/Data-Probe-bin6}}\\
	{\includegraphics[scale = 0.25]{figures/lowptiso/Data-Probe-bin7}}
	{\includegraphics[scale = 0.25]{figures/lowptiso/Data-Probe-bin8}}
	{\includegraphics[scale = 0.25]{figures/lowptiso/Data-Probe-bin9}}
	\caption{$\mll$ distributions for events that passed tag-and-probe selection, before isolation requirements.
          Each plot shows distribution for different probe muon \pt range.
          Red histograms show the distribution of opposite-charge events, blue histograms show the distribution for same-charge events.
          Green histograms are distribution after scaling, where the scale factors are taken from the high-mass tail.           
        }
        \label{fig:lowptiso-mll-probe}
\end{figure}


\begin{figure}
	\centering
	{\includegraphics[scale = 0.25]{figures/lowptiso/FCTightTrackOnly-Data-Match-bin1}}
	{\includegraphics[scale = 0.25]{figures/lowptiso/FCTightTrackOnly-Data-Match-bin2}}
	{\includegraphics[scale = 0.25]{figures/lowptiso/FCTightTrackOnly-Data-Match-bin3}}\\
	{\includegraphics[scale = 0.25]{figures/lowptiso/FCTightTrackOnly-Data-Match-bin4}}
	{\includegraphics[scale = 0.25]{figures/lowptiso/FCTightTrackOnly-Data-Match-bin5}}
	{\includegraphics[scale = 0.25]{figures/lowptiso/FCTightTrackOnly-Data-Match-bin6}}\\
	{\includegraphics[scale = 0.25]{figures/lowptiso/FCTightTrackOnly-Data-Match-bin7}}
	{\includegraphics[scale = 0.25]{figures/lowptiso/FCTightTrackOnly-Data-Match-bin8}}
	{\includegraphics[scale = 0.25]{figures/lowptiso/FCTightTrackOnly-Data-Match-bin9}}
	\caption{$\mll$ distributions for events that passed tag-and-probe selection, with probe muon passing FCTightTrackOnly.
          Each plot shows distribution for different probe muon \pt range.
          Red histograms show the distribution of opposite-charge events, blue histograms show the distribution for same-charge events.
          Green histograms are distribution after scaling, where the scale factors are taken from the high-mass tail.           
        }
        \label{fig:lowptiso-mll-match-fctighttrackonly}
\end{figure}


$\myNumber{T}{MC}{OC/SC}$ was measured in each MC samples, where they were found to be consistent with 1.
Therefore we took $\myNumber{T}{MC}{OC/SC}$ to be 1 as nominal, and assigned 10\% variance to this transfer factor, to be conservative.
10\% variance was considered for $\myNumber{T}{Irr}{scale}$ as well, which was found to be negligible compared to other systematics.

All the systematics considered is summarized in Table~\ref{tab:lowptiso-sys}.
For each variable, UP and DOWN type variations were considered.

\begin{table}
	\centering
	\caption{Systematic variations for the muon isolation scale factor measurements.}
        \label{tab:lowptiso-sys}
	\begin{tabular}{c c}
		\hline
		\hline
		Definition of systematic & Range of variation \\
		\hline
		$Z$ mass & ($88$ - $\SI{94}{\giga\electronvolt}$) or ($81$ - $\SI{101}{\giga\electronvolt}$) \\
		Tag isolation & Maximum variation of \emph{FCTight} and \emph{FCTightTrackOnly} \\
		$\Delta{}R_{\mu\mu}$ & $\Delta{}R_{\mu\mu} > 0.3$ or $\Delta{}R_{\mu\mu} > 0.5$ \\
		$\Delta{}R_{\mu{}j}$ & $\Delta{}R_{\mu{}j} > 0.3$ or $\Delta{}R_{\mu{}j} > 0.5$ \\
		\hline
		MC OC/SC ratio                      & $1.0 \pm 0.1$  \\
		MC Background scale                 & $1.0 \pm 0.1$  \\
		Data OC/SC ratio                    & $1.0$ or $1.0 + 2\times|(\mathrm{measured~ratio})-1|$  \\
		Data OC/SC ratio measurement region &  [116, 121] GeV  \\
		\hline
		\hline
	\end{tabular}
\end{table}


\subsection{Results}
\label{ssec:applowptiso-results}

Efficiencies and scale factors for all the official working points are shown in Fig.~\ref{fig:lowptiso-eff}.
Scale factors deviate from 1 at low-$\pt$, with the largest deviation of $\sim 0.95$ at lowest $\pt$ bin.
The scale factors in all the $\pt$ range and working points are consistent with 1.0 within error.

Systematics breakdown for all the working points are shown in Fig.~\ref{fig:lowptiso-sys}.
The dominant systematic sources at low-$\pt$ is $\myNumber{T}{data}{OC/SC}$ uncertainty (``Data OC/SC'' in the legend).
This is as expected, as we assign large systematics to the $\myNumber{T}{data}{OC/SC}$ to be conservative.

\begin{figure}
	\centering
	\subfloat[\emph{FCLoose}]{\includegraphics[scale = 0.25]{figures/lowptiso/Eff-IsoFCLoose}}
	\subfloat[\emph{FCLoose\_FixedRad}]{\includegraphics[scale = 0.25]{figures/lowptiso/Eff-IsoFCLoose-FixedRad}}
	\subfloat[\emph{FCTight}]{\includegraphics[scale = 0.25]{figures/lowptiso/Eff-IsoFCTight}}\\
	\subfloat[\emph{FCTight\_FixedRad}]{\includegraphics[scale = 0.25]{figures/lowptiso/Eff-IsoFCTight-FixedRad}}
	\subfloat[\emph{FCTightTrackOnly}]{\includegraphics[scale = 0.25]{figures/lowptiso/Eff-IsoFCTightTrackOnly}}
	\subfloat[\emph{FCTightTrackOnly\_FixedRad}]{\includegraphics[scale = 0.25]{figures/lowptiso/Eff-IsoFCTightTrackOnly-FixedRad}}\\
	\subfloat[\emph{FixedCutHighPtTrackOnly}]{\includegraphics[scale = 0.25]{figures/lowptiso/Eff-IsoFixedCutHighPtTrackOnly}}
	\subfloat[\emph{FixedCutPflowLoose}]{\includegraphics[scale = 0.25]{figures/lowptiso/Eff-IsoFixedCutPflowLoose}}
	\subfloat[\emph{FixedCutPflowTight}]{\includegraphics[scale = 0.25]{figures/lowptiso/Eff-IsoFixedCutPflowTight}}
	\caption{LowPtID-muon isolation efficiencies and scale factors for 
          (a) \emph{FCLoose}, 
          (b) \emph{FCLoose\_FixedRad}, 
          (c) \emph{FCTight}, 
          (d) \emph{FCTight\_FixedRad}, 
          (e) \emph{FCTightTrackOnly}, 
          (f) \emph{FCTightTrackOnly\_FixedRad}, 
          (g) \emph{FixedCutHighPtTrackOnly}, 
          (h) \emph{FixedCutPflowLoose}, 
          (i) \emph{FixedCutPflowTight}.
        }
        \label{fig:lowptiso-eff}
\end{figure}


\begin{figure}
	\centering
	\subfloat[\emph{FCLoose}]{\includegraphics[scale = 0.25]{figures/lowptiso/Sys-IsoFCLoose}}
	\subfloat[\emph{FCLoose\_FixedRad}]{\includegraphics[scale = 0.25]{figures/lowptiso/Sys-IsoFCLoose-FixedRad}}
	\subfloat[\emph{FCTight}]{\includegraphics[scale = 0.25]{figures/lowptiso/Sys-IsoFCTight}}\\
	\subfloat[\emph{FCTight\_FixedRad}]{\includegraphics[scale = 0.25]{figures/lowptiso/Sys-IsoFCTight-FixedRad}}
	\subfloat[\emph{FCTightTrackOnly}]{\includegraphics[scale = 0.25]{figures/lowptiso/Sys-IsoFCTightTrackOnly}}
	\subfloat[\emph{FCTightTrackOnly\_FixedRad}]{\includegraphics[scale = 0.25]{figures/lowptiso/Sys-IsoFCTightTrackOnly-FixedRad}}\\
	\subfloat[\emph{FixedCutHighPtTrackOnly}]{\includegraphics[scale = 0.25]{figures/lowptiso/Sys-IsoFixedCutHighPtTrackOnly}}
	\subfloat[\emph{FixedCutPflowLoose}]{\includegraphics[scale = 0.25]{figures/lowptiso/Sys-IsoFixedCutPflowLoose}}
	\subfloat[\emph{FixedCutPflowTight}]{\includegraphics[scale = 0.25]{figures/lowptiso/Sys-IsoFixedCutPflowTight}}
	\caption{LowPtID-muon systematics breakdown for 
          (a) \emph{FCLoose}, 
          (b) \emph{FCLoose\_FixedRad}, 
          (c) \emph{FCTight}, 
          (d) \emph{FCTight\_FixedRad}, 
          (e) \emph{FCTightTrackOnly}, 
          (f) \emph{FCTightTrackOnly\_FixedRad}, 
          (g) \emph{FixedCutHighPtTrackOnly}, 
          (h) \emph{FixedCutPflowLoose}, 
          (i) \emph{FixedCutPflowTight}.
        }
        \label{fig:lowptiso-sys}
\end{figure}

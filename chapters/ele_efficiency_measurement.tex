The electron isolation scale factors are computed separately for 2015, 2016, 2017 and 2018 data sets. They are obtained with respect to MC16 simulations: MC16a for 2015 and 2016, MC16d for 2017 and FullSim MC16e for 2018. For FastSim no results were obtained with 2018 data because of the missing MC16e FastSim samples, and the scale factors measured with 2017 data are used instead. In this document only the measurements performed with 2017 data are documented.

The isolation efficiencies and scale factors are determined with a tag and probe method in
$Z\to ee$ events, using the TagAndProbe framework~\cite{twiki-TAP}.
The details of the tag and probe method can be found in~\cite{PERF-2017-01}.
The efficiency is obtained by calculating the
ratio between the number of electron candidates passing the identification and isolation
requirements and the number of candidates passing the identification requirement
only. After getting the efficiency for data and MC, the scale factors are
obtained by taking the ratio of efficiency between data and MC. Since the
efficiencies depend on the transverse energy and pseudo-rapidity, the
measurements are performed in two-dimensional bins in ($\pt, \eta$):
\begin{itemize}
\item 20 bins in $\eta$: [-2.47, -2.37, -2.01, -1.81, -1.52, -1.37, -1.15, -0.80, -0.60, -0.10, 0.00, 0.10, 0.60, 0.80, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37, 2.47]
\item 13 bins $\pt$: [4.5, 7, 10, 15, 20, 25, 30, 40, 45, 50, 60, 80, 150, 200, 250, 300, 350, 500] GeV
\end{itemize}
Due the low number of probe electrons in the high-\pt region, the $\eta$ binning is reduced
for $200\ \gev < \pt < 500\ \gev$ to :
\begin{itemize}
\item $|\eta|$: [0.00, 0.60, 1.37, 1.52, 2.47]
\end{itemize}

For the assessment of the systematic uncertainties in the measurement, variations
are applied to the selection of the tag electron, in the dilepton invariant mass
window considered and in the templates used for background subtraction, as
detailed below. The central value of the efficiency is then calculated as the
mean value of all the variation efficiencies and the systematic uncertainty as
the standard deviation.

\begin{itemize}
\item Tag ID: Medium + \etcb < 5 \gev, Tight, Tight + \etcb < 5 \gev.
\item Electron pair invariant mass window: $[80, 100]$, $[75, 105]$, $[70, 110]$ \gev;
\item Two different background templates;
\end{itemize}

The isolation efficiency and scale factors for the different working points are
computed for the three different identification selections of the electrons (Loose, Medium and Tight).

Figure~\ref{fig:eleiso_eff_sf_vs_eta_bins} shows the efficiency and scale factors
as a function of $\eta$ for the different isolation  working points, for Medium
electrons with $30\ \gev < \pt < 35\ \gev$. Similarly, Figure~\ref{fig:eleiso_eff_sf_vs_pt_bins}
shows the isolation efficiencies and scale factors as a function of the electron \pt
for Medium electrons with $0.1 < \eta < 0.6$. The full scale factor 2D maps as a function of \pt and $\eta$ can be found
in Figure~\ref{fig:eleiso_2d_maps} for Medium electrons.

\begin{figure}[h]
  \centering

  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_eta__pt_30_35_data17_MediumLH_Gradient.pdf}
  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_eta__pt_30_35_data17_MediumLH_FCHighPtCaloOnly.pdf}

  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_eta__pt_30_35_data17_MediumLH_FCLoose.pdf}
  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_eta__pt_30_35_data17_MediumLH_FCTight.pdf}

  \caption{Efficiency of the different isolation working points for electrons
    from $Z\to ee$ events as a function of $\eta$, for
    electrons with $30\ \gev < \pt < 35\ \gev$ passing the Medium identification requirements. The efficiency for data (black)
    and MC (red) are shown as well as the scale factors in the bottom panel.
    The efficiencies have been measured using data recorded by the ATLAS experiment in 2017 at a centre-of-mass energy of $\sqrt{s} = 13$ \tev.
    Both statistical and systematic uncertainties are included in the upper plots while for the bottom plots the uncertainties are shown separately.}
  \label{fig:eleiso_eff_sf_vs_eta_bins}

\end{figure}

\begin{figure}[h]
  \centering

  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_logpt__eta_010_060_data17_MediumLH_Gradient.pdf}
  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_logpt__eta_010_060_data17_MediumLH_FCHighPtCaloOnly.pdf}

  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_logpt__eta_010_060_data17_MediumLH_FCLoose.pdf}
  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_logpt__eta_010_060_data17_MediumLH_FCTight.pdf}

  \caption{Efficiency of the different isolation working points for electrons
    from $Z\to ee$ events as a function of \pt, for
    electrons with $0.1 < \eta < 0.6$ passing the Medium identification requirements. The efficiency for data (black)
    and MC (red) are shown as well as the scale factors in the bottom panel.
    The efficiencies have been measured using data recorded by the ATLAS experiment in 2017 at a centre-of-mass energy of $\sqrt{s} = 13$ \tev.
    Both statistical and systematic uncertainties are included in the upper plots while for the bottom plots the uncertainties are shown separately.}
  \label{fig:eleiso_eff_sf_vs_pt_bins}

\end{figure}

\begin{figure}[h]
  \centering

  \includegraphics[width=0.49\textwidth]{figures/ele_iso/2d_sf_data17_mc16d_Medium_LH_Gradient.pdf}
  \includegraphics[width=0.49\textwidth]{figures/ele_iso/2d_sf_data17_mc16d_Medium_LH_FCHighPtCaloOnly.pdf}

  \includegraphics[width=0.49\textwidth]{figures/ele_iso/2d_sf_data17_mc16d_Medium_LH_FCLoose.pdf}
  \includegraphics[width=0.49\textwidth]{figures/ele_iso/2d_sf_data17_mc16d_Medium_LH_FCTight.pdf}

  \caption{Scale factor maps of the different isolation working points for electrons
    from $Z\to ee$ events as a function of \pt and $\eta$, for electrons passing the Medium identification requirements.
    The scale factors have been measured using data recorded by the ATLAS experiment in 2017 at a centre-of-mass energy of $\sqrt{s} = 13$ \tev.}
  \label{fig:eleiso_2d_maps}

\end{figure}


Figure \ref{fig:eleiso_inclusive} shows the efficiency as function of $\pt$,
$\eta$ and $\mu$ for the different isolation working points, for Medium
electrons inclusive in the other variables.
Additionally, Figure \ref{fig:eleiso_inclusive_mubins} shows the isolation
efficiency as a function of $\pt$ in two different $\mu$ bins: $\mu < 40$ and
$\mu > 40$.

\begin{figure}[h]
  \centering

  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_pt_data17_MediumLH_4WPs.pdf}
  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_eta_data17_MediumLH_4WPs.pdf}

  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_mu_data17_MediumLH_4WPs.pdf}

  \caption{Efficiency of the different isolation working points for electrons
    from $Z\to ee$ events as a function of the electron $\pt$ (top left),
    electron $\eta$ (top right) and the number of interactions per bunch crossing $\mu$ (bottom).
    The ratio between the data and Monte Carlo efficiency is shown in the bottom panel of each plot.
    The electrons are required to fulfill the Medium selection from the likelihood based electron
    identification. The efficiencies have been measured using data recorded by
    the ATLAS experiment in 2017 at a centre-of-mass energy of $\sqrt{s} = 13$ \tev. Both statistical and systematic uncertainties are included.}
  \label{fig:eleiso_inclusive}

\end{figure}


\begin{figure}[h]
  \centering

  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_logpt_data17_Medium_LH_4WPs_mu_bins.pdf}

  \caption{Efficiency of the different isolation working points for electrons
    from $Z\to ee$ events as a function of the electron $\pt$ in two different bins of the
    number of interactions per bunch crossing $\mu$.
    The electrons are required to fulfill the Medium selection from the likelihood based electron
    identification. The efficiencies have been measured using data recorded by
    the ATLAS experiment in 2017 at a centre-of-mass energy of $\sqrt{s} = 13$ \tev. Both statistical and systematic uncertainties are included.}
  \label{fig:eleiso_inclusive_mubins}

\end{figure}

Finally, Figure \ref{fig:eleiso_LMT} shows the efficiency as function of $\eta$, inclusive in the other variables,
of the different isolation working points for the different electron ID requirements.

\begin{figure}[h]
  \centering

  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_eta_data17_LMT_Gradient.pdf}
  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_eta_data17_LMT_FCHighPtCaloOnly.pdf}

  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_eta_data17_LMT_FCLoose.pdf}
  \includegraphics[width=0.49\textwidth]{figures/ele_iso/eff_sf_vs_eta_data17_LMT_FCTight.pdf}

  \caption{Efficiency of the different isolation working points for electrons from $Z\to ee$
    events passing the different identification selections, as a function of the electron $\eta$.
    The efficiencies have been measured using data recorded by the ATLAS experiment in 2017
    at a centre-of-mass energy of $\sqrt{s} = 13$ \tev. Both statistical and systematic uncertainties are included.}
  \label{fig:eleiso_LMT}

\end{figure}

\FloatBarrier

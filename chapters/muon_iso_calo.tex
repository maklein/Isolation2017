The historical computation of calorimetric isolation was simply summing the transverse energy of the calorimeter cells (from both the electromagnetic and hadronic calorimeters) within a cone centred around the lepton / photon direction. This \textbf{etcone} variable showed bad performance in run 1 for what concerns the pile-up resilience and data-Monte Carlo agreement. A significant improvement was brought by collecting the transverse energy of topological clusters instead of cells, thus effectively applying some noise-suppression algorithm to the cells. 

Topological clusters~\cite{ATL-LARG-PUB-2008-002} are clusters seeded by cells with an energy more than four times above the noise threshold of that cell. The clusters are then expanded by adding neighbouring cells (in the three spatial directions, across all calorimeter layers, excluding the cells from the Tile gap scintillators) that have an energy more than two times above the noise level. After the expansion around the cluster stops, a last layer of cells is added around the cluster. The topological clusters used in the isolation computation are not further calibrated: they remain at the electromagnetic scale. 

The muon direction is given by the weighted mean of the extrapolated positions of the muon track into the electromagnetic calorimeter, as discussed in \ref{app:muonext}. All positive energy topological clusters which barycentre falls within a cone centered around the lepton / photon direction are summed into what is called the raw \textbf{topoetcone} isolation $E_\textrm{T, raw}^\textrm{isol}$. This is illustrated in Fig.~\ref{fig:IsolSchema}.

\begin{figure}[htbp]
\centering
\includegraphics[width=8cm]{figures/SchemaIsolation.pdf}
\caption{Schema of the \textbf{topoetcone} variable: the grid represents the middle calorimeter cells in the $\eta$ and $\phi$ directions. The lepton / photon is located in the center of the yellow cone representing the isolation cone. All topological clusters, represented in red, which barycenter falls into the isolation cone are included in the isolation computation. The $5\times 7$ cells white rectangle corresponds to the subtracted cells in the default \emph{core subtraction} method.}
\label{fig:IsolSchema}
\end{figure}

\textbf{Core subtraction}

Topoclusters close enough to muon trajectories are removed from muon calo isolation. The current size of the core cone is $\Delta{}R = \sqrt{\left(\Delta\eta_{\mu\text, cluster}\right)^{2} + \left(\Delta\phi_{\mu\text, cluster}\right)^{2}} = 0.05$.

This is the result of the study of the $E_{\text{T}}^{\text{topocone}}$ background rejection (using $t\bar{t}$ events) and efficiency (using $Z\rightarrow\mu\mu$ events).
The samples for the study are MC, and the truth information is used to ensure the background or signal origin of the muon.
Efficiencies and rejections are calculated for various cuts on $E_{\text{T}}^{\text{topocone}}$ and the plot of the rejection as a function of the efficiency is presented in Fig.~\ref{fig:calo_opti}.
In this graph, the core sizes $\left\{0.02, 0.04, 0.05, 0.06, 0.08, 0.10\right\}$ were tested, and the size $0.05$ provides the best rejection for a given efficiency.

\begin{figure}
  \centering
  \includegraphics[width=0.75\textwidth]{calo_opti/c_Raw_Sumopti_Dongliang_ZT.pdf}
  \caption{The rejection in $t\bar{t}$ background events is plotted as a function of the efficiency in $Z\rightarrow\mu\mu$ signal events, for various core cone sizes. 
  Each point corresponds to one specific cut on $E_{\text{T}}^{\text{topocone}20}$. The optimal core cone size was found to be $0.05$.}
  \label{fig:calo_opti}
\end{figure}

\textbf{Pile-up and underlying event correction}

The pile-up and underlying event contribution to the isolation cone is estimated using the technique of the ambient energy density [ref ?]. The ambient energy of a given event is computed in the following way using the \textbf{FastJet}~\cite{Cacciari:2011ma} package:

\begin{itemize}
\item one reconstructs from positive energy topological clusters in the whole calorimeter acceptance (up to $\eta=5$) jets with an anti-kT algorithm of size $0.5$, with no \pt\ threshold. 
\item the area $A_\textrm{i}$ of each jet in the event is estimated from a Voronoi tessellation algorithm. 
\item the energy density of each jet is computed as $\rho_i=p_\textrm{T,i}/A_\textrm{i}$.
\item the median of the distribution of all the energy densities in the even, \rhomedian, is used as an estimator of the energy density of the event.  \end{itemize}

Fig.~\ref{fig:rhomedian} (left) shows the distribution of \rhomedian\ as a function of $\eta$ in $13~\tev$ $Z \to \ee$ Monte Carlo events. For this figure, \rhomedian\ is estimated from jets contained in a sliding window located between $\eta$ and $\eta+3$. The quantity \rhomedian\ is flat in the central $\eta$ region, and decreases in the forward $\eta$ region. For simplicity, \rhomedian\ is finally estimated on average in each of these regions: a central region with $|\eta|<1.5$ and a forward region $1.5<|\eta|<3$. The right plot of~\ref{fig:rhomedian} shows the distribution of \rhomedian\ in these two regions. 

\begin{figure}[htbp]
\centering
\includegraphics[width=8cm]{figures/rhomedianeta.pdf}
\includegraphics[width=8cm]{figures/rhomedian.pdf}
\caption{Left: distribution of \rhomedian\ as a function of $\eta$. Right: distribution of  $E_\textrm{T, pile-up}$ in the central and forward regions. Both figures are done for $13~\tev$ $Z \to \ee$ Monte Carlo events. }
\label{fig:rhomedian}
\end{figure}

The pile-up correction is then evaluated as:
\begin{equation}
E_\textrm{T, pile-up}(\eta) = \rhomedian(\eta) \times \left( \pi R^2  - A_\textrm{core}\right),
\end{equation}
where $R$ is the radius of the isolation cone and $A_\textrm{core}$ is the area of the core that was subtracted ($\pi \times 0.05^2$). 


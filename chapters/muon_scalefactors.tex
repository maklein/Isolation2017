In this section, the procedure for the measurement of the efficiencies and scale factors of the 
muon isolation working points is outlined. The selection of the muons is based on the Tag-And-Probe method, 
and the selection cuts are the same as in \ref{ssec:datamc_muons}. The only change is on the 
background subtraction, which is described in the following section. Note that all the results presented here correspond to Moriond 2017 recommendations.

\input{chapters/muon_scalefactors_backgroundsubtraction}

\paragraph{Measurements of the efficiencies and scale factors}

The efficiencies are calculated as the ratio of probe muons passing the isolation cuts associated to the working point, divided 
by the total number of probes of the samples. In order to correct simulation, scale factors are derived, defined as the ratios of the efficiencies 
in data over the same quantity assessed in MC. Systematics are assigned to the measured scale factors and are described in Tab.~\ref{tab:muons_systematics}.
All the systematics are added in quadrature and are symmetrised using the highest up or down variation. The statistical uncertainty 
from MC is treated as a systematic. Note that additional $\eta$ systematic is equal to $0.2\%$ over all $p_{\text{T}}$ bins, and is only applied to 
the scale factors derived in 1D.

\begin{table}
	\centering
	\caption{Systematic variations for the muon isolation scale factor measurements.\label{tab:muons_systematics}}
	\begin{tabular}{c c}
		\hline
		\hline
		Definition of systematic & Range of variation \\
		\hline
		$Z$ mass & ($86$ - $\SI{96}{\giga\electronvolt}$) or ($71$ - $\SI{111}{\giga\electronvolt}$) \\
		Tag isolation & Maximum variation among all isolation working points \\
		Probe muon identification & \emph{Loose} or \emph{Tight} \\
		$\Delta{}R_{\mu\mu}$ & $\Delta{}R_{\mu\mu} > 0.2$ or $\Delta{}R_{\mu\mu} > 0.5$ \\
		$\Delta{}R_{\mu{}j}$ & $\Delta{}R_{\mu{}j} > 0.3$ or $\Delta{}R_{\mu{}j} > 0.5$ \\
		\hline
		Background subtraction & Subtraction using the up and down variations of the corrected efficiencies \\
		\hline
		\hline
	\end{tabular}
\end{table}

Since the scale factors have a very strong dependence on the presence of a close-by jet, additional systematics are added in the case of $\Delta{}R_{\mu{}j} < 0.4$.
In this region, the central value of the scale factor is the same as for the $\Delta{}R_{\mu{}j} > 0.4$ case, but an additional systematic 
is applied to make sure that the corrected MC with its uncertainties covers the data points after correction. 

The results are shown in Fig.~\ref{fig:inclusiveiso-eff} and Fig.~\ref{fig:inclusiveiso-sys} for all the working points. In the top 
panels, the efficiencies of data (combining 2015 and 2016) and MC are plotted, and the corresponding scale factors are 
represented in the bottom panel. Overall, there is a good agreement between data and MC. The additional $\eta$ systematic is flat over $p_{\text{T}}$ 
and compensates the $\eta$ dependence of the scale factors. 

\begin{figure}[h!]
	\centering
	\subfloat[\emph{FCLoose}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/eff_2018/AllEffPlots_FancyIsoBkgSub_extended3GeV_18_Part1.pdf}}
	\subfloat[\emph{FCLoose\_FixedRad}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/eff_2018/AllEffPlots_FancyIsoBkgSub_extended3GeV_18_Part2.pdf}}
	\subfloat[\emph{FCTight}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/eff_2018/AllEffPlots_FancyIsoBkgSub_extended3GeV_18_Part3.pdf}}\\
	\subfloat[\emph{FCTight\_FixedRad}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/eff_2018/AllEffPlots_FancyIsoBkgSub_extended3GeV_18_Part4.pdf}}
	\subfloat[\emph{FCTightTrackOnly}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/eff_2018/AllEffPlots_FancyIsoBkgSub_extended3GeV_18_Part5.pdf}}
	\subfloat[\emph{FCTightTrackOnly\_FixedRad}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/eff_2018/AllEffPlots_FancyIsoBkgSub_extended3GeV_18_Part6.pdf}}\\
	\subfloat[\emph{FixedCutHighPtTrackOnly}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/eff_2018/AllEffPlots_FancyIsoBkgSub_extended3GeV_18_Part7.pdf}}
	\subfloat[\emph{FixedCutPflowLoose}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/eff_2018/AllEffPlots_FancyIsoBkgSub_extended3GeV_18_Part8.pdf}}
	\subfloat[\emph{FixedCutPflowTight}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/eff_2018/AllEffPlots_FancyIsoBkgSub_extended3GeV_18_Part9.pdf}}
	\caption{Muon isolation efficiencies and scale factors for 
          (a) \emph{FCLoose}, 
          (b) \emph{FCLoose\_FixedRad}, 
          (c) \emph{FCTight}, 
          (d) \emph{FCTight\_FixedRad}, 
          (e) \emph{FCTightTrackOnly}, 
          (f) \emph{FCTightTrackOnly\_FixedRad}, 
          (g) \emph{FixedCutHighPtTrackOnly}, 
          (h) \emph{FixedCutPflowLoose}, 
          (i) \emph{FixedCutPflowTight},
        for data 2018}
        \label{fig:inclusiveiso-eff}
\end{figure}

\begin{figure}[h!]
	\centering
	\subfloat[\emph{FCLoose}]{\includegraphics[height=4.85cm, width=5cm]{figures/inclusiveiso/sys_2018/AllSysPlots_FancyIsoBkgSub_extended3GeV_18_Part2.pdf}}
	\subfloat[\emph{FCLoose\_FixedRad}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/sys_2018/AllSysPlots_FancyIsoBkgSub_extended3GeV_18_Part2.pdf}}
	\subfloat[\emph{FCTight}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/sys_2018/AllSysPlots_FancyIsoBkgSub_extended3GeV_18_Part3.pdf}}\\
	\subfloat[\emph{FCTight\_FixedRad}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/sys_2018/AllSysPlots_FancyIsoBkgSub_extended3GeV_18_Part4.pdf}}
	\subfloat[\emph{FCTightTrackOnly}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/sys_2018/AllSysPlots_FancyIsoBkgSub_extended3GeV_18_Part5.pdf}}
	\subfloat[\emph{FCTightTrackOnly\_FixedRad}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/sys_2018/AllSysPlots_FancyIsoBkgSub_extended3GeV_18_Part6.pdf}}\\
	\subfloat[\emph{FixedCutHighPtTrackOnly}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/sys_2018/AllSysPlots_FancyIsoBkgSub_extended3GeV_18_Part7.pdf}}
	\subfloat[\emph{FixedCutPflowLoose}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/sys_2018/AllSysPlots_FancyIsoBkgSub_extended3GeV_18_Part8.pdf}}
	\subfloat[\emph{FixedCutPflowTight}]{\includegraphics[scale = 0.25]{figures/inclusiveiso/sys_2018/AllSysPlots_FancyIsoBkgSub_extended3GeV_18_Part9.pdf}}
	\caption{Muon systematics breakdown for 
          (a) \emph{FCLoose}, 
          (b) \emph{FCLoose\_FixedRad}, 
          (c) \emph{FCTight}, 
          (d) \emph{FCTight\_FixedRad}, 
          (e) \emph{FCTightTrackOnly}, 
          (f) \emph{FCTightTrackOnly\_FixedRad}, 
          (g) \emph{FixedCutHighPtTrackOnly}, 
          (h) \emph{FixedCutPflowLoose}, 
          (i) \emph{FixedCutPflowTight},
        for data 2018}
        \label{fig:inclusiveiso-sys}
\end{figure}

Additional figures are given in App.~\ref{app:app_muons_WPs}, which present the importance of each systematic contributions in $p_{\text{T}}$ bins, as well as the 
plots showing the validation closure tests of the scale factors (to check the MC corrected using the produced scale factors covers the data points).

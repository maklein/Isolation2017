% author: Miha Zgubic (miha.zgubic@cern.ch)

\textbf{Efficiencies and scale factors for the muons, background}

\label{sec:bkg_subtraction_muons}

In order to provide scale factors (SF) for muon isolation the isolation efficiency of $Z\rightarrow\mu\mu$ events in data must be measured. However, at low probe muon $\pt$, the isolation efficiency in data is contaminated by the presence of QCD and EWK\footnote{Comprises of Wminusmunu, Wplusmunu, Ztautau, WWlvlv, WZlvll, ZZllll, ZZvvll, WZqqll, ZZqqll, and ttbar MC samples generated with Powheg + Pythia 8.} events, so the measured efficiency is
\begin{equation}
\label{eq:edata}
\edata = \fqcd \eqcd + \fewk \eewk + (1-\fqcd-\fewk) \eZ, 
\end{equation}
where $\fqcd$ and $\fewk$ are the fractions of QCD and EWK backgrounds in data, $\eqcd$ and $\eewk$ are the efficiencies of QCD and EWK backgrounds in data, and $\eZ$ is the efficiency of $Z\rightarrow\mu\mu$ events in data that we want to extract. $\eZ$ can be expressed from Eq. (\ref{eq:edata}) to obtain
\begin{equation}
\label{eq:correction}
\eZ = \frac{\edata-\fqcd \eqcd - \fewk \eewk}{1-\fqcd-\fewk} = \frac{\edata + \Su}{\Sc},
\end{equation}
where subtraction (Su) and scaling (Sc) terms have been defined as $\Su = -\fqcd \eqcd - \fewk \eewk$ and $\Sc = 1-\fqcd-\fewk$. This subsection describes how these two terms are computed for each isolation working point (WP) as a function of $\pt$, as well as the procedure to estimate systematic uncertainties and a validation of the method.

In order to compute $\eZ$ the values of the efficiencies and fractions on the RHS of Eq. (\ref{eq:correction}) are required. Efficiencies can be computed per WP as a function of $\pt$ from MC for EWK processes and from data with two same sign muons (SS data) for QCD. Fractions of QCD and EWK are determined by a template fit to the dimuon invariant mass spectrum in the opposite sign (OS) data, in the bins of probe muon $\pt$. The background fraction rapidly decreases above the probe muon $\pt$ of 25 GeV and the fit fails to converge, hence a rough estimate of SS/OS is used in that region. It should be noted that the background subtraction effects in that region are minimal.

It is important to see that the fit result, performed on the probe muon sample, is independent of the isolation WP, but does of course depend on the input shape of the templates.

\underline{Shape templates}

A template fraction fit is performed using two templates. The first template models the QCD shape and is taken as SS data. Note that it is assumed here that the QCD shape is the same in SS data as it is in OS data, which is a source of systematic uncertainty and is discussed later. The second template is a combination of EWK backgrounds and the signal sample $Z\rightarrow\mu\mu$, and is constructed from MC.

There is also a third template, EWK backgrounds without the $Z\rightarrow\mu\mu$ sample. This is not used in the fitting, but to compute the fractions under the $Z$ peak as described in the next paragraph. 

\underline{Computing the fractions}
\label{sec:fractions}

While the efficiencies and the fractions required are those under the $Z$ peak ($81 -101 \text{ GeV}$), the fit is performed along a wider range ($61 -121 \text{ GeV}$) to be able to discriminate between the shapes of the two templates, EWK with the $Z$ peak and a smooth QCD.

In order to obtain the number of QCD events under the $Z$ peak the fitted QCD template (fit changes the shape of the template within uncertainties) is integrated between 81 and 101 GeV. Number of QCD events under the $Z$ peak is then divided by the integral of the OS data in the same range to obtain the fraction of QCD background under the $Z$ peak, $\fqcd$.

The EWK background fraction, $\fewk$, is computed in a slightly more complicated way. Template fit result is obtained by fitting the ``EWK background + $Z\rightarrow\mu\mu$'' template, which is then used to scale the ``EWK background only'' template. Then  ``EWK background only'' template is integrated in the $81 -101 \text{ GeV}$ range to obtain the number of EWK background events under the $Z$ peak. However, since the EWK background MC samples suffer from low statistics in the bins with probe muon $\pt$ below 10 GeV, the integral is a subject to fluctuations. To suppress these fluctuations, the scaled ``EWK background only'' template is fitted by a falling exponential function in the bins below 10 GeV, which is then integrated between $81 -101 \text{ GeV}$ to obtain a more stable estimate of the number of EWK events under the $Z$ peak. Finally this number is divided by the integral of the OS data between 81 and 101 GeV to obtain the fraction of EWK background under the $Z$ peak, $\fewk$.

An example of a fit is shown in Fig. \ref{fig:fit} below.
\begin{figure}[h]
\centering
\includegraphics[width=8cm]{figures/muon_bkg_sub/ZFitter_SCQCD_slice1.pdf}
\caption{Template fit of the muon invariant mass distribution in the $4 < \pt < 5$ GeV bin. The QCD template is shown in blue, EWK background template is shown in green, $Z\rightarrow\mu\mu$ is shown in white. $\chi ^2$ divided by the number of degrees of freedom is also shown on the plot for both fraction fit and the exponential fit.}
\label{fig:fit}
\end{figure}


\underline{Systematic uncertainties}
\label{sec:syst}

There is a systematic uncertainty associated with the choice of QCD template that is estimated from SS data. Additional background enriched regions are introduced in order to estimate this uncertainty and are shown as a sketch in Fig. \ref{fig:abcd} below.\\
\begin{figure}[h]
\centering
\includegraphics[width=.7\linewidth]{figures/muon_bkg_sub/ABCD_sketch.pdf}
\caption{Sketch of the regions used in estimating systematic uncertainties. $d_0$ significance on the $x$-axis is defined as $\vert \frac{d_0}{\sigma_{d_0}} \vert$, where $d_0$ is the impact parameter and $\sigma_{d_0}$ is the error on the impact parameter. The requirement is applied to both the tag and probe muons. Large buffer region is used to get rid of the $Z\rightarrow\mu\mu$ events in region D. The role of function $f$ is explained in the main text.}
\label{fig:abcd}
\end{figure}
\\
Sketch in Fig. \ref{fig:abcd} shows the four different QCD templates, while example shapes can be seen in Fig. \ref{fig:templates}. They are taken as data in regions B, C, and D, while the template in region A is hidden under the $Z$ peak and is estimated from region B, using a transfer function $f$ from SS to OS data which is estimated from the ratio of shapes in regions C and D. QCD template in region A is therefore computed as
\begin{equation}
\label{eq:AQCD}
\text{shape}_A = f \times \text{shape}_B = \frac{\text{shape}_C}{\text{shape}_D} \times \text{shape}_B.
\end{equation}
To estimate the systematics, subtraction and scaling terms from Eq. (\ref{eq:correction}) are computed for each choice of QCD template. While it would be natural to take the estimated template in region A as the nominal correction, low statistics in regions C and D result in a fluctuating estimate for QCD template in region A, producing less smooth correction terms. Instead, region B has the highest statistics and produced the most stable results, and is hence taken as the nominal correction.

Systematics are computed directly on the background subtracted efficiency, $\eZ$ in Eq. (\ref{eq:correction}). Denoting $\epsilon_X$ as the $\eZ$ computed with the QCD template taken from region $X$, the estimate of the systematics can be written as
\begin{equation}
\label{eq:syst}
\text{syst. unc.} = \max_{X \in \{ A, C, D\}} \{ \vert \epsilon_B - \epsilon_X\vert \}
\end{equation}
and is computed per $\pt$ bin, per WP.
\begin{figure}[h!]
\centering
\includegraphics[width=.7\linewidth]{figures/muon_bkg_sub/mll_bin1.pdf}
\caption{Top panel shows the QCD templates from regions A, B, C, and D. Bottom panel shows the function $f$, obtained which clearly suffers from lack of statistics in regions C and D.}
\label{fig:templates}
\end{figure}
Result for  \verb|FixedCutLoose| isolation WP is shown in Fig. \ref{fig:result}.
\begin{figure}[h]
\centering
\includegraphics[width=.7\linewidth]{figures/muon_bkg_sub/IsoFixedCutLoose_Efficiency_syst_NomMll.pdf}
\caption{MC $Z\rightarrow\mu\mu$ efficiencies shown in red, $\edata$ in Eq. (\ref{eq:correction}) shown in black, and the background subtracted efficiencies $\eZ$ shown in blue. Error bars represent statistical uncertainties, while the shaded blue bands represent systematic uncertainties estimated by the method described above.} 
\label{fig:result}
\end{figure}

\underline{Validation}

The method is validated by constructing pseudo data with known efficiencies, so that it is known to what values the background subtraction should converge to. OS pseudo data is constructed by combining all EWK MC samples, including $Z\rightarrow\mu\mu$, and a QCD template from region B. The background subtracted efficiency (green) should then match the $Z\rightarrow\mu\mu$ MC efficiency (red), which is indeed what is observed in Fig. \ref{fig:pseudodata} below.
\begin{figure}[h]
\centering
\includegraphics[width=.7\linewidth]{figures/muon_bkg_sub/IsoFixedCutLoose_Efficiency_B_pseudodata.pdf}
\caption{Similarly to Fig. \ref{fig:result}, MC $Z\rightarrow\mu\mu$ efficiencies are shown in red and $\edata$, which is now pseudo data, is shown in black. The background subtracted efficiency is shown in greeen, and it can be seen it closes on the expected MC efficiency within systematic uncertainty.}
\label{fig:pseudodata}
\end{figure}

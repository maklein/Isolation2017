Photon isolation is studied in two main signatures: radiative $Z$ decays (valid for $10 < \ET < 100\ \gev$~\footnote{In this note, both \pt and \ET notations are used to label the same quantity, the transverse energy $E/\cosh\eta$.}) and single photons (that is used in the $ 25 < \ET \sim 1\ \tev$ range). In the latter case, different methods are used for track isolation and for calorimeter isolation efficiency measurements, as detailed later in this section. When the measurement is done for the track-only (calo-only) the requirements on the calorimeter (track) based isolation are applied at pre-selection level. The photon isolation scale factors have been computed separately for 2015 combined with 2016, and 2017 data sets. Results using the radiative $Z$ decays are obtained also with 2018 data and summarized later in this section. 

The photon isolation variables used for the photon isolation efficiency and scale factor measurements are: 
\begin{itemize}
    \item Track based isolation variable:
	\begin{itemize} 
    \item \ptcs
	\end{itemize}
	\item Calorimeter based isolation variables:
	\begin{itemize}
    \item \etcs and \etcb
	\end{itemize}
\end{itemize}
For the calorimeter based isolation variables a discrepancy between the peak positions of their distributions in Data and in MC simulations was found since Run 1. This disagreement persists when considering the variables subject to cuts in the calorimetric isolation working point definitions: $\etcs < 0.065 \times \pt$ and $\etcb < 0.022 \times \pt + 2.45$ \gev. As a result, the photon isolation efficiencies in data and MC simulations disagree significantly, leading to large scale factors. These differences can be greatly mitigated by applying a so called ``data-driven shift’’ to the photon calorimetric isolation variable in Monte Carlo. In order to compute the proper data-driven shift, Asymmetric Crystal Ball fits to Tight ID photons in Monte Carlo and real photons in data are performed. Further, the difference in the fitted peak values from the data and MC Asymmetric Crystal Ball fits are taken as the shift values. These shift values are added to the calorimetric isolation value of photons in MC simulations for the corresponding \ET, $|\eta|$, conversion status, data-taking period and simulation-type bin. No systematic errors are currently applied to the shift values. For all the results shown in this note, the calorimeter based photon isolation variables in Monte Carlo are corrected with these data-driven shifts. 

The fitting procedure and background subtraction methodology used to obtain the data-driven shifts is also used to measure the photon calorimetric isolation efficiencies for single photon events. This procedure is detailed in section~\ref{sec:ph_caloSnPh}. The (data-driven) shifts are computed in bins of photon $\eta$ and \ET, and conversion status, for \etcs and \etcb; the measurement is performed separately for 2015 combined with 2016, and 2017 data sets. In addition, the peak position of the calorimetric isolation distributions in photons from fast simulation was found to deviate greatly from that of photons from full simulation, and, as a result, the data-driven shifts for photons from either fast or full simulation are also calculated separately. Figure~\ref{fig:photoncalo_DD_shifts_pythia} illustrates the data-driven shifts for \etcb obtained with 2017 Data and Pythia 8 (full-sim) MC, for \texttt{FixedCutTight} working point. The shifts are usually smaller than 20\%. Similar data-driven shifts for photons in 2017 Data and Pythia 8 fast-sim MC are shown in Figure \ref{fig:photoncalo_DD_shifts_pythiaAF2} for the endcap region. The shifts derived for fast-sim are observed to be much larger than those for full-sim, especially for larger pseudo-rapidity photons. 

 
\begin{figure}
\centering
\subfloat[\emph{Converted photons}]{\includegraphics[width=0.45\textwidth]{ph_iso-caloSnPh/DD_shifts/{DDshift_PlotForNote_topoETcone40_conv_2017}}}
\subfloat[\emph{Unconverted photons}]{\includegraphics[width=0.45\textwidth]{ph_iso-caloSnPh/DD_shifts/{DDshift_PlotForNote_topoETcone40_unconv_2017}}}
\caption{The data-driven shifts for \etcb obtained with 2017 Data and Pythia 8 (full-sim) MC, for \texttt{FixedCutTight} working point for a) converted and b) unconverted photons. The results are shown as a function of photon \ET, in the $|\eta|<0.6$ bin. Only the error associated to the fit parameters is shown.}
\label{fig:photoncalo_DD_shifts_pythia}
\end{figure}

\begin{figure}
\centering
\subfloat[\emph{Converted photons}]{\includegraphics[width=0.45\textwidth]{ph_iso-caloSnPh/DD_shifts/{DDshift_AF2_PlotForNote_topoETcone40_conv_2017}}}
\subfloat[\emph{Unconverted photons}]{\includegraphics[width=0.45\textwidth]{ph_iso-caloSnPh/DD_shifts/{DDshift_AF2_PlotForNote_topoETcone40_unconv_2017}}}
\caption{The data-driven shifts for \etcb obtained with 2017 Data and Pythia 8 (fast-sim) MC, for \texttt{FixedCutTight} working point for a) converted and b) unconverted photons. The results are shown as a function of photon \ET, in the endcap ($1.81<|\eta|<2.37$) bin. Only the error associated to the fit parameters is shown.}
\label{fig:photoncalo_DD_shifts_pythiaAF2}
\end{figure}



\subsubsection{Photon isolation in radiative Z decays}

Final State Radiation (FSR) photons from $Z$ decays can be used to probe low \pt\ photons in a very clean environment. The following criteria are used for the selection of the photon control sample from $Z\to\mu\mu\gamma$ and $Z\to ee\gamma$ decays and define the signal region: electrons and muons from $Z$ boson are reconstructed requiring that the leptons have a \ET\ greater than 10 \gev, the absolute pseudorapidity should be $|\eta|<1.37$ or $1.52<|\eta|<2.47$ for electrons, and $|\eta| < 2.5$ for muons. The leptons are required to fulfill the \textit{Loose}~\footnote{This working point is deprecated for full Run II analyses.} isolation requirement (yielding a 99$\%$ efficiency)~\cite{PERF-2017-01,PERF-2015-10} and to pass a medium identification criterion. 

The photons are selected in the following way:
\begin{itemize}
    \item transverse energy: $\ET>10$ \gev;
    \item pseudorapidity: $|\eta| < 1.37$ or $1.52 <|\eta|<2.37$;
    \item tight ID requirement;
    \item $\Delta R_{min}(\ell,\gamma)$ > 0.4 ($\ell = e$ or $\mu$)
\end{itemize}

Electron and muon triggers were used in the measurements: 
\begin{itemize}
    \item muon triggers: \texttt{HLT\_mu26\_imedium}, \texttt{HLT\_mu26\_ivarmedium\_OR\_HLT\_mu50}, \texttt{HLT\_mu26\_ivarmedium}, \texttt{HLT\_mu50}, \texttt{HLT\_mu50\_0eta105\_msonly}, \texttt{HLT\_2mu14}, \texttt{HLT\_mu22\_mu8noL1};
    \item electron triggers: \texttt{HLT\_e26\_lhtight\_nod0\_ivarloose}, \texttt{HLT\_e60\_lhmedium\_nod0},\\ \texttt{HLT\_e140\_lhloose\_nod0}, \texttt{HLT\_2e24\_lhvloose\_nod0}
\end{itemize}
Further, only the FSR photons are considered and selected by requiring $80<m_{\ell\ell\gamma} < 100\ \gev$ and $40<m_{\ell\ell} <83\ \gev$, since photon candidates from ISR are largely affected by the $Z$+jets background, where a jet fakes a photon (the cross section for $Z$+jets is about three order of magnitudes higher than for $Z \to \ell\ell\gamma$, and a non-negligible fraction of jets contains high-momentum $\pi^0$'s decaying to a collimated photon pairs). The reached purity is $> 95\%$ when the photon $\ET$ is between 10-\SI{20}{GeV} and $>99\%$ otherwise. Given the negligible background contamination above \SI{20}{GeV} no background subtraction is performed (see~\cite{PERF-2017-02}). The nominal MC signal samples were generated with SHERPA; alternative \textsc{Powheg}+\textsc{Pythia} MC simulations for the signal events are used for cross-checks and to assign the systematic uncertainties.

\textbf{Track isolation distribution}\\
Track based isolation \ptcs/\pt distributions are shown in Figure~\ref{fig:radZ_track_iso} in data and Monte Carlo events for unconverted and converted photons with \ET > 20 \gev. Fine agreement is seen in case of converted photons, although a disagreement is seen for unconverted photons. Figure~\ref{fig:radZ_track_truth_iso} shows \ptcs/\pt in Monte-Carlo for reconstructed unconverted and converted photons, with overlaid truth unconverted and converted photons for each case. The differences seen in~\ref{fig:radZ_track_iso} are arising due to failed reconstruction of conversion tracks.

\begin{figure}[htbp]
\centering
  \includegraphics[width=7cm]{ph_iso_var/{ptcone20_pt_Zll_data17_mc_unc_pt20-1000_iddep}}
  \includegraphics[width=7cm]{ph_iso_var/{ptcone20_pt_Zll_data17_mc_conv_pt20-1000_iddep}}
  \caption{$\ptcs/\pT$ distributions for data (dots), for signal MC Sherpa (red line) and Pythia (green line) for unconverted (left) and converted (right) photons, in signal photon region for \ET > 20 GeV. The isolation working points \texttt{FixedCutLoose} and \texttt{FixedCutTight} require this quantity to be smaller than 0.05. No background subtraction in data is performed. Only statistical uncertainties are shown.}
\label{fig:radZ_track_iso}
\end{figure}

\begin{figure}[htbp]
\centering
  \includegraphics[width=7cm]{ph_iso_var/{ptcone20_pt_truth_Zll_data17_mc_unc_pt20-100_iddep}}
  \includegraphics[width=7cm]{ph_iso_var/{ptcone20_pt_truth_Zll_data17_mc_conv_pt20-100_iddep}}
  \caption{$\ptcs/\pT$ distributions for signal MC Sherpa, reconstructed photons (black line), for true converted (red line) and true unconverted (green line), in case of reconstructed unconverted (left) and reconstructed converted (right) photons, in signal photon region for \ET > 20 GeV. Only statistical uncertainties are shown.}
\label{fig:radZ_track_truth_iso}
\end{figure}


\textbf{Calorimeter isolation distribution}\\
% Two calorimeter based isolation variables are used for the studies: \etcs and \etcb. 
Figures~\ref{fig:radZ_topoetcone40_iso} and~\ref{fig:radZ_topoetcone20_iso} show the distributions of $\etcb - 0.022 \times \pt$ and $\etcs - 0.065 \times \pt$, respectively, in data and Monte Carlo for unconverted and converted photons, for \ET > 20 \gev. A good agreement between data and simulation is observed.

\begin{figure}[htbp]
\centering
  \includegraphics[width=7cm]{ph_iso_var/{topoetcone40_Zll_data17_mc_unc_nolog_pt20-1000_iddep}}
  \includegraphics[width=7cm]{ph_iso_var/{topoetcone40_Zll_data17_mc_conv_nolog_pt20-1000_iddep}}
  \caption{$\etcb - 0.022 \times \pt$ distributions for data (dots), for signal MC Sherpa (red line) and Pythia (green line) for unconverted (left) and converted (right) photons, in signal photon region for \ET > 20 \gev. Only statistical uncertainties are shown. The \texttt{FixedCutTight} and \texttt{FixedCutTightCaloOnly} working points require this quantity to be smaller than 2.45 \gev.}
\label{fig:radZ_topoetcone40_iso}
\end{figure}

\begin{figure}[htbp]
\centering
  \includegraphics[width=7cm]{ph_iso_var/{topoetcone20_Zll_data17_mc_unc_nolog_pt20-1000_iddep}}
  \includegraphics[width=7cm]{ph_iso_var/{topoetcone20_Zll_data17_mc_conv_nolog_pt20-1000_iddep}}
  \caption{$\etcs - 0.065 \times \pt$ distributions for data (dots), for signal MC Sherpa (red line) and Pythia (green line) for unconverted (left) and converted (right) photons, in signal photon region for \ET > 20 \gev. Only statistical uncertainties are shown. The \texttt{FixedCutLoose} working point requires this quantity to be smaller than 0.}
\label{fig:radZ_topoetcone20_iso}
\end{figure}


\subsubsection{Photons isolation in single photons}
\label{sec:ph_calotrackiso_sgPhotons}

Single photon samples suffer from large background contamination from jets. The jet background can be estimated by obtaining a background template from a background-enriched control region (where some of the identification cuts are reversed), and using this template in the signal region of tightly identified photons. Details of the method can be found in Section~\ref{sec:ph_trackSnPh}.

For these studies, the pre-selection requirements are:
\begin{itemize}
	\item single photon triggers: \texttt{HLT\_gXX\_loose} (\texttt{XX}=10, 60, 70, 80, 100, 120, 140), \texttt{HLT\_g15\_loose\_L1EM7}, \texttt{HLT\_g20\_loose\_L1EM12}, \texttt{HLT\_gXX\_loose\_L1EM15} (\texttt{XX}=25, 35, 40, 45, 50). Some of these triggers are prescaled;
    \item transverse energy: $\ET>25$ GeV;
    \item pseudorapidity: $|\eta| < 1.37$ or $1.52 <|\eta|<2.37$;
    \item loose ID requirement;    
    \item offline photon quality cuts; 
    \item overlap removal: $\Delta R_{min}(\gamma,X)$ > 0.4 ($ X = $ lepton or jet).
\end{itemize}
For the overlap removal, the jets are from the \texttt{AntiKt4EMTopoJets} collection, with $|\eta|~<~4.4$ and $\pt~>~\SI{25}{GeV}$. The bad jets are removed (\texttt{LooseBad} criteria applied) as well as the pile-up jets (recommended JVT cut applied, no JVF cut). The electrons are required to pass the object quality cuts and to not be in the "bad calorimeter region" (recommend passHVCut applied), have $|\eta|~<~2.47$ and $\pt~<~\SI{10}{GeV}$. Standard requirements on the longitudinal impact parameter ($|z_0 * \operatorname{sin}\theta|~<~\SI{0.5}{mm}$) and transverse impact parameter ($|d_0/\sigma(d_0)|~<~5$) are also applied. They should pass the Medium ID and Loose isolation. All muons are required to pass the Medium ID, GradientLoose isolation and have $|\eta|~<~2.7$ and $\pt~>~\SI{10}{GeV}$. As for electrons, requirements on the impact parameters are also applied ($|z_0 * \operatorname{sin}\theta|~<~\SI{0.5}{mm}$ and $|d_0/\sigma(d_0)|~<~3$). As recommended by the Muon CP group, the SagittaCorr2015\_2016 is considered. 


In addition, photons from Monte Carlo simulation are required to have a reconstructed transverse momentum which is within the range of $0.98 p_{\mathrm{T,lower}}$ to $1.02 p_{\mathrm{T,upper}}$, where $p_{\mathrm{T,lower}}$ ($p_{\mathrm{T,upper}}$) refers to the lower (upper) bound of the truth transverse momentum slice of the source sample. Truth matching cuts are also applied to Monte Carlo simulated photons in order to select only true prompt photons. 



\textbf{Track isolation distribution}\\
Figure~\ref{fig:ptcone_distribution} shows the distributions of \ptcs/\pt for signal data and Monte Carlo events for unconverted and converted photons with $|\eta|<0.60$ and $25<\ET<30~\gev$, after the pre-selection cuts are applied as well as a tight cut ($\etcb < 0.022 \times \pt + 2.45$ \gev) on the calorimeter isolation. The difficulty for the track isolation is that the distribution is not continuous (a large peak at 0, followed by a gap and then a small tail) thus it cannot be fitted with an analytic function.

\begin{figure}[htbp]
\centering
  \includegraphics[width=0.49\textwidth]{figures/ph_iso_var/UnConverted.pdf}
  \includegraphics[width=0.49\textwidth]{figures/ph_iso_var/Converted.pdf}
  \caption{Distributions of \ptcs/\pt for data (dots) and Monte Carlo (blue histogram) single photon events for unconverted (left) and converted (right) photons with $|\eta|<0.6$ and $25<\pt<30~\gev$, the tight calorimeter isolation cut ($\etcb < 0.022 \times \pt + 2.45$ \gev) is applied at pre-selection level.}
\label{fig:ptcone_distribution}
\end{figure}


\textbf{Calorimeter isolation distribution}\\
The distributions for calorimeter isolation from single photon data are obtained with a dedicated iterative fit described in Section~\ref{sec:ph_caloSnPh}, where the resulting distributions of the isolation variables are shown. The photon preselection applied for the track isolation studies is also used here. 
Figure~\ref{fig:topoetcone20_40_distribution} shows the result of a fit to the $\etcs - 0.065 \times \ET$ and $\etcb - 0.022 \times \ET$ distributions for signal data with signal and background templates for unconverted photons in one example $\ET/|\eta|$ bin ($40<\ET<45$ \gev, $|\eta|<0.60$).
\begin{figure}[htbp]
\centering
  \includegraphics[width=0.49\textwidth]{ph_iso_var/{tightDataBkgCB2SigACB2_topoETcone20pt_2017_unconv_pT_40_45_absEta_0_60_Loose4_NominalLeakage}}
  \includegraphics[width=0.49\textwidth]{ph_iso_var/{tightDataBkgCB2SigACB2_topoETcone40pt_2017_unconv_pT_40_45_absEta_0_60_Loose4_NominalLeakage}}
  \caption{Fit to $\etcs - 0.065 \times \pt$ (left) and $\etcb - 0.022 \times \pt$ (right) distributions for unconverted photons from single photon events, $|\eta|<0.6$ and $40<\ET<45~\gev$; the track isolation cut is applied at pre-selection level. The black points show the data points, the red line shows the fitted background (fake photon) shape, and the blue line shows the total signal (real photons) plus background fitted shape.}
\label{fig:topoetcone20_40_distribution}
\end{figure}

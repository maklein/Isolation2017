% ---------------
% Track Isolation
% ---------------

\textbf{Track isolation}

The track isolation variable \emph{ptcone} is computed by summing the transverse momentum of some selected
tracks within a cone centred around the electron track direction as described \cite{IsoSupportingNote}.
Since in busy environments other objects can end up very close to the electron direction, a more useful
variable with variable-cone size, called \emph{ptvarcone}, is defined. For this
variable, the cone size gets smaller for larger transverse momentum of the electron:
%
\begin{equation}
  \Delta R = \min \left( \frac{k_{\mathrm{T}}}{\pt}, R_{\mathrm{max}}\right)
\end{equation}
%
where $k_{\mathrm{T}}$ is a constant fixed to 10 GeV and $R_{\mathrm{max}}$ is the maximum cone size (typically 0.2).

There are two variants of the \emph{ptvarcone} variable requiring different selections for the tracks used to build them (see also Table~\ref{tab:ele_track_selection}):

\begin{itemize}\itemsep0.2cm\parskip0.2cm
\item \texttt{ptvarcone20} (\ptvcs) : \emph{Loose} track quality cut, $\pt > 1\ \gev$, $|\Delta z_0|\sin\theta < 3$ mm;
\item \texttt{ptvarcone20}\_\texttt{TightTTVA}\_\texttt{pt1000} (\ptvcsttva): \emph{Loose} track quality cut, $\pt > 1\ \gev$, \emph{Loose} vertex association.
\end{itemize}

In the two cases the tracks are required to pass the \emph{Loose} track quality cut and have $\pt > 1\ \gev$ but differ in the track-to-vertex association.
The \emph{Loose} vertex association working point is described below:

\begin{itemize}
\item  the track was used in the vertex fit or
\item  the track was not used in any vertex fit but satisfies $|\Delta z_0|\sin\theta < 3$ mm.
\end{itemize}


\begin{table}[!ht]
  \centering

  \begin{tabular}{|c|c|c|}
    \hline
    Requirement                 & \ptvcs & \ptvcsttva \\
    \hline \hline
    $\pt$                       & \multicolumn{2}{c|}{> 1 \gev} \\
    $|\eta|$                    & \multicolumn{2}{c|}{< 2.5}    \\
    $N_{\text{Si}}$              &  \multicolumn{2}{c|}{$\geq 7$} \\
    $N^{\text{sh}}_{\text{Si}}$    & \multicolumn{2}{c|}{$\leq 1$} \\
    $N^{\text{hole}}_{\text{Si}}$  & \multicolumn{2}{c|}{$\leq 2$} \\ 
    $N^{\text{hole}}_{\text{Pix}}$ & \multicolumn{2}{c|}{$\leq 1$} \\ \hline
    Track-To-Vertex            & -  &  \emph{Loose} \\
    $|\Delta z_0|\sin\theta$   & $< 3$ mm  &     - \\
    \hline
  \end{tabular}

  \caption{Track selection for the different track isolation variables.
    The first six lines correspond to the \emph{Loose} track quality requirements (with $\pt > 1\ \gev$ instead of $\pt > 500\ \mev$).
    $N_{\text{Si}}$ is the number of silicon (Pixel + SCT) hits (including dead sensors); $N^{\text{sh}}_{\text{Si}}$ is the number of shared hits
    (defined as $N_{\text{Pix}}^{\text{sh}} + N_{\text{SCT}}^{\text{sh}}/2$ where $N_{\text{Pix}}^{\text{sh}}$ and
    $N_{\text{SCT}}^{\text{sh}}$ are the number of hits assigned to several tracks in the Pixel and SCT detectors);
    $N_{\text{Si}}^{\text{hole}}$ is the number of silicon holes (\emph{i.e.} missing hits in the pixel and SCT detectors);
    $N_{\text{Pix}}^{\text{hole}}$ is the number of pixel holes; the \emph{Loose} Track-To-Vertex association is described in the text;
    $\Delta z_0$ is the longitudinal impact parameter of the track with respect 
    to a given vertex (by default, the one maximizing $\Sigma \pt^2$).}
  \label{tab:ele_track_selection}

\end{table}

A variable similar to \ptvcs, but built with a fixed radius, is used for the photon track isolation.
It is labelled \ptcs in Section~\ref{sec:ph_iso}.

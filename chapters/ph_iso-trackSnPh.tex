For the computation of the scale factors related to the photon track isolation (track-only) with single photon events, the efficiencies for Data and Monte Carlo (Pythia) must be obtained for \texttt{FixedCutTight} and \texttt{FixedCutLoose} working points. For each measurement, the cut on the track isolation variable (see table~\ref{tab:phWPs}) is applied at pre-selection level. The main difficulty for the track isolation related measurements is that the distribution is not continuous (a large peak at 0, followed by a gap and then a small tail), thus it cannot be fitted with an analytic function as done for the calo only isolation measurements.

Single photon samples' largest background contamination comes from jets faking photons. This source of background is estimated by obtaining a background template from a background-enriched control region (where the photon identification cut is reversed) which is further used in a signal region enriched in tightly identified photons. Thus, to measure the isolation efficiency in Data, the photons that pass the pre-selection cuts summarized in section~\ref{sec:ph_calotrackiso_sgPhotons} and the requirement on the calorimeter isolation used to define a certain photon isolation working point (see Table~\ref{tab:phWPs}) are classified in two regions:
\begin{itemize}
\item signal region (SR): photons passing the Tight ID requirement;
\item control region (CR): photons failing the Tight ID requirement but passing the Loose'X ID requirement (X = 2, 3, 4, 5; Loose'4 is the nominal control region).
\end{itemize}

After the event categorization is done, an iterative template fit is performed:
\begin{itemize}
\item Using Data in the CR as the background template and the MC simulations in the SR as the signal template, perform a first fit to Data in the SR to get a first estimation of the number of background and signal photons;
\item Fixing the ratio of the number of events with signal photons in the SR and CR using the MC expectation, the number of signal events leaking in the CR is also known and it can be subtracted from Data in the CR to obtain a new background shape;
\item Using the new background shape and the MC signal template, perform a second template fit to Data in the SR to get the final number of background and signal events. %The CR should perfectly agree by construction.
\end{itemize}
For the nominal measurement, the fit fit range is [0., 0.30]. To check the fit stability, two additional fit ranges are considered, [0., 0.15] (reduced fit range) and [0, $\infty$) (extended fit range). Same bins in photon $\eta$ and \ET as for the calo-only measurement with single photon events are considered. The event weight is taken as in detailed in section~\ref{sec:ph_caloSnPh}.

Figure~\ref{fig. tracking: iso signal} shows the typical distribution of the \ptcs/\pt variable for unconverted photons in the signal region, illustrating the fit performance, for two \pt regions. The MC signal template is shown with blue, while the background template is shown with cyan. The fit results (red line) match very well the Data observation. 
%an example of result for the template fit to the \ptcs/\pt distribution for signal Data (black dots) with signal (blue line) and background (light blue line) templates for unconverted photons for the signal region (red is the full model).
%Plots in Fig.~\ref{fig. tracking: iso signal} show the template fit of ptcone20/\pt for signal Data with signal and background templates for unconverted photons for the signal (a) and the background (b) region. 


%\begin{figure}[htbp]
%\centering
%  \includegraphics[width=0.49\textwidth]{ph_iso_var/{fit_UnConverted_Eta0.00_0.60_PT45_50_Pythia_0.3_LoosePrime4_ptcone20_loosecalocut_ite2}.eps}
%  \includegraphics[width=0.49\textwidth]{ph_iso_var/{fit_UnConverted_Eta0.00_0.60_PT45_50_Pythia_0.3_LoosePrime4_ptcone20_loosecalocut_bkg}.eps}
%  \caption{Template fit of ptcone20/\pt for Data (dots) for signal region (left) and background region (right) with unconverted,  $0.0<|\eta|<0.6$ and $45<\pt<50~\gev$, loose calo isolation cut applied.}
%\label{fig:ptcone_distribution_template_fit_unc}
%\end{figure}
\begin{figure}
\centering
\includegraphics[width=0.4\textwidth]{ph_iso-trackSnPh/FitTemplate/{fit_UnConverted_Eta0_00_0_60_PT25_30_Pythia_0_3_LoosePrime4_ptcone20_calocut_ite2.pdf}}
\includegraphics[width=0.4\textwidth]{ph_iso-trackSnPh/FitTemplate/{fit_UnConverted_Eta0_00_0_60_PT125_150_Pythia_0_3_LoosePrime4_ptcone20_calocut_ite2.pdf}}

	\caption{Measurement using single photons, track-only: Template fit of \ptcs/\pt\ for Data (dots) for signal region with unconverted photons, in the 0.0<|$\eta$|<0.6 and 25 < \pt < 30 GeV bin for the left plot and 125 < \pt < 150 GeV bin for the right plot. The $\etcb < 0.022 \times \pt + 2.45$ \gev  calorimeter isolation cut is applied at pre-selection level. The pull is defined as: (Data - fit results) / $\sqrt{(err_{Data})^2 + (err_{Fit})^2}$. Only statistical uncertainties are shown.}
\label{fig. tracking: iso signal}
\end{figure}

%\begin{figure}[htbp]
%\centering
%  \includegraphics[width=0.49\textwidth]{ph_iso_var/{fit_Converted_Eta1.81_2.37_PT25_30_Pythia_0.3_LoosePrime4_ptcone20_loosecalocut_ite2}.eps}
%  \includegraphics[width=0.49\textwidth]{ph_iso_var/{fit_Converted_Eta1.81_2.37_PT25_30_Pythia_0.3_LoosePrime4_ptcone20_loosecalocut_bkg}.eps}
%  \caption{Measurement using single photons: Template fit of ptcone20/\pt for Data (dots) for signal region (left) and background region (right) with converted,  $1.81<|\eta|<2.37$ and $25<\pt<30~\gev$, loose calo isolation cut applied.}
%\label{fig:ptcone_distribution_template_fit_con}
%\end{figure}


After the fit, one can obtain the fit residual, which is the difference between Data and the sum of template. Assuming that the residual all comes from the mismodeling of the MC signal shape, the residual is added back to the MC signal distribution and the data-driven number of signal photons is finally obtained.

%The plots of Data, signal, background and fit model of the variable ptcone20/$p_T$ can be seen in Fig. \ref{fig. tracking: iso} for 0 < $\eta$ < 0.6, 25 GeV <$p_T$ < 30 GeV and requiring unconverted photons.

%\begin{figure}
%\centering
%\subfloat[\emph{}]{\includegraphics[width=0.45\textwidth]{ph_iso-trackSnPh/FitTemplate/{fit_UnConverted_Eta0-00_0-60_PT25_30_Pythia_0-3_LoosePrime4_ptcone20_calocut_ite2.pdf}}}
%\subfloat[\emph{}]{\includegraphics[width=0.45\textwidth]{ph_iso-trackSnPh/FitTemplate/{fit_UnConverted_Eta0-00_0-60_PT25_30_Pythia_0-3_LoosePrime4_ptcone20_calocut_bkg.pdf}}}
%	\caption{Measurement using single photons: Template fit of ptcone20/$p_T$ for Data (dots) for signal region (a) and background region (b) with unconverted photons, 0.0<|$\eta$|<0.6 and 25 < $p_T$< 30  GeV, and calo isolation cut applied.}
%\label{fig. tracking: iso}
%\end{figure}

% Once the \ptcs distribution is obtained for the signal photons, the efficiency is computed. 
%dividing the integral of the distribution of the variable $\ptcs/\pt$ below 0.05, to the integral over the whole range. For the scale factors (SF), the ratio of the efficiencies in Data and MC is performed.%
%\begin{eqnarray}
%\label{eq. tracking: eff}
%	\varepsilon = \frac{N_{\ptcs/\pt<0.05}}{N_{total}}
%\end{eqnarray}
%
%\begin{eqnarray}
%\label{eq. tracking: SF}
%	SF = \frac{\varepsilon_{\text{Data-driven}}}{\varepsilon_{MC}}
%\end{eqnarray}
%

Figure~\ref{fig. tracking: SFs} illustrates the track-only efficiencies and corresponding scale factors (lower panel) for \texttt{FixedCutLoose} without and with the calorimeter cut applied at pre-selection level for unconverted photons, and for \texttt{FixedCutTight} for unconverted and converted photons, in the 0 < $\eta$ < 0.6 region. 

\begin{figure}
\centering
\subfloat[\emph{}]{\includegraphics[width=0.45\textwidth]{ph_iso-trackSnPh/effSFplots/{combine_UnConverted_Eta0_00_0_60_Sh_0_3_LoosePrime4_ptcone20_nocalocut_CR1.pdf}}}
\subfloat[\emph{}]{\includegraphics[width=0.45\textwidth]{ph_iso-trackSnPh/effSFplots/{combine_UnConverted_Eta0_00_0_60_Sh_0_3_LoosePrime4_ptcone20_loosecalocut_CR1.pdf}}}\\
\subfloat[\emph{}]{\includegraphics[width=0.45\textwidth]{ph_iso-trackSnPh/effSFplots/{combine_UnConverted_Eta0_00_0_60_Sh_0_3_LoosePrime4_ptcone20_calocut_CR1.pdf}}}
\subfloat[\emph{}]{\includegraphics[width=0.45\textwidth]{ph_iso-trackSnPh/effSFplots/{combine_Converted_Eta0_00_0_60_Sh_0_3_LoosePrime4_ptcone20_calocut_CR1.pdf}}}
	\caption{Measurement using single photons, track-only: Track isolation $\ptcs /\pt < 0.05$ cut efficiencies and SFs (lower panel) as a function of \ET for unconverted photons when (a) no cut on the calorimeter based isolation (for illustration purposes only) and (b) $\etcs < 0.065 \times \pt$ (used to define \texttt{FixedCutLoose} working point). The results for $\etcb < 0.022 \times \pt + 2.45$ \gev\ (\texttt{FixedCutTight} working point) are shown for converted photons (c) and unconverted (d) photons. The selection on the calorimeter based isolation variable is considered at the pre-selection level. The results are shown as a function of photon \pt, in the |$\eta$|<0.6 region. Only the statistical uncertainties are shown.}
\label{fig. tracking: SFs}
\end{figure}


\subsubsection{Systematic uncertainties}

Two sources of systematic uncertainties which affect the tracking isolation efficiency scale factors are considered. The first source comes from the three different definitions of the background templates which correspond to the Loose'X photon ID requirements (X = 2, 3, 5). The second source is due to the alternative fit range in which the fits are performed. 

Efficiencies for each of these five configurations are computed, and with them the corresponding scale factors. Once the five different scale factors are calculated, a bin-by-bin scan is performed, keeping the largest deviation with respect to the nominal value, amongst the five variations. The total uncertainty is obtained adding in quadrature the total systematic uncertainty and the statistical uncertainty. 

Plots in Figure \ref{fig: syst tracking} show, for the three working points and the converted and unconverted photons cases, each of the contributions for the different systematic uncertainties, along with the very small statistical uncertainty and the total calculation. One should notice that almost the totality of the latter, is due to the definition of the range in which the fit for estimating the efficiency is done.

\begin{figure}
\centering
\subfloat[\emph{}]{\includegraphics[width=0.45\textwidth]{ph_iso-trackSnPh/systPlots/SystSF_13TeV_2017Data_Rel21_con_TrkIso_v23_FixedCutTight.eps}}
\subfloat[\emph{}]{\includegraphics[width=0.45\textwidth]{ph_iso-trackSnPh/systPlots/SystSF_13TeV_2017Data_Rel21_unc_TrkIso_v23_FixedCutTight.eps}}\\
\subfloat[\emph{}]{\includegraphics[width=0.45\textwidth]{ph_iso-trackSnPh/systPlots/SystSF_13TeV_2017Data_Rel21_con_TrkIso_v23_FixedCutLoose.eps}}
\subfloat[\emph{}]{\includegraphics[width=0.45\textwidth]{ph_iso-trackSnPh/systPlots/SystSF_13TeV_2017Data_Rel21_unc_TrkIso_v23_FixedCutLoose.eps}}\\
\subfloat[\emph{}]{\includegraphics[width=0.45\textwidth]{ph_iso-trackSnPh/systPlots/SystSF_13TeV_2017Data_Rel21_con_TrkIso_v23_FixedCutnocalocut.eps}}
\subfloat[\emph{}]{\includegraphics[width=0.45\textwidth]{ph_iso-trackSnPh/systPlots/SystSF_13TeV_2017Data_Rel21_unc_TrkIso_v23_FixedCutnocalocut.eps}}
\caption{Scale factors (black dots) with the statistical uncertainty, the systematic uncertainties due to the background template definition (green), due to the fitting range (red), and the total uncertainty (blue) almost completely overlapped by the fitting range uncertainty. }
\label{fig: syst tracking}
\end{figure}

